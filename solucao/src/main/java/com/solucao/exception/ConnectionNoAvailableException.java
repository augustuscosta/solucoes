package com.solucao.exception;

public class ConnectionNoAvailableException extends Exception {

    public ConnectionNoAvailableException() {
    }

    public ConnectionNoAvailableException(String detailMessage) {
        super(detailMessage);
    }

    public ConnectionNoAvailableException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ConnectionNoAvailableException(Throwable throwable) {
        super(throwable);
    }

}

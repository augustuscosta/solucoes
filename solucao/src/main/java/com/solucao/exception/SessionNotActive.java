package com.solucao.exception;

/**
 * Created by pierrediderot on 01/06/14.
 */
public class SessionNotActive extends Throwable {

    public SessionNotActive() {
    }

    public SessionNotActive(String detailMessage) {
        super(detailMessage);
    }

    public SessionNotActive(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public SessionNotActive(Throwable throwable) {
        super(throwable);
    }
}

package com.solucao.exception;

/**
 * Created by pierrediderot on 01/06/14.
 */
public class UserBlocked extends Throwable {

    public UserBlocked() {
    }

    public UserBlocked(String detailMessage) {
        super(detailMessage);
    }

    public UserBlocked(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UserBlocked(Throwable throwable) {
        super(throwable);
    }
}

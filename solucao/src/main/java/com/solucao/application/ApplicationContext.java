package com.solucao.application;

import android.app.Application;

import com.google.inject.Inject;
import com.google.inject.Stage;
import com.solucao.manager.LoginManager;
import com.solucao.module.ModuleConfiguration;

import roboguice.RoboGuice;
import roboguice.util.Ln;

public class ApplicationContext extends Application {

    @Inject
    private LoginManager loginManager;

    @Override
    public void onCreate() {
        super.onCreate();
        RoboGuice.setBaseApplicationInjector(this, Stage.PRODUCTION,  RoboGuice.newDefaultRoboModule(this), new ModuleConfiguration());
        initSession();
    }

    private void initSession() {
        try {
            loginManager.loadSession();
        } catch (Exception e) {
            Ln.e(e, "Erro ao inicializar a sessão.");
        }
    }

    private void initServices() {
    }

}

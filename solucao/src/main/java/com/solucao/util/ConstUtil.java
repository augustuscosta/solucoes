package com.solucao.util;

/**
 * Created by brunoramosdias on 27/04/14.
 */
public class ConstUtil {

    public static final int USER_REQUEST = 21;
    public static final int TABLE_SELECT = 22;
    public static final int SIGNUP_ID = 0;
    public static final int SIGNIN_ID = 1;
    public static final int QR_CODE_SCAN = 12;
    public static final int SETUP_REQUEST = 2;
    public static final int MAIN_MENU = 45;
    public static final String COMMA =";";
    public static final String SPACE =" ";
    public static final String SERIALIZABLE_KEY ="SERIALIZABLE_KEY";
    public static final int ORDER_BILL = 23;
}

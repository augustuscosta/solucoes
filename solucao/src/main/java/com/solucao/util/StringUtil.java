package com.solucao.util;

/**
 * Created by pierrediderot on 04/06/14.
 */
public class StringUtil {

    public static boolean isEmpty(final String value) {
        return value == null || value.trim().length() == 0;
    }

}

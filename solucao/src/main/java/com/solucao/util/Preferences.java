package com.solucao.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by brunoramosdias on 27/04/14.
 */
public class Preferences {

    public static final String PREFS 						 = "SESSION_PREFS";
    public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
    public static final String SSID              			 = "SSID_KEY";
    private static SharedPreferences settings;


    private static SharedPreferences getSharedPreferencesInstance(Context context){
        if(settings == null){
            settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        }
        return settings;
    }



    public static void setServer(String server,Context context){
        settings = getSharedPreferencesInstance(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SERVER_PREFS_KEY, server);
        editor.commit();
    }

    public static String getServer(Context context){
        return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "mock");
    }

}

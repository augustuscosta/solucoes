package com.solucao.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.List;

/**
 * Created by brunoramosdias on 04/06/14.
 */
public class ConnectivityHelper {


    public static boolean connectToWifi(String networkSSID, String networkPass, Context context){
        WifiConfiguration conf = getWifiConfiguration(networkSSID, networkPass);
        WifiManager wifiManager = getWifiManager(context, conf);
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        boolean connected = connectToSelectedNetwork(networkSSID, wifiManager, list);
        return connected;
    }

    public static void enableWifi(WifiManager wifiManager) {
        if(!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }
    }

    public static boolean connectToSelectedNetwork(String networkSSID, WifiManager wifiManager, List<WifiConfiguration> list) {
        for( WifiConfiguration wifiConfiguration : list ) {
            if(wifiConfiguration.SSID != null && wifiConfiguration.SSID.equals("\"" + networkSSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(wifiConfiguration.networkId, true);
                return wifiManager.reconnect();

            }
        }

        return false;
    }

    public static WifiConfiguration getWifiConfiguration(String networkSSID, String networkPass) {
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";
        conf.preSharedKey = "\""+ networkPass +"\"";
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        return conf;
    }

    public static WifiManager getWifiManager(Context context, WifiConfiguration conf) {
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);
        return wifiManager;
    }




    public static String getWifiSSid(Context context){
        WifiManager wifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if(isOnWifi(context)){
            return wifiInfo.getSSID();
        }else{
            return null;
        }
    }

    public static boolean isOnWifi(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }

        return false;
    }

    public static boolean isWifiEnabled(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifi.isWifiEnabled();
    }

    public static void enableWifi(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }
}

package com.solucao.fragment;

import android.content.Context;
import android.os.Bundle;

public class FragmentBuilder {

	private String tag;
	private Class<?> clazz;
	private Bundle bundleArgs;
	private Context context;
	private  boolean isBackPressed;
	private int layoutID = android.R.id.content;
	
	public FragmentBuilder(Context context) {
		this.context = context;
	}

	public FragmentBuilder addFragmentContentLayout(final int layoutID) {
		this.layoutID = layoutID;
		return this;
	}
	
	public FragmentBuilder addTag(final String tag) {
		this.tag = tag;
		return this;
	}

	public FragmentBuilder addClazz(final Class<?> clazz) {
		this.clazz = clazz;
		return this;
	}
	
	public FragmentBuilder addArgs(final Bundle bundleArgs) {
		this.bundleArgs = bundleArgs;
		return this;
	}
	
	public FragmentBuilder addBackPressed(final boolean isBackPressed) {
		this.isBackPressed = isBackPressed;
		return this;
	}

	public String getTag() {
		return tag;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public Bundle getBundleArgs() {
		return bundleArgs;
	}

	public Context getContext() {
		return context;
	}

	public int getLayoutID() {
		return layoutID;
	}

	public boolean isBackPressed() {
		return isBackPressed;
	}
	
}

package com.solucao.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * @author Pierre Diderot II
 */
public class FragmentHelper {
	
	private final FragmentManager fragmentManager;
	
	public FragmentHelper(FragmentManager fragmentManager) {
		this.fragmentManager = fragmentManager;
	}

	public Fragment add(final FragmentBuilder fragmentBuilder) {
		Fragment fragment = fragmentManager.findFragmentByTag(fragmentBuilder.getTag());
		if ( fragment != null ) {
			showFragment(fragment);
			return fragment; 
		}
		
		fragment = Fragment.instantiate(fragmentBuilder.getContext(), fragmentBuilder.getClazz().getName(), fragmentBuilder.getBundleArgs());
		final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(fragmentBuilder.getLayoutID(), fragment, fragmentBuilder.getTag());
		
		if(fragmentBuilder.isBackPressed()){
			fragmentTransaction.addToBackStack(fragmentBuilder.getTag());
		}
		
		fragmentTransaction.commit();
		return fragment;
	}
	
	private void showFragment(final Fragment fragment) {
		final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.show(fragment);
	}

	public void remove(final String tag) {
		final Fragment fragment = findFragmentByTag(tag);
		if ( fragment == null ) {
			return;
		}
		
		final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.detach(fragment);
		fragmentTransaction.commit();
	}

	public Fragment findFragmentByTag(final String tag) {
		return fragmentManager.findFragmentByTag(tag);
	}
	
	public void removeFragment(final Fragment fragment){
		if(fragmentManager.getBackStackEntryCount() > 0){
			fragmentManager.popBackStack(fragment.getTag(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
		
		fragmentManager.beginTransaction().remove(fragment).commit();
	}

}

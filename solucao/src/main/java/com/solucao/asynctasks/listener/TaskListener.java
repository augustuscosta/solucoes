package com.solucao.asynctasks.listener;

import com.solucao.domain.Order;

/**
 * Created by pierrediderot on 03/07/14.
 */
public interface TaskListener<ResultType> {

    void onException(Exception e);
    void onSuccess(ResultType resultType);

}

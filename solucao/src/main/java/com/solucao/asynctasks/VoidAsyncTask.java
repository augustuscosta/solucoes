package com.solucao.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by brunoramosdias on 30/05/14.
 */
public class VoidAsyncTask extends AsyncTask<Void,Void,Void> {

    protected ProgressDialog dialog;
    protected Context context;

    public VoidAsyncTask(Context context) {
        this.context = context;
        dialog = new ProgressDialog(context);

    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }
}

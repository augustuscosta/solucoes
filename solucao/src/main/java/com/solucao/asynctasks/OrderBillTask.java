package com.solucao.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.asynctasks.listener.TaskListener;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.domain.Order;
import com.solucao.domain.Session;
import com.solucao.manager.OrderManager;

import java.util.ArrayList;
import java.util.List;

import roboguice.util.RoboAsyncTask;

/**
 * Created by pierrediderot on 11/06/14.
 */
public class OrderBillTask extends RoboAsyncTask<Void> {

    private Order orderToSave;
    private List<Order> bill;
    private ProgressDialog progressDialog;
    private OrderManager orderManager;
    private TaskListener listener;

    @Inject
    protected OrderBillTask(Context context) {
        super(context);
    }


    @Override
    public Void call() throws Exception {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getString(R.string.asking_order));
        progressDialog.setMessage(context.getString(R.string.processing));
        bill = orderManager.orderBill(Session.getSession().getTable().getId());
        return null;
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        if ( hasListener() ) {
            listener.onException(e);
        }
    }

    @Override
    protected void onSuccess(Void aVoid) throws Exception {
        if ( hasListener() ) {
            listener.onSuccess(bill);
        }
    }

    public void addListener(final TaskListener listener) {
        this.listener = listener;
    }

    private boolean hasListener() {
        return listener != null;
    }

}

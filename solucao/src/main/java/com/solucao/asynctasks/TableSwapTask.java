package com.solucao.asynctasks;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.solucao.R;
import com.solucao.activity.table.TableSwapTaskCallback;
import com.solucao.domain.Table;
import com.solucao.manager.TableManager;

/**
 * Created by brunoramosdias on 30/05/14.
 */
public class TableSwapTask extends BooleanAsyncTask {

    private TableSwapTaskCallback tableSwapTaskCallback;
    private TableManager tableManager;
    private Table table;
    private Table newTable;

    public TableSwapTask(Context context, TableSwapTaskCallback tableSwapTaskCallback, TableManager tableManager, Table newTable) {
        super(context);
        dialog = new ProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setMessage(context.getString(R.string.requisita_troca_mesa));
        dialog.setOnCancelListener(onCancelListener);
        this.tableSwapTaskCallback = tableSwapTaskCallback;
        this.tableManager = tableManager;
        this.newTable = newTable;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            table = tableManager.changeTable(1,tableManager.getTable(),newTable);
        } catch (Exception e) {
            Log.e(context.getString(R.string.app_name), "erro ao substiuir mesa", e);
        }
        return table!= null;
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        dialog.dismiss();
        tableSwapTaskCallback.tableResult(table);
        super.onPostExecute(aBoolean);
    }

    Dialog.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            tableSwapTaskCallback.presentTableErrorDialog(newTable);
        }
    };
}

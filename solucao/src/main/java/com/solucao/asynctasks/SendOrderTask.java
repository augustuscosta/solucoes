package com.solucao.asynctasks;

import android.content.Context;

import com.google.inject.Inject;
import com.solucao.asynctasks.listener.TaskListener;
import com.solucao.domain.Order;
import com.solucao.manager.OrderManager;

import roboguice.util.RoboAsyncTask;

/**
 * Created by pierrediderot on 11/06/14.
 */
public class SendOrderTask extends RoboAsyncTask<Void> {

    private Order orderToSave;

    @Inject
    private OrderManager orderManager;
    private TaskListener listener;

    @Inject
    protected SendOrderTask(Context context) {
        super(context);
    }

    public void addOrderToSave(final Order order) {
        orderToSave = order;
    }

    @Override
    public Void call() throws Exception {
        if ( orderToSave != null ) {
            orderManager.save(orderToSave);
            orderManager.sendOrder(orderToSave);
        } else {
            orderManager.sendPendingOrders();
        }

        return null;
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        if ( hasListener() ) {
            listener.onException(e);
        }
    }

    @Override
    protected void onSuccess(Void aVoid) throws Exception {
        if ( hasListener() ) {
            listener.onSuccess(aVoid);
        }
    }

    public void addListener(final TaskListener listener) {
        this.listener = listener;
    }

    private boolean hasListener() {
        return listener != null;
    }

}

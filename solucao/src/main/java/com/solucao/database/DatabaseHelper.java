package com.solucao.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.google.inject.Inject;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.solucao.R;
import com.solucao.domain.Category;
import com.solucao.domain.Order;
import com.solucao.domain.OrderProduct;
import com.solucao.domain.OrderProductItemAdditional;
import com.solucao.domain.OrderProductItemMontage;
import com.solucao.domain.OrderProductItemOptional;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;
import com.solucao.domain.Session;
import com.solucao.domain.Table;

import java.sql.SQLException;

import roboguice.util.Ln;

/**
 * Database helper class used to manage the creation and upgrading of your
 * database. This class also usually provides the DAOs used by the other
 * classes.
 *
 * @author pierrediderot
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "solucao_db.db";
	private static int DATABASE_VERSION = 1;
	
	private Context context;

	@Inject
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
		this.context = context;
    }

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Ln.d("Creating database");
			createTables(connectionSource);
		} catch (Throwable e) {
			Ln.e(e, "Error to create tables in database");
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		Ln.i("Upgrade Database from version " + oldVersion + " to version " + newVersion);
        // TODO Migrations here.
	}

	private void createTables(final ConnectionSource connectionSource) throws SQLException {
		TableUtils.createTable(connectionSource, Session.class);
        TableUtils.createTable(connectionSource, Table.class);

		TableUtils.createTable(connectionSource, Category.class);

        TableUtils.createTable(connectionSource, Product.class);
        TableUtils.createTable(connectionSource, ProductItemOptional.class);
        TableUtils.createTable(connectionSource, ProductItemAdditional.class);
		TableUtils.createTable(connectionSource, ProductItemMontage.class);

		TableUtils.createTable(connectionSource, Order.class);
		TableUtils.createTable(connectionSource, OrderProduct.class);
		TableUtils.createTable(connectionSource, OrderProductItemAdditional.class);
		TableUtils.createTable(connectionSource, OrderProductItemOptional.class);
		TableUtils.createTable(connectionSource, OrderProductItemMontage.class);
    }
}

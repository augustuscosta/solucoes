package com.solucao.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.solucao.domain.Category;
import com.solucao.domain.Order;
import com.solucao.domain.OrderProduct;
import com.solucao.domain.OrderProductItemAdditional;
import com.solucao.domain.OrderProductItemMontage;
import com.solucao.domain.OrderProductItemOptional;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;
import com.solucao.domain.Session;
import com.solucao.domain.Table;
import com.solucao.domain.User;

import java.io.File;

/**
 * @author pierrediderot
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {


    private final static Class[] classz = { Session.class,
                                            Category.class,
                                            Order.class,
                                            OrderProduct.class,
                                            OrderProductItemAdditional.class,
                                            OrderProductItemMontage.class,
                                            OrderProductItemOptional.class,
                                            Product.class,
                                            ProductItemAdditional.class,
                                            ProductItemMontage.class,
                                            ProductItemOptional.class,
                                            Table.class,
                                            User.class
                                           };

    public static void main(String[] args) throws Exception {
        System.out.println("Generating DB Config File...");
        writeConfigFile(new File("solucao/src/main/res/raw/ormlite_config.txt"), classz);
    }

}
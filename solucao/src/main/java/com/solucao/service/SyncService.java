package com.solucao.service;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.fandas.webclient.serializer.SerializerUtil;
import com.google.inject.Inject;
import com.solucao.asynctasks.SendOrderTask;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.manager.OrderManager;
import com.solucao.util.ConstUtil;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import roboguice.service.RoboService;
import roboguice.util.Ln;

/**
 * @author Pierre Diderot
 */
public class SyncService extends RoboService {

    public static final String INTENT_AUTHORIZATION_REQUEST = "COM.SOLUCAO.AUTHORIZATION.REQUEST";

    private LocalBroadcastManager localBroadcast;

    private ScheduledExecutorService executor;
    private ScheduledFuture<?> schedulerSynchronizeOrdersWaitingApproval;
    private ScheduledFuture<?> schedulerSendPendingOrders;
    private ScheduledFuture<?> schedulerAuthorizationRequest;

    @Inject
    private SendOrderTask sendOrderTask;

    @Inject
    private OrderManager orderManager;

    public SyncService() {}

    @Override
    public void onCreate() {
        super.onCreate();
        localBroadcast = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startDaemons();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Ln.d("Parando daemons que sincronizam os pedidos.");
        stopScheduler(schedulerSendPendingOrders);
        stopScheduler(schedulerSynchronizeOrdersWaitingApproval);
        stopScheduler(schedulerAuthorizationRequest);

        if ( executor != null && !executor.isShutdown() ) {
            executor.shutdownNow();
        }
    }

    private void stopScheduler(ScheduledFuture<?> scheduler) {
        if ( scheduler != null && !scheduler.isCancelled() ) {
            scheduler.cancel(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void startDaemons() {
        Ln.d("Iniciando os deamons de sincronismo.");
        executor = Executors.newScheduledThreadPool(3);
        schedulerSynchronizeOrdersWaitingApproval = executor.schedule(synchronizeOrdersWaitingApprovalJob, 1, TimeUnit.MINUTES);
        schedulerSendPendingOrders = executor.schedule(sendPendingOrdersJob, 1, TimeUnit.MINUTES);
        schedulerAuthorizationRequest = executor.schedule(authorizationRequestJob, 1, TimeUnit.MINUTES);
    }

    private final Runnable sendPendingOrdersJob = new Runnable() {

        @Override
        public void run() {
            Ln.d("Enviando os pedidos pendentes");
            sendOrderTask.execute();
        }
    };

    private final Runnable synchronizeOrdersWaitingApprovalJob = new Runnable() {

        @Override
        public void run() {
            try {
                Ln.d("Sincronizando os pedidos por aprovar.");
                orderManager.synchronizeOrdersWaitingApproval();
            } catch (Exception e) {
                Ln.e(e, "Erro ao sincronizar os pedidos pendentes de aprovação.");
            }
        }
    };

    private final Runnable authorizationRequestJob = new Runnable() {

        @Override
        public void run() {
            // TODO Realizar a chamada que verifica se existe solicitacoes pendentes a serem processadas.
            Ln.d("Verificando se existe notificação pendentes.");
            gotAuthorizationRequest(null);
        }
    };

    private void gotAuthorizationRequest(final AuthorizationRequestUser request) {
        try {
            final Intent it = new Intent(INTENT_AUTHORIZATION_REQUEST);
            it.putExtra(ConstUtil.SERIALIZABLE_KEY, SerializerUtil.objectToString(request));
            localBroadcast.sendBroadcast(it);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao serializar a requisição de autorização de terceiros na mesa.");
        }
    }

}

package com.solucao.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by brunoramosdias on 12/05/14.
 */
public class Setup implements Serializable {

    @JsonProperty("ssid")
    private String ssid;

    @JsonProperty("password")
    private String password;

    @JsonProperty("atendimento")
    private Integer tableId;

    @JsonProperty("servidor")
    private String servidor;

    @JsonProperty("estabelecimento")
    private Integer localId;

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }
}

package com.solucao.domain.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.solucao.domain.Category;

import java.io.IOException;

import roboguice.util.Ln;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class ProductCategoryDeserializer extends JsonDeserializer<Category> {

    @Override
    public Category deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final Integer id = jp.readValueAs(Integer.class);
        if ( id == null ) {
            Ln.d("Product without category assign");
            return null;
        }

        final Category category = new Category();
        category.setId(id);
        return category;
    }
}

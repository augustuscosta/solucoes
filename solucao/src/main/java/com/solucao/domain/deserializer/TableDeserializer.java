package com.solucao.domain.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.solucao.domain.Table;

import java.io.IOException;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class TableDeserializer extends JsonDeserializer<Table> {

    @Override
    public Table deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final Integer id = jp.readValueAs(Integer.class);
        if ( id == null ) {
            return null;
        }

        final Table table = new Table();
        table.setId(id);
        return table;
    }
}

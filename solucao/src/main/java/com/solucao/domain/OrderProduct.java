package com.solucao.domain;

import android.provider.BaseColumns;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.solucao.domain.deserializer.OrderProductDeserializer;
import com.solucao.domain.serialize.OrderProductSerializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pierrediderot on 17/02/14.
 */
@DatabaseTable
public class OrderProduct {

    public final static String COLUMN_ORDER_ID = "order_id";
    public final static String COLUMN_PRODUCT_ID = "product_id";

    @JsonIgnore
    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private Integer id;

    @JsonIgnore
    @DatabaseField(foreign = true)
    private Order order;

    @JsonProperty("protudoId")
    @JsonSerialize(using = OrderProductSerializer.class)
    @JsonDeserialize(using = OrderProductDeserializer.class)
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Product product;

    @JsonProperty("quantidade")
    @DatabaseField
    private Integer quantity;

    @JsonProperty("obs")
    @DatabaseField
    private String note;

    @JsonProperty("msg")
    @DatabaseField
    private String messageStatus;

    @JsonProperty("status")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private OrderStatusType orderStatusType; // TODO Realmente eh nescessario este atributo aqui.?

    @JsonProperty("itensAdicionais")
    private List<OrderProductItemAdditional> productItemAdditionals;

    @JsonProperty("itensOpcionais")
    private List<OrderProductItemOptional> productItemOptionals;

    @JsonProperty("itensMontegem")
    private List<OrderProductItemMontage> productItemMontages;

    public OrderProduct() {}

    public OrderProduct(Order order, Product product, String note, int quantity) {
        this.order = order;
        this.product = product;
        this.quantity = quantity;
        this.note = note;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public OrderStatusType getOrderStatusType() {
        return orderStatusType;
    }

    public void setOrderStatusType(OrderStatusType orderStatusType) {
        this.orderStatusType = orderStatusType;
    }

    public List<OrderProductItemAdditional> getProductItemAdditionals() {
        if ( productItemAdditionals == null ) {
            productItemAdditionals = new ArrayList<OrderProductItemAdditional>();
        }

        return productItemAdditionals;
    }

    public void setProductItemAdditionals(List<OrderProductItemAdditional> productItemAdditionals) {
        this.productItemAdditionals = productItemAdditionals;
    }

    public List<OrderProductItemOptional> getProductItemOptionals() {
        if ( productItemOptionals == null ) {
            productItemOptionals = new ArrayList<OrderProductItemOptional>();
        }

        return productItemOptionals;
    }

    public void setProductItemOptionals(List<OrderProductItemOptional> productItemOptionals) {
        this.productItemOptionals = productItemOptionals;
    }

    public List<OrderProductItemMontage> getProductItemMontages() {
        if ( productItemMontages == null ) {
            productItemMontages = new ArrayList<OrderProductItemMontage>();
        }

        return productItemMontages;
    }

    public void setProductItemMontages(List<OrderProductItemMontage> productItemMontages) {
        this.productItemMontages = productItemMontages;
    }
}

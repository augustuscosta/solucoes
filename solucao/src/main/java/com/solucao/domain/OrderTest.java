package com.solucao.domain;

import com.fandas.webclient.serializer.SerializerUtil;

import java.util.List;

/**
 * Created by pierrediderot on 13/03/14.
 */
public class OrderTest {

    static {
        LoadDataMenuTest.assignCategoryProducts();
        LoadDataMenuTest.loadProductItens();
        LoadDataMenuTest.assignProductItens();
    }

    public static void main(String... args) {
        final Order order = generateOrder();

        try {
            System.out.println(SerializerUtil.objectToString(order));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static Order generateOrder() {
        OrderBuilder builder = new OrderBuilder();
        // Inicio do pedido
        builder.addOrUpdateProduct(LoadDataMenuTest.sucoAbacaxi, "Sem gelo.", 4);

        builder.addOrUpdateProduct(LoadDataMenuTest.pizzaMeio, null, 1);
        builder.addItemAdditional(LoadDataMenuTest.pizzaMeio, LoadDataMenuTest.bordaCatupiri, 1);
        builder.addItemAdditional(LoadDataMenuTest.pizzaMeio, LoadDataMenuTest.porcaoBacon, 1);
        builder.addItemMontage(LoadDataMenuTest.pizzaMeio, LoadDataMenuTest.pizzaPortuguesa, 1);
        builder.addItemMontage(LoadDataMenuTest.pizzaMeio, LoadDataMenuTest.pizzaMarguerita, 1);

        builder.addOrUpdateProduct(LoadDataMenuTest.burgerMax, "Sem verdura.", 1);
        builder.addItemAdditional(LoadDataMenuTest.burgerMax, LoadDataMenuTest.porcaoBacon, 1);
        builder.addItemAdditional(LoadDataMenuTest.burgerMax, LoadDataMenuTest.porcaoOvos, 1);
        builder.addItemAdditional(LoadDataMenuTest.burgerMax, LoadDataMenuTest.molhoBarbecue, 1);
        builder.addItemOptional(LoadDataMenuTest.burgerMax, LoadDataMenuTest.paoArabe, 1);
        // End
        return builder.build();
    }

}

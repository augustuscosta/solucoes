package com.solucao.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.solucao.domain.Category;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by pierrediderot on 11/02/14.
 */
public class MenuResponse {

    @JsonProperty("categorias")
    private List<Category> categories;

    @JsonProperty("produtos")
    private List<Product> products;

    @JsonProperty("protudoItensAdicionais")
    private List<ProductItemAdditional> productItemAdditionals;

    @JsonProperty("protudoItensMontagens")
    private List<ProductItemMontage> productItemMontages;

    @JsonProperty("protudoItensOpcionais")
    private List<ProductItemOptional> productItemOptionals;

    public void validate() throws Exception {
        if ( getCategories() == null || getCategories().isEmpty() ) {
            throw new InvalidParameterException("Menu sem nenhuma categoria.");
        }

        if ( getProducts() == null || getProducts().isEmpty() ) {
            throw new InvalidParameterException("Menu sem nunhum produto.");
        }
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<ProductItemAdditional> getProductItemAdditionals() {
        return productItemAdditionals;
    }

    public void setProductItemAdditionals(List<ProductItemAdditional> productItemAdditionals) {
        this.productItemAdditionals = productItemAdditionals;
    }

    public List<ProductItemMontage> getProductItemMontages() {
        return productItemMontages;
    }

    public void setProductItemMontages(List<ProductItemMontage> productItemMontages) {
        this.productItemMontages = productItemMontages;
    }

    public List<ProductItemOptional> getProductItemOptionals() {
        return productItemOptionals;
    }

    public void setProductItemOptionals(List<ProductItemOptional> productItemOptionals) {
        this.productItemOptionals = productItemOptionals;
    }
}

package com.solucao.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by pierrediderot on 11/02/14.
 */
@DatabaseTable
public class Category {

    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_INDEX = "index";

    @DatabaseField(id = true)
    private Integer id;

    @JsonProperty("nome")
    @DatabaseField
    private String name;

    @JsonProperty("ordem")
    @DatabaseField
    private int index;

    @JsonProperty("tipo")
    @DatabaseField
    private String type;

    @JsonProperty("produtos")
    @JsonManagedReference
    private List<Product> products;

    @DatabaseField
    private String imageUrl;

    public Category(int id) {
        this.id = id;
    }

    public Category() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (!id.equals(category.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

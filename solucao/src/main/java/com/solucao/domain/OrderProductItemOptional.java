package com.solucao.domain;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by pierrediderot on 17/02/14.
 */
@DatabaseTable
public class OrderProductItemOptional extends OrderProductItem {

    public OrderProductItemOptional() {
    }

    public OrderProductItemOptional(Order order, Product product, Product item, Integer quantity) {
        super(order, product, item, quantity);
    }
}

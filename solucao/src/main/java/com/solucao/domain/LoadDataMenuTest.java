package com.solucao.domain;

import com.fandas.webclient.serializer.SerializerUtil;
import com.solucao.domain.response.MenuResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pierrediderot on 08/03/14.
 */
public class LoadDataMenuTest {

    private static final Category catPratos;
    private static final Category catBebidas;
    private static final Category catSucos;
    private static final Category catPaes;
    private static final Category catRefrigerantes;
    private static final Category catExtraBordas;
    private static final Category catExtraPorcao;
    private static final Category catExtraMolho;
    public static final List<Category> categories;

    public static final Product burgerMax;
    public static final Product xSalada;
    public static final Product pizzaMeio;
    public static final Product pizzaMarguerita;
    public static final Product pizzaPortuguesa;
    public static final Product bordaCatupiri;
    public static final Product bordaRequeijao;
    public static final Product porcaoOvos;
    public static final Product porcaoMilho;
    public static final Product porcaoBacon;
    public static final Product molhoBarbecue;
    public static final Product paoArabe;
    public static final Product paoBatata;
    public static final Product paoBola;
    public static final Product sucoAbacaxi;
    public static final Product sucoManga;
    public static final Product cocacola2l;
    public static final Product kuat2l;
    public static final List<Product> products;

    private static List<ProductItemAdditional> productItemAdditionals;
    private static List<ProductItemMontage> productItemMontages;
    private static List<ProductItemOptional> productItemOptionals;

    static {
        // Categories
        catPratos = createCategory(1, "Pratos", "P", 1);
        catBebidas = createCategory(2, "Bebidas", "P", 2);
        catSucos = createCategory(3, "Sucos", "I", 0);
        catPaes = createCategory(4, "Pães", "I", 0);
        catRefrigerantes = createCategory(18, "Refrigerantes", "I", 0);
        catExtraBordas = createCategory(30, "Extra-Bordas", "I", 0);
        catExtraPorcao = createCategory(13, "Extra-Poção", "I", 0);
        catExtraMolho = createCategory(14, "Extra-Molho", "I", 0);

        categories = new ArrayList<Category>();
        categories.add(catPratos);
        categories.add(catBebidas);
        categories.add(catSucos);
        categories.add(catPaes);
        categories.add(catRefrigerantes);
        categories.add(catExtraBordas);
        categories.add(catExtraPorcao);
        categories.add(catExtraMolho);

        // Products
        burgerMax = createProduct(1, "COMBO BURGER MAX", "<b>Combo Burger Max</b><br>Composi&ccedil;&atilde;o:...", 17.5f, catPratos);
        xSalada = createProduct(2, "X-SALADA", "<b>X-SALADA</b><br>Composi&ccedil;&atilde;o:...", 12f, catPratos);
        pizzaMeio = createProduct(3, "PIZZA MEIO A MEIO", "<b>PIZZA MEIO A MEIO</b><br>Composi&ccedil;&atilde;o:...", 27.99f, catPratos);

        pizzaMarguerita = createProduct(180, "MARGUERITA", "<b>Pizza Marguerita</b><br>Composição:<li>Tomate<li>...", 27.99f, catPratos);
        pizzaPortuguesa = createProduct(181, "PORTUGUESA", "<b>Pizza Portuguesa</b><br>Composição:<li>Tomate<li>...", 27.99f, catPratos);

        bordaCatupiri = createProduct(201, "BORDA CATUPIRI", "<b>BORDA CATUPIRI</b><br>Composição:<li>Catupiri<li>...", 2.3f, catExtraBordas);
        bordaRequeijao = createProduct(202, "BORDA REQUEIJAO", "<b>BORDA REQUEIJAO</b><br>Composição:<li>Requeijão<li>...", 2.3f, catExtraBordas);

        porcaoOvos = createProduct(141, "PORCAO DE OVOS", "<b>PORÇÃO DE OVOS</b>", 1f, catExtraPorcao);
        porcaoMilho = createProduct(142, "PORCAO DE MILHO", "<b>PORÇÃO DE MILHO</b>", 1.5f, catExtraPorcao);
        porcaoBacon = createProduct(143, "PORCAO DE BACON", "<b>PORÇÃO DE BACON</b>", 1.5f, catExtraPorcao);

        molhoBarbecue = createProduct(162, "MOLHO BARBECUE", "<b>MOLHO BARBECUE</b>", 1.5f, catExtraMolho);

        paoArabe = createProduct(107, "PAO ARABE", "<b>PÃO ÁRABE</b>", 1.5f, catPaes);
        paoBatata = createProduct(108, "PAO BATATA", "<b>PÃO BATATA</b>", 1.5f, catPaes);
        paoBola = createProduct(109, "PAO BOLA", "<b>PÃO BOLA</b>", 1f, catPaes);

        sucoAbacaxi = createProduct(4, "SUCO DE ABACAXI", "<b>SUCO DE ABACAXI</b><br>Composi&ccedil;&atilde;o:...", 4.5f, catSucos);
        sucoManga = createProduct(5, "SUCO DE MANGA", "<b>SUCO DE MANGA</b><br>Composi&ccedil;&atilde;o:...", 4.5f, catSucos);

        cocacola2l = createProduct(190, "COCA-COLA 2L", "<b>COCA-COLA 2L</b>", 6f, catRefrigerantes);
        kuat2l = createProduct(197, "KUAT 2L", "<b>KUAT 2L</b>", 5.50f, catRefrigerantes);

        products = new ArrayList<Product>();
        products.add(burgerMax);
        products.add(xSalada);
        products.add(pizzaMeio);
        products.add(pizzaMarguerita);
        products.add(pizzaPortuguesa);
        products.add(bordaCatupiri);
        products.add(bordaRequeijao);
        products.add(porcaoOvos);
        products.add(porcaoMilho);
        products.add(porcaoBacon);
        products.add(molhoBarbecue);
        products.add(paoArabe);
        products.add(paoBatata);
        products.add(paoBola);
        products.add(sucoAbacaxi);
        products.add(sucoManga);
        products.add(cocacola2l);
        products.add(kuat2l);

        assignCategoryProducts();
        loadProductItens();
        assignProductItens();
    }

    public static void main(String[] args) {
        try {
            System.out.println("Categorias: " + SerializerUtil.objectToString(categories));
            System.out.println("\nProdutos: " + SerializerUtil.objectToString(products));

            System.out.println("\nProduto -> ItemAdicional: " + SerializerUtil.objectToString(productItemAdditionals));
            System.out.println("\nProduto -> ItemOpcional:" + SerializerUtil.objectToString(productItemOptionals));
            System.out.println("\nProduto -> ItemMontagem:" + SerializerUtil.objectToString(productItemMontages));

            MenuResponse menuResponse = getMenuResponse();
            System.out.println("\nMenu:" + SerializerUtil.objectToString(menuResponse));

            final String menuJson = SerializerUtil.objectToString(menuResponse);
            menuResponse = SerializerUtil.fromToObject(menuJson, MenuResponse.class);
            System.out.println("\n  Test Deserialize - Menu Response:" + SerializerUtil.objectToString(menuResponse));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static MenuResponse getMenuResponse() {
        final MenuResponse menuResponse = new MenuResponse();
        menuResponse.setCategories(categories);
        menuResponse.setProducts(products);
        menuResponse.setProductItemAdditionals(productItemAdditionals);
        menuResponse.setProductItemOptionals(productItemOptionals);
        menuResponse.setProductItemMontages(productItemMontages);
        return menuResponse;
    }

    public static void assignProductItens() {
        for (Product product : products) {
            product.setItemsAdditional(getItemsAdditional(product));
            product.setItemsOptional(getItemsOptional(product));
            product.setItemsMontage(getItemsMontage(product));
        }
    }

    private static List<Product> getItemsMontage(final Product product) {
        final List<Product> items = new ArrayList<Product>();
        Product itemClone;
        for (ProductItemMontage item : productItemMontages) {
            if (product.getId() == item.getProduct().getId()) {
                itemClone = item.getItem();
                if (itemClone != null) {
                    items.add(itemClone);
                }
            }
        }

        return items;
    }

    private static List<Product> getItemsOptional(final Product product) {
        final List<Product> items = new ArrayList<Product>();
        Product itemClone;
        for (ProductItemOptional item : productItemOptionals) {
            if (product.getId() == item.getProduct().getId()) {
                itemClone = item.getItem();
                if (itemClone != null) {
                    items.add(itemClone);
                }
            }
        }

        return items;
    }

    private static List<Product> getItemsAdditional(final Product product) {
        final List<Product> items = new ArrayList<Product>();
        Product itemClone;
        for (ProductItemAdditional item : productItemAdditionals) {
            if (product.getId() == item.getProduct().getId()) {
                itemClone = item.getItem();
                if (itemClone != null) {
                    items.add(itemClone);
                }
            }
        }

        return items;
    }

    private static Product getProduct(final Product product) {
        for (Product p : products) {
            if (product.getId() == p.getId()) {
                return p;
            }
        }

        return null;
    }

    public static void loadProductItens() {
        try {
            // Json contendo as associassoes Produtos -> Itens.
            final String json = "[{\"id\":\"1\",\"itensMontagem\":[],\"itensOpcionais\":[{\"categoriaId\":\"4\",\"id\":\"107\",\"nome\":\"Pão �RABE\"},{\"categoriaId\":\"4\",\"id\":\"108\",\"nome\":\"Pão BATATA\"},{\"categoriaId\":\"4\",\"id\":\"109\",\"nome\":\"Pão BOLA\"},{\"categoriaId\":\"5\",\"id\":\"131\",\"nome\":\"SUCO DE ABACAXI\"},{\"categoriaId\":\"5\",\"id\":\"132\",\"nome\":\"SUCO DE MANGA\"},{\"categoriaId\":\"5\",\"id\":\"133\",\"nome\":\"SUCO DE ACEROLA\"},{\"categoriaId\":\"5\",\"id\":\"134\",\"nome\":\"SUCO DE GOIABA\"},{\"categoriaId\":\"5\",\"id\":\"135\",\"nome\":\"SUCO DE UVA\"},{\"categoriaId\":\"5\",\"id\":\"136\",\"nome\":\"SUCO DE AMEIXA\"},{\"categoriaId\":\"5\",\"id\":\"137\",\"nome\":\"SUCO DE TOMATE\"}],\"itensAdicionais\":[{\"categoriaId\":\"13\",\"id\":\"141\",\"nome\":\"POR�ão DE OVOS\",\"preco\":\"1.0\"},{\"categoriaId\":\"13\",\"id\":\"142\",\"nome\":\"POR�ão DE MILHO\",\"preco\":\"1.5\"},{\"categoriaId\":\"13\",\"id\":\"143\",\"nome\":\"POR�ão DE ERVILHA\",\"preco\":\"1.75\"},{\"categoriaId\":\"14\",\"id\":\"161\",\"nome\":\"MOLHO PARMESão\",\"preco\":\"1.5\"},{\"categoriaId\":\"14\",\"id\":\"162\",\"nome\":\"MOLHO BARBECUE\",\"preco\":\"1.5\"},{\"categoriaId\":\"14\",\"id\":\"163\",\"nome\":\"MOLHO BRANCO\",\"preco\":\"1.5\"}]},{\"id\":\"2\",\"itensMontagem\":[],\"itensOpcionais\":[{\"categoriaId\":\"4\",\"id\":\"107\",\"nome\":\"Pão �RABE\"},{\"categoriaId\":\"4\",\"id\":\"108\",\"nome\":\"Pão BATATA\"},{\"categoriaId\":\"4\",\"id\":\"109\",\"nome\":\"Pão BOLA\"},{\"categoriaId\":\"5\",\"id\":\"131\",\"nome\":\"SUCO DE ABACAXI\"},{\"categoriaId\":\"5\",\"id\":\"132\",\"nome\":\"SUCO DE MANGA\"},{\"categoriaId\":\"5\",\"id\":\"133\",\"nome\":\"SUCO DE ACEROLA\"},{\"categoriaId\":\"5\",\"id\":\"134\",\"nome\":\"SUCO DE GOIABA\"},{\"categoriaId\":\"5\",\"id\":\"135\",\"nome\":\"SUCO DE UVA\"}],\"itensAdicionais\":[{\"categoriaId\":\"13\",\"id\":\"141\",\"nome\":\"POR�ão DE OVOS\",\"preco\":\"1.0\"},{\"categoriaId\":\"13\",\"id\":\"142\",\"nome\":\"POR�ão DE MILHO\",\"preco\":\"1.5\"},{\"categoriaId\":\"13\",\"id\":\"143\",\"nome\":\"POR�ão DE ERVILHA\",\"preco\":\"1.75\"},{\"categoriaId\":\"14\",\"id\":\"161\",\"nome\":\"MOLHO PARMESão\",\"preco\":\"1.5\"},{\"categoriaId\":\"14\",\"id\":\"162\",\"nome\":\"MOLHO BARBECUE\",\"preco\":\"1.5\"}]},{\"id\":\"3\",\"itensMontagem\":[{\"categoriaId\":\"0\",\"id\":\"180\",\"nome\":\"MARGUERITA\",\"Descricao\":\"<b>Pizza Marguerita</b><br>Composi�ão:<li>Tomate<li>...\"},{\"categoriaId\":\"0\",\"id\":\"180\",\"nome\":\"PORTUGUESA\",\"Descricao\":\"<b>Pizza Portuguesa</b><br>Composi�ão:<li>Tomate<li>...\"},{\"categoriaId\":\"0\",\"id\":\"180\",\"nome\":\"4 QUEIJOS\",\"Descricao\":\"<b>Pizza 4 Queijos</b><br>Composi�ão:<li>Tomate<li>...\"},{\"categoriaId\":\"0\",\"id\":\"180\",\"nome\":\"NORDESTINA\",\"Descricao\":\"<b>Pizza Nordestina</b><br>Composi�ão:<li>Tomate<li>...\"},{\"categoriaId\":\"0\",\"id\":\"180\",\"nome\":\"NAPOLITANA\",\"Descricao\":\"<b>Pizza Napolitana</b><br>Composi�ão:<li>Tomate<li>...\"}],\"itensOpcionais\":[{\"categoriaId\":\"18\",\"id\":\"190\",\"nome\":\"COCA-COLA 2L\"},{\"categoriaId\":\"18\",\"id\":\"192\",\"nome\":\"PEPSI-COLA 2L\"},{\"categoriaId\":\"18\",\"id\":\"197\",\"nome\":\"KUAT 2L\"}],\"itensAdicionais\":[{\"categoriaId\":\"30\",\"id\":\"201\",\"nome\":\"BORDA CATUPIRI\",\"preco\":\"2.3\"},{\"categoriaId\":\"30\",\"id\":\"201\",\"nome\":\"BORDA REQUEIJAO\",\"preco\":\"2.3\"},{\"categoriaId\":\"30\",\"id\":\"201\",\"nome\":\"BORDA PARMESão\",\"preco\":\"3.5\"}]}]";
            final List<Product> prods = Arrays.asList(SerializerUtil.fromToObject(json, (new Product[0]).getClass()));
            productItemAdditionals = new ArrayList<ProductItemAdditional>();
            productItemMontages = new ArrayList<ProductItemMontage>();
            productItemOptionals = new ArrayList<ProductItemOptional>();

            for (Product product : prods) {
                if (product.getItemsAdditional() != null) {
                    for (Product additional : product.getItemsAdditional()) {
                        productItemAdditionals.add(createProductItemAdditional(product, additional));
                    }
                }

                if (product.getItemsMontage() != null) {
                    for (Product montage : product.getItemsMontage()) {
                        productItemMontages.add(createProductItemMontage(product, montage));
                    }
                }

                if (product.getItemsOptional() != null) {
                    for (Product optional : product.getItemsOptional()) {
                        productItemOptionals.add(createProductItemOptional(product, optional));
                    }
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static void assignCategoryProducts() {
        for (Category category : categories) {
            category.setProducts(getProducts(category));
        }
    }

    private static List<Product> getProducts(final Category category) {
        final List<Product> prods = new ArrayList<Product>();
        for (Product product : products) {
            if (category.getId() == product.getCategory().getId()) {
                prods.add(product);
            }
        }

        return prods;
    }

    public static Category createCategory(Integer id, String name, String type, Integer index) {
        final Category category = new Category();
        category.setId(id);
        category.setName(name);
        category.setType(type);
        category.setIndex(index);
        category.setImageUrl("http://www.rantlifestyle.com/wp-content/uploads/2014/01/Fast-Food.jpg"); // TODO MOCK RETIRAR ESSA ATRIBUICAO.
        return category;
    }

    public static Product createProduct(Integer id, String name, String description, Float price, Category category) {
        final Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDescription(description);
        product.setPrice(price);
        product.setCategory(category);
        product.setImageUrl("http://www.bahianoticias.com.br/fotos/editor/Image/tortelini1.JPG");
        return product;
    }

    public static ProductItemAdditional createProductItemAdditional(Product product, Product item) {
        final ProductItemAdditional productItemAdditional = new ProductItemAdditional();
        productItemAdditional.setProduct(product);
        productItemAdditional.setItem(item);
        return productItemAdditional;
    }

    public static ProductItemOptional createProductItemOptional(Product product, Product item) {
        final ProductItemOptional productItemOptional = new ProductItemOptional();
        productItemOptional.setProduct(product);
        productItemOptional.setItem(item);
        return productItemOptional;
    }

    public static ProductItemMontage createProductItemMontage(Product product, Product item) {
        final ProductItemMontage productItemMontage = new ProductItemMontage();
        productItemMontage.setProduct(product);
        productItemMontage.setItem(item);
        return productItemMontage;
    }

    public static List<ProductItemAdditional> getProductItemAdditionals() {
        return productItemAdditionals;
    }

    public static List<ProductItemMontage> getProductItemMontages() {
        return productItemMontages;
    }

    public static List<ProductItemOptional> getProductItemOptionals() {
        return productItemOptionals;
    }

}

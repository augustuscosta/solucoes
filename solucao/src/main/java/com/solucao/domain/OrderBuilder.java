package com.solucao.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by pierrediderot on 14/03/14.
 */
public class OrderBuilder {

    private Order order;
    private final List<OrderProduct> products = new ArrayList<OrderProduct>();

    public OrderBuilder() {
        createOrder();
    }

    private void createOrder() {
        this.order = new Order();
        this.order.setKey(UUID.randomUUID().toString());
        this.order.setOrderStatusType(OrderStatusType.TO_SEND);

        final Session session = Session.getSession();
        this.order.setLocalId(session.getLocalId());
        this.order.setTable(session.getTable());
    }

    public Order build() {
        order.setProducts(products);
        return order;
    }

    public void addOrUpdateProduct(final Product product, final String note, int quantity) {
        OrderProduct orderProduct = getOrderProduct(product);
        if ( orderProduct == null ) {
            orderProduct = new OrderProduct(order, product, note, quantity);
            products.add(orderProduct);
        }

        orderProduct.setQuantity(quantity);
        orderProduct.setNote(note);
    }

    // TODO ATENCAO AO LER OS TRES METODOS ABAIXO IRAM PERCEBER QUE EH REPETICAO DE CODIGO,
    // POR FAVOR O CIDADAO QUE FOR MEXER AQUI NESSE BUILDER REFATORE ESSES METODOS DE ADICIONAR ITENS AOS PRODUTOS.
    public void addItemAdditional(final Product product, final Product item, final Integer quantity) {
        final OrderProduct orderProduct = getOrderProduct(product);
        if ( orderProduct == null ) {
            return;
        }

        orderProduct.getProductItemAdditionals().add( createOrderProductItemAdditional(product, item, quantity) );
    }

    public void addItemOptional(final Product product, final Product item, final Integer quantity) {
        final OrderProduct orderProduct = getOrderProduct(product);
        if ( orderProduct == null ) {
            return;
        }

        orderProduct.getProductItemOptionals().add( createOrderProductItemOptional(product, item, quantity) );
    }

    public void addItemMontage(final Product product, final Product item, final Integer quantity) {
        final OrderProduct orderProduct = getOrderProduct(product);
        if ( orderProduct == null ) {
            return;
        }

        orderProduct.getProductItemMontages().add( createOrderProductItemMontage(product, item, quantity) );
    }

    private OrderProduct getOrderProduct(Product product) {
        for (OrderProduct orderProduct : products) {
            if ( product.getId() == orderProduct.getProduct().getId() ) {
                return orderProduct;
            }
        }

        return null;
    }

    private OrderProductItemAdditional createOrderProductItemAdditional(final Product product, final Product item, final int quantity) {
        return new OrderProductItemAdditional(order, product, item, quantity);
    }

    private OrderProductItemOptional createOrderProductItemOptional(final Product product, final Product item, final int quantity) {
        return new OrderProductItemOptional(order, product, item, quantity);
    }

    private OrderProductItemMontage createOrderProductItemMontage(final Product product, final Product item, final int quantity) {
        return new OrderProductItemMontage(order, product, item, quantity);
    }
}

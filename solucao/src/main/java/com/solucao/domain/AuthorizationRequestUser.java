package com.solucao.domain;

/**
 * Created by pierrediderot on 16/07/14.
 */
public class AuthorizationRequestUser {

    private User user;

    private Boolean accept;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getAccept() {
        return accept;
    }

    public void setAccept(Boolean accept) {
        this.accept = accept;
    }
}

package com.solucao.domain;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by pierrediderot on 17/02/14.
 */
@DatabaseTable
public class OrderProductItemAdditional extends OrderProductItem {

    public OrderProductItemAdditional() {
    }

    public OrderProductItemAdditional(Order order, Product product, Product item, Integer quantity) {
        super(order, product, item, quantity);
    }
}

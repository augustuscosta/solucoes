package com.solucao.domain.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.solucao.domain.Category;

import java.io.IOException;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class ProductCategorySerializer extends JsonSerializer<Category> {

    @Override
    public void serialize(Category value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        jgen.writeNumber(value.getId());
    }
}

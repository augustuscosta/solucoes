package com.solucao.domain.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.solucao.domain.Table;

import java.io.IOException;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class TableSerializer extends JsonSerializer<Table> {

    @Override
    public void serialize(Table value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        jgen.writeNumber(value.getId());
    }
}

package com.solucao.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.solucao.domain.deserializer.ProductCategoryDeserializer;
import com.solucao.domain.serialize.ProductCategorySerializer;

import java.util.List;

/**
 * Created by pierrediderot on 11/02/14.
 * Edited by brunoramosdias on 25/06/2014
 */
@DatabaseTable
public class Product {

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CATEGORY_ID = "category_id";

    @DatabaseField(id = true)
    private Integer id;

    @JsonProperty(value = "nome")
    @DatabaseField
    private String name;

    @JsonProperty("descricao")
    @DatabaseField
    private String description;

    @JsonProperty("preco")
    @DatabaseField
    private Float price;

    @JsonProperty("categoriaId")
    @JsonSerialize(using = ProductCategorySerializer.class)
    @JsonDeserialize(using = ProductCategoryDeserializer.class)
    @JsonBackReference
    @DatabaseField(foreign = true)
    private Category category;

    @JsonProperty("itensOpcionais")
    private List<Product> itemsOptional;

    @JsonProperty("itensAdicionais")
    private List<Product> itemsAdditional;

    @JsonProperty("itensMontagem")
    private List<Product> itemsMontage;

    @DatabaseField
    private String imageUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Product> getItemsOptional() {
        return itemsOptional;
    }

    public void setItemsOptional(List<Product> itemsOptional) {
        this.itemsOptional = itemsOptional;
    }

    public List<Product> getItemsAdditional() {
        return itemsAdditional;
    }

    public void setItemsAdditional(List<Product> itemsAdditional) {
        this.itemsAdditional = itemsAdditional;
    }

    public List<Product> getItemsMontage() {
        return itemsMontage;
    }

    public void setItemsMontage(List<Product> itemsMontage) {
        this.itemsMontage = itemsMontage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (!id.equals(product.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

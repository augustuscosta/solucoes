package com.solucao.domain;

/**
 * Created by pierrediderot on 17/02/14.
 */
public enum OrderStatusType {

    TO_SEND,
    SUCCESS,
    WARNING,
    WAITING_APPROVAL,
    UNAUTHORIZED,

}

package com.solucao.domain;

import android.provider.BaseColumns;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DatabaseField;
import com.solucao.domain.deserializer.ProductItemDeserializer;
import com.solucao.domain.serialize.ProductItemSerializer;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class ProductItem {

    public static final String COLUMN_PRODUCT_ID = "product_id";
    public static final String COLUMN_ITEM_ID = "item_id";

    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    protected Integer id;

    @JsonProperty("produtoId")
    @JsonSerialize(using = ProductItemSerializer.class)
    @JsonDeserialize(using = ProductItemDeserializer.class)
    @DatabaseField(foreign = true)
    protected Product product;

    @JsonProperty("produtoItemId")
    @JsonSerialize(using = ProductItemSerializer.class)
    @JsonDeserialize(using = ProductItemDeserializer.class)
    @DatabaseField(foreign = true)
    protected Product item;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getItem() {
        return item;
    }

    public void setItem(Product item) {
        this.item = item;
    }
}

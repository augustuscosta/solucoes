package com.solucao.domain;

import java.util.List;

/**
 * Created by pierrediderot on 11/02/14.
 */
public class Menu {

    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}

package com.solucao.domain;

import android.provider.BaseColumns;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DatabaseField;
import com.solucao.domain.deserializer.OrderProductDeserializer;
import com.solucao.domain.serialize.OrderProductSerializer;

/**
 * Created by pierrediderot on 12/03/14.
 */
public class OrderProductItem {

    @JsonIgnore
    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    protected Integer id;

    @JsonIgnore
    @DatabaseField(foreign = true)
    protected Order order;

    @JsonIgnore
    @DatabaseField(foreign = true)
    protected Product product;

    @JsonProperty("itemProdutoId")
    @JsonSerialize(using = OrderProductSerializer.class)
    @JsonDeserialize(using = OrderProductDeserializer.class)
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    protected Product item;

    @JsonProperty("quantidade")
    @DatabaseField
    protected Integer quantity; // TODO Precisamos desse atributo?

    public OrderProductItem() {
    }

    public OrderProductItem(Order order, Product product, Product item, Integer quantity) {
        this.order = order;
        this.product = product;
        this.item = item;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getItem() {
        return item;
    }

    public void setItem(Product item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

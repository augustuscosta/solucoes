package com.solucao.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by pierrediderot on 07/03/14.
 */
@DatabaseTable
public class Table {

    @DatabaseField(id = true)
    private Integer id;

    @JsonProperty("descricao")
    @DatabaseField
    private String description;

    @DatabaseField
    private String obs;

    @JsonProperty("reserva")
    @DatabaseField
    private Date reservedDate;

    @JsonProperty("numero")
    @DatabaseField
    private Integer number;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(Date reservedDate) {
        this.reservedDate = reservedDate;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}

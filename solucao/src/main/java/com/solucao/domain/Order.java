package com.solucao.domain;

import android.provider.BaseColumns;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.solucao.domain.deserializer.OrderTableDeserializer;
import com.solucao.domain.serialize.OrderTableSerializer;

import java.util.List;

/**
 * Created by pierrediderot on 17/02/14.
 */
@DatabaseTable
public class Order {

    public final static String COLUMN_ORDER_STATUS_TYPE = "orderStatusType";

    @JsonIgnore
    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private Integer id;

    @JsonProperty("status")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private OrderStatusType orderStatusType;

    @DatabaseField
    private String msg;

    @DatabaseField
    private Integer localId;

    @JsonProperty("produtos")
    private List<OrderProduct> products;

    @JsonProperty("atendimento")
    @JsonDeserialize(using = OrderTableDeserializer.class)
    @JsonSerialize(using = OrderTableSerializer.class)
    @DatabaseField(foreign = true)
    private Table table;

    @JsonProperty("id")
    @DatabaseField(unique = true)
    private String key;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OrderStatusType getOrderStatusType() {
        return orderStatusType;
    }

    public void setOrderStatusType(OrderStatusType orderStatusType) {
        this.orderStatusType = orderStatusType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public List<OrderProduct> getProducts() {
        return products;
    }

    public void setProducts(List<OrderProduct> products) {
        this.products = products;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public float getTotal() {
        float total = 0;
        // Somando os produtos do pedido.
        for (OrderProduct product : this.getProducts()) {
            total += product.getProduct().getPrice() * product.getQuantity();

            // Somando os itens adicionais.
            for (OrderProductItemAdditional orderProductItemAdditional : product.getProductItemAdditionals()) {
                total += orderProductItemAdditional.getItem().getPrice() * orderProductItemAdditional.getQuantity();
            }
        }

        return total;
    }
}

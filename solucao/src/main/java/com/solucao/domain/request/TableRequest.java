package com.solucao.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.solucao.domain.Table;
import com.solucao.domain.deserializer.TableDeserializer;
import com.solucao.domain.serialize.TableSerializer;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class TableRequest {

    @JsonSerialize(using = TableSerializer.class)
    @JsonDeserialize(using = TableDeserializer.class)
    @JsonProperty("mesaOrigemId")
    private Table originTable;

    @JsonSerialize(using = TableSerializer.class)
    @JsonDeserialize(using = TableDeserializer.class)
    @JsonProperty("mesaDestinoId")
    private Table targetTable;

    public TableRequest(Table origin, Table target) {
        this.originTable = origin;
        this.targetTable = target;
    }

    public TableRequest() {
        super();
    }

    public Table getOriginTable() {
        return originTable;
    }

    public void setOriginTable(Table originTable) {
        this.originTable = originTable;
    }

    public Table getTargetTable() {
        return targetTable;
    }

    public void setTargetTable(Table targetTable) {
        this.targetTable = targetTable;
    }
}

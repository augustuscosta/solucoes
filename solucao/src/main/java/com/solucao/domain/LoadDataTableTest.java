package com.solucao.domain;

import com.fandas.webclient.serializer.SerializerUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pierrediderot on 09/03/14.
 */
public class LoadDataTableTest {

    private static List<Table> tables = new ArrayList<Table>();

    static {
        Table table;
        table = new Table();
        table.setId(1);
        table.setDescription("Mesa 20");
        table.setNumber(20);
        table.setObs("Mesa reservada para Sr.Paulo Silva");
        table.setReservedDate(new Date());
        tables.add(table);

        table = new Table();
        table.setId(2);
        table.setDescription("Mesa 30");
        table.setNumber(30);
        tables.add(table);

        table = new Table();
        table.setId(3);
        table.setDescription("Mesa 54");
        table.setNumber(54);
        tables.add(table);

        table = new Table();
        table.setId(4);
        table.setDescription("Mesa 44");
        table.setNumber(44);
        table.setObs("Mesa para fumantes.");
        tables.add(table);
    }

    public static void main(String... args) {
        try {
            System.out.println("Mesas: " + SerializerUtil.objectToString(tables));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

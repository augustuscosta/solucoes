package com.solucao.domain;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Session {

    private static Session session;

    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private Integer id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private User user;

    @DatabaseField
    private String serverUrl;

    @DatabaseField
    private Integer localId;

    @DatabaseField(foreign = true)
    private Table table;

    public static Session getSession() {
        return session;
    }

    public static void addSession(final Session session) {
        Session.session = session;
    }

    public static boolean isActive() {
        return session != null;
    }

    public Session(String serverUrl, Integer localId) {
        this.serverUrl = serverUrl;
        this.localId = localId;
    }

    public Session() {}

    public static void setSession(Session session) {
        Session.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}

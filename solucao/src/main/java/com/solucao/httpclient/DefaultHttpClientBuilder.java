package com.solucao.httpclient;

import com.solucao.domain.Session;
import com.solucao.domain.User;

import ch.boye.httpclientandroidlib.auth.AuthScope;
import ch.boye.httpclientandroidlib.auth.UsernamePasswordCredentials;
import ch.boye.httpclientandroidlib.client.CredentialsProvider;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.impl.client.BasicCredentialsProvider;
import ch.boye.httpclientandroidlib.impl.client.HttpClientBuilder;

/**
 * Created by pierrediderot on 18/06/14.
 */
public class DefaultHttpClientBuilder {

    public HttpClient build() {
        final HttpClientBuilder builder = HttpClientBuilder.create();

        final User user = getUserFromSession();
        if ( user != null ) {
            final CredentialsProvider credentials = new BasicCredentialsProvider();
            credentials.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user.getEmail(), user.getPassword()));
            builder.setDefaultCredentialsProvider(credentials);
        }

        return builder.build();
    }

    private User getUserFromSession() {
        final Session session = Session.getSession();
        if ( session == null || session.getUser() == null ) {
            return null;
        }

        return session.getUser();
    }

}

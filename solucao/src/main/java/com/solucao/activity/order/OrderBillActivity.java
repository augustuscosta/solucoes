package com.solucao.activity.order;

import android.view.View;
import android.widget.Button;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.asynctasks.OrderBillTask;
import com.solucao.asynctasks.listener.TaskListener;
import com.solucao.domain.Order;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.interfacehandlers.DialogHelper;

import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.List;

import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 7/17/14.
 */
public class OrderBillActivity extends  PedidosActivity implements TaskListener<List<Order>>, DecisionDialogCallback {


    @Inject
    OrderBillTask orderBillTask;
    @InjectView(R.id.save_order)
    Button saveOrderButton;
    private List<Order> orders;


    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_orderbill);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_orderbill);
        goback.setOnClickListener(goBackOnClickListener);
        saveOrderButton.setOnClickListener(onClickConfirmOrder);
        orderBillTask.addListener(this);
        try {
            orderBillTask.call();
        } catch (Exception e) {
            Ln.e(e,"erro ao pedir conta");
        }
    }

    @Override
    public void onException(Exception e) {
        DialogHelper.presentError(this, getString(R.string.errro_pedir_conta),e.getMessage());
    }

    @Override
    public void onSuccess(List<Order> orders) {
        this.orders = orders;
        totalView.setText(getString(R.string.currency)+ new DecimalFormat("#.00").format(orderManager.getTotal(orders)));
        setAdapter();
    }

    @Override
    protected void showOrdersOnList() {
        orderBillTask.addListener(this);
        try {
            orderBillTask.call();
        } catch (Exception e) {
            Ln.e(e,getString(R.string.errro_pedir_conta));
        }
    }

    private final View.OnClickListener onClickConfirmOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          DialogHelper.presentDecisionDialog(OrderBillActivity.this,getString(R.string.ask_bill), getString(R.string.bill_accept),OrderBillActivity.this);
        }
    };


    @Override
    public void accept() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

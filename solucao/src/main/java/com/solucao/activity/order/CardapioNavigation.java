package com.solucao.activity.order;

/**
 * Created by brunoramosdias on 27/05/14.
 */
public enum CardapioNavigation {

    CATEGORY("categoria"),
    PRODUCT_GALLERY("produto"),
    PRODUCT_DETAIL("detail"),
    PIZZA_MEIO_A_MEIO("pizza_meio_a_meio"),
    PRODUCT_ADITIONAL("adicional"),
    PRODUCT_COUNT("contagem"),
    PRODUCT_OPTIONS("options");

    CardapioNavigation(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;


    public static CardapioNavigation fromValue(String tag) {
        for(CardapioNavigation cardapioNavigation : CardapioNavigation.values()){
            if(cardapioNavigation.getValue().equals(tag)){
                return cardapioNavigation;
            }
        }
        return null;
    }
}

package com.solucao.activity.order;

import android.os.Bundle;
import android.util.Log;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.domain.Category;
import com.solucao.domain.CategoryType;
import com.solucao.domain.OrderBuilder;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import roboguice.util.Ln;

public class CardapioActivity extends AbstractCardapioAcivity implements CardapioCallback {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_cardapio);
        orderBuilder = new OrderBuilder();
        createSingleInstanceOrderList();
        setListeners();
        getCategorysAndSetCardapio();

    }

    private void getCategorysAndSetCardapio() {
        try {
            categorys = getCategorys();
        } catch (SQLException e) {
            Log.e(getString(R.string.app_name), getString(R.string.erro_categorias_banco), e);
        }
        setCardapioFragment();
    }

    private void setListeners() {
        goBack.setOnClickListener(goBackOnClickListener);
    }

    @Override
    public void cardapioCategorySelected(Category category) {
        List<Product> products = null;
        try {
            products = getProductsFromCategory(category);
            getProductSelectFragment(products, true);
        } catch (SQLException e) {
            Log.e(getString(R.string.app_name), getString(R.string.erro_produtos_categoria) + category.getName(), e);
        }
    }

    @Override
    public void cardapioProductSelected(Product product) {
        getSupportFragmentManager().beginTransaction().remove(productfragment).commit();
        try {
            product = productManager.getProductWithItens(product.getId());
        } catch (Exception e) {
            Ln.e(e);
        }
        getNextProductFragment(product);
    }
    /*
    TODO desacoplar lógica em futuras versões
     */

    @Override
    public void goBack(CardapioNavigation cardapioNavigation) {
        switch (cardapioNavigation) {
            case CATEGORY:
                finish();
                break;
            case PRODUCT_GALLERY:
                returnFromProductSelection();
                break;
            case PIZZA_MEIO_A_MEIO:
                returnFromPizzaMeioAMeio();
                break;
            case PRODUCT_OPTIONS:
                returnFromProductOption();
                break;
            case PRODUCT_ADITIONAL:
                returnFromProductAditionals();
                break;
            case PRODUCT_COUNT:
                returnFromCount();
                break;
        }
    }

    private void returnFromProductAditionals() {
        Product product = productAditionalFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productAditionalFragment).commit();
        if (hasProducts(product.getItemsOptional())) {
            getProductOptionsFragment(product);
        } else if (hasProducts(product.getItemsMontage())) {
            getPizzaMeioAMeioFragment(product);
        } else {
            cardapioCategorySelected(product.getCategory());
        }
    }

    private void returnFromProductOption() {
        Product product = productOptionsFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productOptionsFragment).commit();
        if (hasProducts(product.getItemsMontage())) {
            getPizzaMeioAMeioFragment(product);
        } else {
            cardapioCategorySelected(product.getCategory());
        }
    }

    private void returnFromProductSelection() {
        getSupportFragmentManager().beginTransaction().remove(productfragment).commit();
        setCardapioFragment();
    }

    private void returnFromPizzaMeioAMeio() {
        Product product = productMontageFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productMontageFragment).commit();
        cardapioCategorySelected(product.getCategory());
    }


    private void returnFromCount() {
        Product product;
        product = productCountFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productCountFragment).commit();
        if (hasProducts(product.getItemsAdditional())) {
                getProductAditionalFragment(product);
        } else if (product.getItemsOptional() != null) {
            getProductOptionsFragment(product);
        } else if (product.getItemsMontage() != null) {
            getPizzaMeioAMeioFragment(product);
        } else {
            cardapioCategorySelected(product.getCategory());
        }
    }

    private List<Category> getCategorys() throws SQLException {
        try {
            return categoryManager.getCategories(CategoryType.P);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private List<Product> getProductsFromCategory(Category category) throws SQLException {
        try {
            return productManager.getProducts(category);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

}

package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.solucao.R;
import com.solucao.activity.order.CardapioNavigation;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;

import java.util.ArrayList;
import java.util.List;


public class ProductMontageFragment extends AbstractCardapioFragment {

    private final View.OnClickListener onClickCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cardapioCallback.goBack(CardapioNavigation.PIZZA_MEIO_A_MEIO);
        }
    };
    private CardapioCallback cardapioCallback;
    private List<Product> montageOptions;
    private Product product;
    private Product montage1;
    private Product montage2;
    private Spinner metade1;
    private Spinner metade2;
    private Button confirmButton;
    private Button cancelButton;
    private String[] montageArrayProvider;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pizza_meio_ameio, container, false);
        instantiateViews(view);
        return view;
    }

    private void instantiateViews(View view) {
        FragmentInterfaceHandler.manageLowerBar(view,R.layout.fragment_pizza_meio_ameio);
        metade1 = (Spinner) view.findViewById(R.id.metade1);
        metade2 = (Spinner) view.findViewById(R.id.metade2);
        confirmButton = (Button) view.findViewById(R.id.confirm);
        cancelButton = (Button) view.findViewById(R.id.cancell);
        setListeners();
        if (montageOptions != null) setSpinnerAdapter();
    }

    private void setListeners() {
        cancelButton.setOnClickListener(onClickCancel);
        confirmButton.setOnClickListener(onClickConfirm);
    }

    private void setSpinnerAdapter() {
        setMontageArrayProvider();
        adapter = getStringArrayAdapter();
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        adapter2 = getStringArrayAdapter();
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_item);
        setMetade1Adapter(adapter);
        setMetade2Adapter(adapter2);
        checkForExistingMontages();
    }

    private void checkForExistingMontages() {
        List<Product> montages = cardapioCallback.getProductMontageAdded(product);
        Product montage1 = null;
        Product montage2 = null;
        if(montages != null && !montages.isEmpty()){
            for(Product montage: montages){
                if(montage1 == null){
                    montage1 = montage;
                }else{
                    montage2 = montage;
                    break;
                }
            }
            if(montage1!= null){
                for (int i = 0; i < montageOptions.size() ; i++) {
                    if(montageOptions.get(i).equals(montage1)){
                       metade1.setSelection(i);
                        break;
                    }
                }
            }
            if(montage2!= null){
                for (int i = 0; i < montageOptions.size() ; i++) {
                    if(montageOptions.get(i).equals(montage2)){
                        metade2.setSelection(i);
                        break;
                    }
                }
            }
        }
    }

    private void setMontageArrayProvider() {
        montageArrayProvider = new String[montageOptions.size()];
        for (int i = 0; i < montageOptions.size(); i++) {
            montageArrayProvider[i] = montageOptions.get(i).getName();
        }
    }

    private ArrayAdapter<String> getStringArrayAdapter() {
        return new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item,
                montageArrayProvider);
    }

    private void setMetade2Adapter(ArrayAdapter<String> adapter) {
        metade2.setAdapter(adapter);
        metade2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int position, long l) {
                if (montageOptions == null || montageOptions.isEmpty()) {
                    return;
                }

                montage2 = montageOptions.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                return;
            }
        });
    }

    private void setMetade1Adapter(ArrayAdapter<String> adapter) {
        metade1.setAdapter(adapter);
        metade1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int position, long l) {
                if (montageOptions == null || montageOptions.isEmpty()) {
                    return;
                }

                montage1 = montageOptions.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                return;
            }
        });
    }

    public void setCardapioCallback(CardapioCallback cardapioCallback) {
        this.cardapioCallback = cardapioCallback;
    }

    public void setMontageOptions(List<Product> montageOptions) {
        this.montageOptions = montageOptions;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private final View.OnClickListener onClickConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<Product> montageSelected = new ArrayList<Product>();
            montageSelected.add(montage1);
            montageSelected.add(montage2);
            cardapioCallback.addItemMontage(product, montageSelected);
        }
    };
}

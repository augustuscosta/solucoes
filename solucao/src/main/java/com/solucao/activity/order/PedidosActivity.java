package com.solucao.activity.order;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericListActivity;
import com.solucao.adapter.OrderAdapter;
import com.solucao.domain.Order;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.manager.OrderManager;

import java.text.DecimalFormat;
import java.util.List;

import roboguice.inject.InjectView;
import roboguice.util.Ln;

public class PedidosActivity extends GenericListActivity {

    @InjectView(R.id.goBack)
    protected Button goback;
    @Inject
    protected OrderManager orderManager;
    protected List<Order> orders;
    private OrderAdapter orderAdapter;
    @InjectView(R.id.total)
    protected TextView totalView;
    private Handler handler;
    private static int delatyTime =15000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();
        showOrdersOnList();
    }

    protected void setLayout() {
        setContentView(R.layout.activity_pedidos);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_pedidos);
        goback.setOnClickListener(goBackOnClickListener);
    }

    @Override
    protected void onResume() {
        handler = new Handler();
        handler.postAtTime(refreshRunnable,delatyTime);
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(refreshRunnable);
        handler = null;
        super.onPause();
    }

    private void handleListRefresh() {
        showOrdersOnList();
    }


    protected void setAdapter() {
        orderAdapter = new OrderAdapter(orders,this);
        totalView.setText(getString(R.string.currency)+ new DecimalFormat("#.00").format(orderManager.getTotal(orders)));
        setListAdapter(orderAdapter);
    }


    protected void showOrdersOnList() {
        try {
            orders = orderManager.getAll();
        } catch (Exception e) {
            Ln.e("erro ao pegar os pedidos do banco", e);

        }
        setAdapter();
    }

    View.OnClickListener goBackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    final Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(
              uiThreadRunnable
            );
        }
    };

    final Runnable uiThreadRunnable = new Runnable() {
        @Override
        public void run() {
            handleListRefresh();
        }
    };
}

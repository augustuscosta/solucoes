package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

/**
 * Created by brunoramosdias on 01/06/14.
 */
public class ProductDetailFragment extends AbstractCardapioFragment {

    private Product product;
    private CardapioCallback cardapioCallback;
    private TextView productName;
    private TextView productDetail;
    private TextView productPrice;
    private Button saveOrderButton;

    public ProductDetailFragment(Product product, CardapioCallback cardapioCallback) {
        this.product = product;
        this.cardapioCallback = cardapioCallback;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_detail,container);
        instantiateViews(rootView);
        FragmentInterfaceHandler.manageLowerBar(rootView,R.layout.fragment_product_detail);
        return rootView;
    }

    private void instantiateViews(View rootView) {
        productName     = (TextView) rootView.findViewById(R.id.productName);
        productDetail   = (TextView) rootView.findViewById(R.id.productDetail);
        productPrice    = (TextView) rootView.findViewById(R.id.productPrice);
        saveOrderButton = (Button) rootView.findViewById(R.id.save_order);
        saveOrderButton.setOnClickListener(onClickSaveOrder);
        fillForm();
    }

    private void fillForm(){
        if(product == null) return;
        if(product.getName() != null) productName.setText(product.getName());
        if(product.getName() != null) productDetail.setText(product.getDescription());
        if(product.getName() != null) productPrice.setText(new DecimalFormat("#.00").format(product.getPrice()));
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private  final View.OnClickListener  onClickSaveOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           cardapioCallback.addProductCount(product,1);
       }
    };
}

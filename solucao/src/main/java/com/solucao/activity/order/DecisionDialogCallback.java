package com.solucao.activity.order;

/**
 * Created by brunoramosdias on 7/18/14.
 */
public interface DecisionDialogCallback {

    void accept();

    void cancel();
}

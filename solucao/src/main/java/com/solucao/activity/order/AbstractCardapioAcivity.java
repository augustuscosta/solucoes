package com.solucao.activity.order;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.activity.order.fragment.CardapioGalleryFragment;
import com.solucao.activity.order.fragment.ProductAditionalFragment;
import com.solucao.activity.order.fragment.ProductCountFragment;
import com.solucao.activity.order.fragment.ProductDetailFragment;
import com.solucao.activity.order.fragment.ProductFragment;
import com.solucao.activity.order.fragment.ProductMontageFragment;
import com.solucao.activity.order.fragment.ProductOptionsFragment;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.asynctasks.SendOrderTask;
import com.solucao.asynctasks.listener.TaskListener;
import com.solucao.domain.Category;
import com.solucao.domain.Order;
import com.solucao.domain.OrderBuilder;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;
import com.solucao.manager.CategoryManager;
import com.solucao.manager.OrderManager;
import com.solucao.manager.ProductManager;
import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 08/06/14.
 */
public class AbstractCardapioAcivity extends GenericActivity implements CardapioCallback, TaskListener<Void> {


    protected CardapioGalleryFragment cardapioGalleryFragment;
    protected ProductFragment productfragment;
    protected ProductDetailFragment productDetailFragment;
    protected ProductCountFragment productCountFragment;
    protected ProductMontageFragment productMontageFragment;
    protected ProductOptionsFragment productOptionsFragment;
    protected ProductAditionalFragment productAditionalFragment;
    protected List<ProductItemAdditional> productAdditionals = new ArrayList<ProductItemAdditional>();
    protected List<ProductItemOptional> productItemOptionals = new ArrayList<ProductItemOptional>();
    protected List<ProductItemMontage> productItemMontages = new ArrayList<ProductItemMontage>();
    protected String[] countArray;
    protected List<Order> orders;
    protected List<Category> categorys;
    protected List<String> countList = new ArrayList<String>();
    protected OrderBuilder orderBuilder;
    @InjectView(R.id.goBack)
    protected Button goBack;
    @Inject
    protected OrderManager orderManager;
    @Inject
    protected CategoryManager categoryManager;
    @Inject
    protected ProductManager productManager;
    @Inject
    protected SendOrderTask sendOrderTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void createSingleInstanceOrderList() {
        if (orders == null) {
            orders = new ArrayList<Order>();
        }
    }

    protected boolean hasProducts(List<Product> products) {
        return (products != null && !products.isEmpty());
    }

    protected void getNextProductFragment(Product product) {
        if (hasProducts(product.getItemsMontage())) {
            getPizzaMeioAMeioFragment(product);
        } else if (hasProducts(product.getItemsOptional())) {
            getProductOptionsFragment(product);
        } else if (hasProducts(product.getItemsAdditional())) {
            getProductAditionalFragment(product);
        } else {
            getProductCountFragment(product);
        }
    }

    protected void continueFlow(Product product, CardapioNavigation cardapioNavigation) {
        switch (cardapioNavigation) {
            case PIZZA_MEIO_A_MEIO:
                getSupportFragmentManager().beginTransaction().remove(productMontageFragment).commit();
                getProductOptionsFragment(product);
                break;
            case PRODUCT_OPTIONS:
                getSupportFragmentManager().beginTransaction().remove(productOptionsFragment).commit();
                getProductAditionalFragment(product);
                break;
            case PRODUCT_ADITIONAL:
                getSupportFragmentManager().beginTransaction().remove(productAditionalFragment).commit();
                getProductCountFragment(product);
                break;
            case PRODUCT_COUNT:
                getSupportFragmentManager().beginTransaction().remove(productCountFragment).commit();
                setCardapioFragment();
                break;
        }
    }

    protected void setCardapioFragment() {
        cardapioGalleryFragment = new CardapioGalleryFragment(categorys);
        cardapioGalleryFragment.setCallback(this);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, cardapioGalleryFragment
                , CardapioNavigation.CATEGORY.getValue()).commit();
    }

    protected void getProductSelectFragment(List<Product> products, boolean hasCardapio) {
        if (hasCardapio) {
            getSupportFragmentManager().beginTransaction().remove(cardapioGalleryFragment).commit();
        }
        productfragment = new ProductFragment();
        productfragment.setCallback(this);
        productfragment.setProducts(products);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productfragment
                , CardapioNavigation.PRODUCT_GALLERY.getValue()).commit();
    }

    protected void getPizzaMeioAMeioFragment(Product product) {
        productMontageFragment = new ProductMontageFragment();
        productMontageFragment.setCardapioCallback(this);
        productMontageFragment.setProduct(product);
        productMontageFragment.setMontageOptions(product.getItemsMontage());
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productMontageFragment
                , CardapioNavigation.PIZZA_MEIO_A_MEIO.getValue()).commit();
    }

    protected void getProductCountFragment(Product product) {
        productCountFragment = new ProductCountFragment(product);
        productCountFragment.setCallback(this);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productCountFragment
                , CardapioNavigation.PRODUCT_COUNT.getValue()).commit();
    }

    protected void getProductOptionsFragment(Product product) {
        productOptionsFragment = new ProductOptionsFragment(this, product);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productOptionsFragment,
                CardapioNavigation.PRODUCT_OPTIONS.getValue()).commit();
    }

    protected void getProductAditionalFragment(Product product) {
        productAditionalFragment = new ProductAditionalFragment();
        productAditionalFragment.setCardapioCallback(this);
        productAditionalFragment.setProduct(product);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productAditionalFragment,
                CardapioNavigation.PRODUCT_ADITIONAL.getValue()).commit();
    }

    @Override
    public void cardapioCategorySelected(Category category) {

    }

    @Override
    public void cardapioProductSelected(Product product) {

    }

    @Override
    public void goBack(CardapioNavigation navigation) {

    }

    protected void getProductPreview(Product product) {
        productDetailFragment = new ProductDetailFragment(product,this);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, productDetailFragment,
                CardapioNavigation.PRODUCT_DETAIL.getValue()).commit();
    }

    @Override
    public void addItemMontage(Product product, List<Product> items) {
        resetMontage();
        for (Product montage : items) {
            ProductItemMontage productItemMontage = new ProductItemMontage();
            productItemMontage.setProduct(product);
            productItemMontage.setItem(montage);
            productItemMontages.add(productItemMontage);
        }
        continueFlow(product, CardapioNavigation.PIZZA_MEIO_A_MEIO);
    }

    @Override
    public void addItemAditional(Product product, List<Product> items, List<String> countList) {
        resetAditionals();
        if(hasProducts(items)){
            for (Product montage : items) {
                ProductItemAdditional productItemMontage = new ProductItemAdditional();
                productItemMontage.setProduct(product);
                productItemMontage.setItem(montage);
                productAdditionals.add(productItemMontage);
            }
            this.countList = countList;
            continueFlow(product,CardapioNavigation.PRODUCT_ADITIONAL);
            return;
        }
        continueFlow(product,CardapioNavigation.PRODUCT_ADITIONAL);
    }

    @Override
    public void addItemOptional(Product product, List<Product> items) {
        resetOptions();
        if (items != null && !items.isEmpty()) {
            for (Product option : items) {
                ProductItemOptional productItemOptional = new ProductItemOptional();
                productItemOptional.setItem(option);
                productItemOptional.setProduct(product);
                productItemOptionals.add(productItemOptional);
            }
            continueFlow(product, CardapioNavigation.PRODUCT_OPTIONS);
            return;
        } else {
            continueFlow(product, CardapioNavigation.PRODUCT_OPTIONS);
        }
    }

    @Override
    public void addProductCount(Product product, int count) {
        orderBuilder.addOrUpdateProduct(product, "", count);
        for (ProductItemMontage productItemMontage : productItemMontages) {
            orderBuilder.addItemMontage(productItemMontage.getProduct(), productItemMontage.getItem(), 1);
        }
        for (ProductItemOptional productItemOptional : productItemOptionals) {
            orderBuilder.addItemOptional(productItemOptional.getProduct(), productItemOptional.getItem(), 1);
        }
        openContinueDialog();
    }

    @Override
    public List<Product> getProductAditionalsAdded(Product product) {
        List<Product> additonals = new ArrayList<Product>();
        for (ProductItemAdditional productItemAdditional : productAdditionals) {
            if (productItemAdditional.getProduct().equals(product)) {
                additonals.add(productItemAdditional.getItem());
            }
        }
        return additonals;
    }

    @Override
    public List<Product> getProductOptionalsAdded(Product product) {
        List<Product> optionals = new ArrayList<Product>();
        for (ProductItemOptional itemOptional : productItemOptionals) {
            if (itemOptional.getProduct().equals(product) && itemOptional.getItem() != null) {
                optionals.add(itemOptional.getItem());
            }
        }
        return optionals;
    }

    @Override
    public List<Product> getProductMontageAdded(Product product) {
        List<Product> montages = new ArrayList<Product>();
        for (ProductItemMontage orderProduct : productItemMontages) {
            if (orderProduct.getProduct().equals(product)) {
                montages.add(orderProduct.getItem());
            }
        }
        return montages;
    }

    @Override
    public List<String> getCountString() {
        return countList;
    }

    @Override
    public String[] getLastCountArray() {
        return countArray;
    }

    @Override
    public void setLastCountArray(String[] countArray) {
        this.countArray = countArray;
    }

    public String getActiveFragmentTag() {
        List<android.support.v4.app.Fragment> fragments = getSupportFragmentManager().getFragments();
        for (android.support.v4.app.Fragment fragment : fragments) {
            if (fragment.isVisible()) {
                return fragment.getTag();
            }
        }
        return null;
    }

    @Override
    public void goToPrevious() {
        String tag = getActiveFragmentTag();
        goBack(CardapioNavigation.fromValue(tag));
    }

    @Override
    public void onBackPressed() {
        goToPrevious();
    }

    protected void openContinueDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.continue_order));
        d.setMessage(getString(R.string.order_confirm_text));
        d.setPositiveButton(getString(R.string.continuar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                sendOrder();
                continueFlow(null, CardapioNavigation.PRODUCT_COUNT);
            }

        });
        d.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
              finish();
            }
        });
        d.setNeutralButton(getString(R.string.only_this_order), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendOrder();
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }

    private void sendOrder() {
        try {
            sendOrderTask.addOrderToSave(orderBuilder.build());
            sendOrderTask.execute();
        } catch (Exception e) {
            Ln.e(e, "Erro ao salvar pedido");
        }
        orderBuilder = new OrderBuilder();
    }

    private void resetOptions(){
        productItemOptionals = new ArrayList<ProductItemOptional>();
    }

    private void resetAditionals(){
        productAdditionals = new ArrayList<ProductItemAdditional>();
        countList = new ArrayList<String>();
    }
    private void resetMontage(){
        productItemMontages = new ArrayList<ProductItemMontage>();
    }

    protected final View.OnClickListener goBackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goToPrevious();
        }
    };


    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onSuccess(Void aVoid) {

    }
}

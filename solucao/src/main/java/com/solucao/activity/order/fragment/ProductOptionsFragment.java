package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.adapter.ProductItemOptionsAdapter;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemOptional;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;

import java.util.List;

public class ProductOptionsFragment extends AbsctractCardapioListFragment {

    private CardapioCallback cardapioCallback;
    private Product product;
    private List<Product> productOptions;
    private List<Product> productOptionsSelected;
    private Button addToOrder;
    private ProductItemOptionsAdapter adapter;

    public ProductOptionsFragment(CardapioCallback cardapioCallback, Product product) {
        this.cardapioCallback = cardapioCallback;
        this.product = product;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_options,container,false);
        instantiateViews(view);
        return view;
    }

    private void instantiateViews(View view) {
        FragmentInterfaceHandler.manageLowerBar(view,R.layout.fragment_options);
        addToOrder = (Button ) view.findViewById(R.id.addtToOrder);
        addToOrder.setOnClickListener(onClickAddToOrder);
        productOptions = product.getItemsOptional();
        adapter = new ProductItemOptionsAdapter(getActivity(),productOptions);
        setListAdapter(adapter);
        checkForExixtingOptions();
    }

    private void checkForExixtingOptions(){
        productOptionsSelected = cardapioCallback.getProductOptionalsAdded(product);
        adapter.setCheckedItems(productOptionsSelected);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private final View.OnClickListener onClickAddToOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(adapter.hasCheckedItems()){
                cardapioCallback.addItemOptional(product,adapter.getCheckedItems());
            }
        }
    };
}

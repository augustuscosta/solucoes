package com.solucao.activity.order.fragment.fragmentcallback;

import com.solucao.activity.order.CardapioNavigation;
import com.solucao.activity.order.fragment.CardapioGalleryFragment;
import com.solucao.domain.Category;
import com.solucao.domain.OrderProduct;
import com.solucao.domain.Product;

import java.util.List;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public interface CardapioCallback {

    void cardapioCategorySelected(Category category);
    void cardapioProductSelected(Product product);
    void goBack(CardapioNavigation navigation);
    void addItemMontage(final Product product,final List<Product> items);
    void addItemAditional(final Product product,final List<Product> items, List<String> countList);
    void addItemOptional(final Product product,final List<Product> items);
    void addProductCount(final Product product, int count);
    List<Product> getProductAditionalsAdded(final Product product);
    List<Product> getProductOptionalsAdded(final Product product);
    List<Product> getProductMontageAdded(final Product product);
    List<String> getCountString();
    String[] getLastCountArray();
    void setLastCountArray(String[] countArray);

}

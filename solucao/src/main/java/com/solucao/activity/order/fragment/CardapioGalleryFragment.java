package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;

import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.adapter.CategoryAdapter;
import com.solucao.domain.Category;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;

import java.util.List;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public class CardapioGalleryFragment extends AbstractCardapioFragment {

    @SuppressWarnings(value = "Deprecated")
    private Gallery horizontalGallery;
    private CardapioCallback callback;
    private List<Category> categorys;


    public CardapioGalleryFragment(List<Category> categorys) {
        this.categorys = categorys;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category,container,false);
        setUpGallery(rootView);
        return rootView;
    }

    private void setUpGallery(View rootView) {
        FragmentInterfaceHandler.manageLowerBar(rootView,R.layout.fragment_category);
        horizontalGallery = (Gallery) rootView.findViewById(R.id.horizontalGallery);
        horizontalGallery.setAdapter(new CategoryAdapter(categorys,getSherlockActivity()));
        horizontalGallery.setOnItemClickListener(onClickCategory);
    }

    private AdapterView.OnItemClickListener onClickCategory = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            callback.cardapioCategorySelected(categorys.get(position));
        }
    };


    public void setCallback(CardapioCallback callback) {
        this.callback = callback;
    }
}

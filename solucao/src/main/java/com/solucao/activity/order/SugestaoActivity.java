package com.solucao.activity.order;

import android.os.Bundle;

import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.domain.OrderBuilder;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import java.util.List;
import roboguice.util.Ln;

public class SugestaoActivity extends AbstractCardapioAcivity  implements CardapioCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_sugestao);
        orderBuilder = new OrderBuilder();
        setListeners();
        createSingleInstanceOrderList();
        setCardapioFragment();
    }

    @Override
    protected void setCardapioFragment() {
        List<Product> products = null;
        try {
            products = productManager.getProducts(1);
            getProductSelectFragment(products,false);
        } catch (Exception e) {
            Ln.e("erro ao pegar susgestões",e);
        }
    }

    private void setListeners() {
        goBack.setOnClickListener(goBackOnClickListener);
    }

    @Override
    public void cardapioProductSelected(Product product) {
        getSupportFragmentManager().beginTransaction().remove(productfragment).commit();
        try {
            product = productManager.getProductWithItens(product.getId());
        } catch (Exception e) {
            Ln.e(e);
        }
        getNextProductFragment(product);
    }

    @Override
    public void goBack(CardapioNavigation navigation) {
        switch (navigation) {
            case CATEGORY:
                break;
            case PRODUCT_GALLERY:
                finish();
                break;
            case PIZZA_MEIO_A_MEIO:
                returnFromPizzaMeioAMeio();
                break;
            case PRODUCT_OPTIONS:
                returnFromProductOption();
                break;
            case PRODUCT_ADITIONAL:
                returnFromProductAditionals();
                break;
            case PRODUCT_COUNT:
                returnFromCount();
                break;
        }
    }

    private void returnFromProductAditionals() {
        Product product = productAditionalFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productAditionalFragment).commit();
        if (hasProducts(product.getItemsOptional())) {
            getProductOptionsFragment(product);
        } else if (hasProducts(product.getItemsMontage())) {
            getPizzaMeioAMeioFragment(product);
        } else {
            setCardapioFragment();
        }
    }

    private void returnFromProductOption() {
        Product product = productOptionsFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productOptionsFragment).commit();
        if (hasProducts(product.getItemsMontage())) {
            getPizzaMeioAMeioFragment(product);
        } else {
            setCardapioFragment();
        }
    }

    private void returnFromPizzaMeioAMeio() {
        getSupportFragmentManager().beginTransaction().remove(productMontageFragment).commit();
        setCardapioFragment();
    }

    private void returnFromCount() {
        Product product;
        product = productCountFragment.getProduct();
        getSupportFragmentManager().beginTransaction().remove(productCountFragment).commit();
        if (hasProducts(product.getItemsAdditional())) {
            getProductAditionalFragment(product);
        } else if (product.getItemsOptional() != null) {
            getProductOptionsFragment(product);
        } else if (product.getItemsMontage() != null) {
            getPizzaMeioAMeioFragment(product);
        } else {
            setCardapioFragment();
        }
    }
}

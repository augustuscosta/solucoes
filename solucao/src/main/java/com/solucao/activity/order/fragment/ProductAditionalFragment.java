package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.adapter.ProductItemAditionalAdapter;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by brunoramosdias on 24/05/14.
 */
public class ProductAditionalFragment extends AbsctractCardapioListFragment {

    private CardapioCallback cardapioCallback;
    private ProductItemAditionalAdapter aditionalAdapter;
    private Product product;
    private List<Product> montagesAdded = new ArrayList<Product>();
    private List<String> countList = new ArrayList<String>();
    private Button addToOrderButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_additional,container,false);
        instatiateView(view);
        return view;
    }

    private void instatiateView(View view) {
        addToOrderButton = (Button) view.findViewById(R.id.addtToOrder);
        addToOrderButton.setOnClickListener(onClickListener);
        FragmentInterfaceHandler.manageLowerBar(view,R.layout.fragment_additional);
        aditionalAdapter = new ProductItemAditionalAdapter(product.getItemsAdditional(),getActivity());
        setListAdapter(aditionalAdapter);
        checkForExistingAditionals();
    }

    private void checkForExistingAditionals() {
        String[] lastcount = cardapioCallback.getLastCountArray();
        if(lastcount!= null){
            aditionalAdapter.setCountArray(lastcount);
        }
    }

    public void setCardapioCallback(CardapioCallback cardapioCallback) {
        this.cardapioCallback = cardapioCallback;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
         addAditionals();
        }
    };

    private void addAditionals() {
        String[] countArray = aditionalAdapter.getCountArray();
        for (int i = 0; i < countArray.length; i++) {
            if(countArray[i] != null && !countArray[i].equals("0")) {
                montagesAdded.add(product.getItemsAdditional().get(i));
                countList.add(countArray[i]);
            }
        }
        cardapioCallback.setLastCountArray(countArray);
        cardapioCallback.addItemAditional(product, montagesAdded, countList);
    };


}

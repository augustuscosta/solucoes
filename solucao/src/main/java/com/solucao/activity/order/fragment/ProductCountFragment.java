package com.solucao.activity.order.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.activity.order.fragment.fragmentcallback.CardapioCallback;
import com.solucao.domain.Product;
import com.solucao.interfacehandlers.FragmentInterfaceHandler;
import com.solucao.util.ConstUtil;

import java.util.List;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public class ProductCountFragment extends AbstractCardapioFragment{

    private Product product;
    private CardapioCallback cardapioCallback;
    private Button addButton;
    private Button removeButton;
    private TextView countText;
    private Button saveOrderButton;
    private TextView productName;
    private TextView productMontage;
    private TextView productOptionals;
    private TextView productAditionals;
    private ImageView touchImageView;

    public ProductCountFragment(Product product) {
        this.product = product;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_count,container,false);
        intantiateViews(view);
        return view;
    }

    private void intantiateViews(View view) {
        FragmentInterfaceHandler.manageLowerBar(view, R.layout.fragment_product_count);
        saveOrderButton = (Button) view.findViewById(R.id.save_order);
        addButton = (Button) view.findViewById(R.id.addButton);
        removeButton = (Button) view.findViewById(R.id.removeButton);
        countText = (TextView) view.findViewById(R.id.productCount);
        productName = (TextView) view.findViewById(R.id.productName);
        productMontage = (TextView) view.findViewById(R.id.productMontage);
        productOptionals = (TextView) view.findViewById(R.id.productOptionals);
        productAditionals = (TextView) view.findViewById(R.id.productAditionals);
        touchImageView = (ImageView) view.findViewById(R.id.touchImage);
        setInfoOnViews();
        setListeners();
    }

    private void setListeners() {
        saveOrderButton.setOnClickListener(onClickAddToOrder);
        addButton.setOnClickListener(onClickAdd);
        removeButton.setOnClickListener(onClickRemove);
    }

    private void setInfoOnViews() {
        productName.setText(product.getName());
        setProductMontage();
        setProductOptionals();
        setProductAditionals();
    }

    private void setProductMontage() {
       final List<Product> toShow = cardapioCallback.getProductMontageAdded(product);
        String toShowString = "";
        if(toShow.isEmpty()){
            toShowString = product.getDescription();
        }else{
            for(Product obj :toShow){
                toShowString += obj.getName()+ ConstUtil.COMMA;
            }
        }

       productMontage.setText(Html.fromHtml(toShowString));
     }

    private void setProductOptionals() {
        final List<Product> toShow = cardapioCallback.getProductOptionalsAdded(product);
        String toShowString = "";
        if(toShow.isEmpty()){
            toShowString = getActivity().getString(R.string.no_optionals);
        }else{
            for(Product obj :toShow){
                toShowString += obj.getName()+ ConstUtil.COMMA;
            }
        }

        productOptionals.setText(toShowString);
    }

    private void setProductAditionals() {
        final List<Product> toShow = cardapioCallback.getProductAditionalsAdded(product);
        final List<String> countList = cardapioCallback.getCountString();
        String toShowString = "";
        if(toShow.isEmpty()){
            toShowString = getActivity().getString(R.string.no_aditionals);
        }else{
            for (int i = 0; i < toShow.size(); i++) {
                toShowString += countList.get(i)+ConstUtil.SPACE+ toShow.get(i).getName()+ ConstUtil.COMMA;
            }
        }

        productAditionals.setText(toShowString);
    }


    public void setCallback(CardapioCallback cardapioCallback) {
        this.cardapioCallback = cardapioCallback;
    }

    public Product getProduct() {
        return product;
    }

    private final View.OnClickListener onClickAdd = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addCount();
        }
    };
    private final View.OnClickListener onClickRemove = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            removeCount();
        }
    };
    private final View.OnClickListener onClickAddToOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int count = Integer.parseInt(countText.getText().toString());
                cardapioCallback.addProductCount(product,count);
        }
    };

    private void addCount(){
        int count = Integer.parseInt(countText.getText().toString());
        count++;
        countText.setText(Integer.toString(count));
    }

    private void removeCount(){
        int count = Integer.parseInt(countText.getText().toString());
        if(count > 1){
            count--;
        }
        countText.setText(Integer.toString(count));
    }

}

package com.solucao.activity.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.solucao.R;
import com.solucao.domain.User;
import com.solucao.util.ConstUtil;
import com.solucao.interfacehandlers.DialogHelper;

/**
 * Created by pierrediderot on 23/03/14.
 * Edited by brunoramosdias on 25/04/14.
 */
public class UserSignUpFragment extends AbstractUserFragment {

    // TODO Ao criar o usuario com sucesso, devemos ignorar o passo de login.
    // Umas vez que o mesmo foi criado, de cara ja podemos considerar o usuario logado.
    // TODO Debater esse fluxo.
    EditText textEmail;
    EditText textSenha;
    EditText textNome;
    EditText textCpf;
    EditText textRg;
    EditText textConfirmaSenha;
    Button buttonSignUp;
    Button buttonGoToSignIn;

    private UserCallback callback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        instatiateViews(rootView);
        return rootView;
    }


    View.OnClickListener onClickSignUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (validateFields()) {
                if (validateSenha()) {
                    User user = new User();
                    user.setEmail(textEmail.getText().toString());
                    user.setCpf(textCpf.getText().toString());
                    user.setName(textNome.getText().toString());
                    user.setRg(textRg.getText().toString());
                    user.setPassword(textSenha.getText().toString());
                    callback.signUp(user);
                } else {
                    DialogHelper.presentError(getSherlockActivity(),
                            getSherlockActivity().getString(R.string.dados_invalidos),
                            getSherlockActivity().getString(R.string.senhas_nao_iguais));
                }
            }else{
                DialogHelper.presentError(getSherlockActivity(),
                        getSherlockActivity().getString(R.string.dados_invalidos),
                        getSherlockActivity().getString(R.string.dados_incompletos));

            }
        }

        private boolean validateFields() {
            return textNome.getText().length() > 0
                    && textEmail.getText().length() > 0
                    && textCpf.getText().length() > 0
                    && textRg.getText().length() > 0
                    && textSenha.getText().length() > 0
                    && textConfirmaSenha.getText().length() > 0;
        }

        private boolean validateSenha() {
            return textSenha.getText().toString().equals(textConfirmaSenha.getText().toString());
        }
    };
    View.OnClickListener onClickGotoSignIn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callback.swapFragment(ConstUtil.SIGNUP_ID);
        }
    };


    private void instatiateViews(View rootView) {
        textSenha = (EditText) rootView.findViewById(R.id.textSenha);
        textEmail = (EditText) rootView.findViewById(R.id.textEmail);
        textNome = (EditText) rootView.findViewById(R.id.textNome);
        textCpf = (EditText) rootView.findViewById(R.id.textCpf);
        textRg = (EditText) rootView.findViewById(R.id.textRg);
        textSenha = (EditText) rootView.findViewById(R.id.textSenha);
        textConfirmaSenha = (EditText) rootView.findViewById(R.id.textConfirmaSenha);
        buttonSignUp = (Button) rootView.findViewById(R.id.buttonSignUp);
        buttonGoToSignIn = (Button) rootView.findViewById(R.id.buttonGoToSignIn);
        buttonSignUp.setOnClickListener(onClickSignUp);
        buttonGoToSignIn.setOnClickListener(onClickGotoSignIn);
    }

    public void setCallback(UserCallback callback) {
        this.callback = callback;
    }
}

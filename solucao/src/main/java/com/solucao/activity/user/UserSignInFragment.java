package com.solucao.activity.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.solucao.R;
import com.solucao.domain.User;
import com.solucao.util.ConstUtil;

/**
 * Created by pierrediderot on 23/03/14.
 * Altered by brunoramosdias on 26/04/14.
 */
public class UserSignInFragment extends AbstractUserFragment {

    UserCallback callback;
    EditText textSenha;
    EditText textLogin;
    Button buttonLogin;
    Button buttonGoToSingnUp;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_usersignin, container, false);
        instantiateViews(rootView);
        return rootView;
    }

    private void instantiateViews(View rootView) {
        textSenha = (EditText) rootView.findViewById(R.id.textSenha);
        textLogin = (EditText) rootView.findViewById(R.id.textEmail);
        buttonLogin = (Button) rootView.findViewById(R.id.buttonLogin);
        buttonGoToSingnUp = (Button) rootView.findViewById(R.id.buttonGoToSignUp);
        buttonLogin.setOnClickListener(onClickLogin);
        buttonGoToSingnUp.setOnClickListener(onClickGotoSignUp);
    }

    public void setCallback(UserCallback callback) {
        this.callback = callback;
    }

    View.OnClickListener onClickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            User user = new User();
            user.setEmail(textLogin.getText().toString());
            user.setPassword(textSenha.getText().toString());
          callback.singIn(user);
        }
    };

    View.OnClickListener onClickGotoSignUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          callback.swapFragment(ConstUtil.SIGNIN_ID);
        }
    };

}

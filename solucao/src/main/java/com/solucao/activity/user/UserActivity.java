package com.solucao.activity.user;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.domain.Setup;
import com.solucao.domain.User;
import com.solucao.interfacehandlers.DialogHelper;
import com.solucao.manager.LoginManager;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.util.ConnectivityHelper;
import com.solucao.util.ConstUtil;

import roboguice.util.Ln;

/**
 * Created by pierrediderot on 23/03/14.
 */
public class UserActivity extends RoboSherlockFragmentActivity implements UserCallback{

    public static final String SIGN_UP_FRAGMENT = "SIGN_UP_FRAGMENT";
    public static final String SIGN_IN_FRAGMENT = "SIGN_IN_FRAGMENT";
    private Setup setup;
    private UserSignInFragment userSigninFragment;
    private UserSignUpFragment userSignUpFragment;
    @Inject
    private LoginManager loginManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_user);
        setup = (Setup)getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
        try {
            loginManager.loadSession();
        } catch (Exception e) {
           Ln.e(e);
        }
        checkIfNewUser();
    }

    private void checkIfNewUser() {
            userSigninFragment = new UserSignInFragment();
            userSigninFragment.setCallback(this);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, userSigninFragment, SIGN_IN_FRAGMENT).commit();
    }

    @Override
    public void signUp(User user) {
        try {
            new SignUpTask(user).execute();
        } catch (Throwable throwable) {
            Log.e(getString(R.string.app_name),"Erro ao logar",throwable);
        }
    }

    @Override
    public void singIn(User user){
        try {
            new SignInTask(user).execute();
        } catch (Throwable throwable) {
            Log.e(getString(R.string.app_name),"Erro ao logar",throwable);
        }
    }

    @Override
    public void swapFragment(int id) {
        switch (id){
            case ConstUtil.SIGNUP_ID:
                getSupportFragmentManager().beginTransaction().remove(userSignUpFragment).commit();
                if(userSigninFragment == null){
                    userSigninFragment = new UserSignInFragment();
                    userSigninFragment.setCallback(this);
                }
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, userSigninFragment, SIGN_IN_FRAGMENT).commit();
                break;

            case ConstUtil.SIGNIN_ID:
                getSupportFragmentManager().beginTransaction().remove(userSigninFragment).commit();
                if(userSignUpFragment == null){
                    userSignUpFragment = new UserSignUpFragment();
                    userSignUpFragment.setCallback(this);
                }
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_content, userSignUpFragment, SIGN_UP_FRAGMENT).commit();

                break;
        }
    }

    class SignUpTask extends AsyncTask<Void,Void,Void>{

        private ProgressDialog dialog;
        private User user;
        private User authenticated;
        private Throwable throwable;

        SignUpTask(User user){
            this.user = user;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(UserActivity.this);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
               authenticated = loginManager.signUp(user);
            } catch (Throwable throwable) {
                this.throwable = throwable;
                Log.e(UserActivity.this.getString(R.string.app_name),"erro ao cadastrar usuario",throwable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.cancel();
            if(authenticated == null){
                if(throwable!= null){
                DialogHelper.presentError(UserActivity.this,"Erro ao cadastrar usuário na aplicação",throwable.getMessage());
                }
            }else{
                try {
                    loginManager.createUniqueUser(authenticated);
                    loginManager.addUserInSession(authenticated);
                } catch (Exception e) {
                    Ln.e(e);
                }
                setResultOk();
            }
            super.onPostExecute(aVoid);
        }
    }

    class SignInTask extends AsyncTask<Void,Void,Void>{

        private ProgressDialog dialog;
        private User user;
        private User authenticated;
        private Throwable throwable;

        SignInTask(User user){
            this.user = user;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(UserActivity.this);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
              authenticated = loginManager.authenticate(user.getEmail(), user.getPassword());
            } catch (Throwable throwable) {
                this.throwable = throwable;
                throwable.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.cancel();
            if(authenticated == null){
                if(throwable!= null){
                    DialogHelper.presentError(UserActivity.this,getString(R.string.login_error),throwable.getMessage());
                }
            }else{
                try {
                    loginManager.createUniqueUser(authenticated);
                    loginManager.addUserInSession(authenticated);
                } catch (Exception e) {
                    Ln.e(e);
                }
                setResultOk();
            }
            super.onPostExecute(aVoid);
        }
    }

    private void setResultOk() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, setup);
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

}

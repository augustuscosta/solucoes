package com.solucao.activity.user;

import com.solucao.domain.User;

/**
 * Created by brunoramosdias on 27/04/14.
 */
public interface UserCallback {

    void signUp(User user);

    void singIn(User user);

    void swapFragment(int id);

}

package com.solucao.activity.user;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.fandas.webclient.serializer.SerializerUtil;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.domain.Session;
import com.solucao.domain.Setup;
import com.solucao.domain.Table;
import com.solucao.manager.LoginManager;
import com.solucao.manager.TableManager;
import com.solucao.util.ConnectivityHelper;
import com.solucao.util.ConstUtil;

import roboguice.util.Ln;

/**
 * Created by pierrediderot on 23/03/14.
 */
public class SetupActivity extends RoboSherlockFragmentActivity {

    //TODO CONNECT Wifi  buged have to test
    @Inject
    private LoginManager loginManager;
    private Setup setup;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//TODO remove emulator test
//        Session  session= new Session("10.0.0.101:8080/solucoes-test-server",1);
//        try {
//            loginManager.createUniqueSession(session);
//        } catch (Exception e) {
//            Ln.e(e);
//        }
//        setDataAndReturn();
        openQrCodeSacanner();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void openQrCodeSacanner() {
        final Intent intent = new Intent(this, ZBarScannerActivity.class);
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE|ONE_D_MODE");
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1:
                switch (resultCode){
                    case RESULT_OK:
                        handleQrCodeResult(data);
                        break;

                    case RESULT_CANCELED:
                        setResult(RESULT_CANCELED);
                        finish();
                        break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void executeQrCodeTask(Intent data){
        parseSetup(data);

        if(setup != null && (setup.getSsid() != null && setup.getPassword()!= null)){
            if(setup.getSsid().equals(ConnectivityHelper.getWifiSSid(this))){
                saveSessionAndExecuteGetTableTask();
                return;
            }
            showDialog();
            connectTowifi();
        }else{
            openCancelDialog();
        }
    }

    private void parseSetup(Intent data) {
        String result = data.getStringExtra(ZBarConstants.SCAN_RESULT);
        try {
             setup = SerializerUtil.fromToObject(result, Setup.class);
        } catch (Throwable throwable) {
            Log.e(getString(R.string.app_name), "erro ao deserializar setup", throwable);
        }
    }

    private void showDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.connecting));
        progressDialog.setIndeterminate(true);
        progressDialog.setOnCancelListener(onCancelListener);
        progressDialog.show();
    }

    private void handleQrCodeResult(Intent data) {
        executeQrCodeTask(data);
    }

    public void reOpenScanner(){
        openQrCodeSacanner();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    private void connectTowifi() {
        checkConnectivity();

    }

    private void checkConnectivity() {
        if (!ConnectivityHelper.isWifiEnabled(this)){
            progressDialog.dismiss();
            openWifiDialog();
        }else{
            WifiConfiguration conf = ConnectivityHelper.getWifiConfiguration(setup.getSsid(), setup.getPassword());
            WifiManager wifiManager =  ConnectivityHelper.getWifiManager(this, conf);
            wifiManager.disconnect();
            wifiManager.enableNetwork(conf.networkId, true);
            wifiManager.reconnect();
            registerReceiver(broadcastReceiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
        }
    }

    private void setDataAndReturn() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, setup);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    DialogInterface.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            openCancelDialog();
        }
    };

    private void openCancelDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.erro));
        d.setMessage(getString(R.string.qrcode_error_wifi));
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        reOpenScanner();
                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                unregisterReceiver(broadcastReceiver);
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo =  intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if((setup != null && networkInfo!= null) && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED){
                if(setup.getSsid() != null && setup.getSsid().equals(ConnectivityHelper.getWifiSSid(SetupActivity.this))){
                    unregisterReceiver(broadcastReceiver);
                    saveSessionAndExecuteGetTableTask();
                }
            }
        }
    };

    private void saveSessionAndExecuteGetTableTask() {
        saveSession();
        setDataAndReturn();
    }

    private void saveSession() {
        Session session = new Session(setup.getServidor(),setup.getLocalId());
        try {
            loginManager.createUniqueSession(session);
        } catch (Exception e) {
            Ln.e(e);
        }
    }

    private void openWifiDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.wifi_disabled));
        d.setMessage(getString(R.string.turn_wifi_on));
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ConnectivityHelper.enableWifi(SetupActivity.this);
                        progressDialog.show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                connectTowifi();
                            }
                        }, 5000);

                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                unregisterReceiver(broadcastReceiver);
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }

}

package com.solucao.activity.table;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.asynctasks.VoidAsyncTask;
import com.solucao.manager.impl.TableManagerImpl;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.interfacehandlers.DialogHelper;

import roboguice.inject.InjectView;

/**
 * Created by brunoramosdias on 27/05/14.
 */
public class GarcomCallActivity extends GenericActivity {

    @InjectView(R.id.chamarGarcon)
    Button button;
    @InjectView(R.id.goBack)
    Button goback;
    @Inject
    TableManagerImpl tableManager;
    private static GarcomCallTask garcomCallTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garcom);
        button.setOnClickListener(onClickListener);
        goback.setOnClickListener(goBackOnClickListener);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_garcom);
    }

    @Override
    protected void onPause() {
        cancellGarcomTask();
        super.onPause();
    }

    class GarcomCallTask extends VoidAsyncTask{

        Exception exception;

        public GarcomCallTask(Context context) {
            super(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage(getString(R.string.chamando_garcom));
            dialog.setOnCancelListener(onCancelListener);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                tableManager.callWaiter(tableManager.getTable().getId());
            } catch (Exception e) {
                exception = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.cancel();
            if(exception == null){
                DialogHelper.presentError(GarcomCallActivity.this,getString(R.string.requisicao_garcom), getString(R.string.garcom_sucess));
            }else{
                DialogHelper.presentError(GarcomCallActivity.this,getString(R.string.requisicao_garcom),getString(R.string.erro_chamar_garcom));
            }
            super.onPostExecute(aVoid);
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        executeGarcomTask();
        }
    };

    private void executeGarcomTask(){
        if(garcomCallTask == null){
            garcomCallTask = new GarcomCallTask(this);
        }

        garcomCallTask.execute();
    }

    private void cancellGarcomTask(){
        if(garcomCallTask != null){
            garcomCallTask.cancel(true);
        }
        garcomCallTask = null;
    }

    View.OnClickListener goBackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    Dialog.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            cancellGarcomTask();
            DialogHelper.presentError(GarcomCallActivity.this, getString(R.string.requisicao_garcom), getString(R.string.erro_chamar_garcom));
        }
    };

}

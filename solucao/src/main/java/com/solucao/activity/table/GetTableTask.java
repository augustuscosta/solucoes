package com.solucao.activity.table;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.appflow.GetTableCallback;
import com.solucao.activity.user.SetupActivity;
import com.solucao.domain.Table;
import com.solucao.manager.TableManager;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 18/06/14.
 */


public class GetTableTask extends AsyncTask<Void,Void,Boolean> {


    @Inject
    private TableManager tableManager;
    private final int id;
    private Table table;
    private ProgressDialog progressDialog;
    private final Context context;
    private final DialogInterface.OnCancelListener onCancelTableListener;
    private final GetTableCallback getTableCallback;

    public GetTableTask(final int id, final Context context,
                        final DialogInterface.OnCancelListener onCancelTableListener,
                        final GetTableCallback getTableCallback) {
        this.id = id;
        this.context = context;
        this.onCancelTableListener = onCancelTableListener;
        this.getTableCallback = getTableCallback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(progressDialog== null){
            progressDialog = new ProgressDialog(context);
        }
        progressDialog.setMessage(context.getString(R.string.buscando_mesa_no_servidor));
        progressDialog.setOnCancelListener(onCancelTableListener);
        if(!progressDialog.isShowing()){
            progressDialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            table =  tableManager.getTableFromNetwork(id);
        } catch (Exception e) {
            Ln.e(e);
        }
        return table!= null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(table == null){
            progressDialog.cancel();
        }else if(table.getReservedDate() !=null){
            getTableCallback.openTableDialog(context.getString(R.string.error_mesa_ocupada));
        }else{
            try {
                tableManager.createUniqueTable(table);
            } catch (Exception e) {
                Ln.e(e);
            }
        }
        progressDialog.dismiss();
    }
}

package com.solucao.activity.table;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.solucao.R;
import com.solucao.asynctasks.VoidAsyncTask;
import com.solucao.domain.Table;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;

import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by brunoramosdias on 30/05/14.
 */
public class TableSelectActivity extends TableSwapTaskCallback {

    private GetTablesTask getTablesTask;
    @InjectView(R.id.spinner)
    Spinner spinner;
    @InjectView(R.id.goBack)
    Button goBack;
    List<Table> tables;
    private String[] tableArrayProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_select);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_table_swap);
        goBack.setOnClickListener(goBackOnClickListener);
        loadTables();
    }


    private void setSpinnerAdapter(){
       tableArrayProvider = new String[tables.size()];
       for (int i = 0; i > tables.size();i++){
           tableArrayProvider[i] = tables.get(i).getNumber().toString();
       }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                TableSelectActivity.this, R.layout.custom_spinner,
                tableArrayProvider);
        adapter.setDropDownViewResource(R.layout.custom_spinner);
        spinner.setAdapter(adapter);
        spinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView,
                                               View view, int position, long l) {
                        if (tables == null || tables.isEmpty()) {
                            return;
                        }

                        swapTable(tables.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        return;
                    }
                });

    }

    class GetTablesTask extends VoidAsyncTask{

        public GetTablesTask(Context context) {
            super(context);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(TableSelectActivity.this);
            dialog.setIndeterminate(true);
            dialog.setMessage(getString(R.string.buscando_mesas));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                tables = tableManager.getTablesFromNetwork(1);
            } catch (Exception e) {
                Log.e(getString(R.string.app_name),"Erro ao pegar mesas do servidor");
            }
            return super.doInBackground(params);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(tables != null && !tables.isEmpty()){
                setSpinnerAdapter();
            }else{
                presentGetTableErrorDialog();
            }
            super.onPostExecute(aVoid);
        }
    }

    private void presentGetTableErrorDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.erro));
        d.setMessage(getString(R.string.erro_mesas_servidor));
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        loadTables();
                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }

    private void loadTables() {
        getTablesTask = new GetTablesTask(this);
        getTablesTask.execute();
    }

    View.OnClickListener goBackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

}

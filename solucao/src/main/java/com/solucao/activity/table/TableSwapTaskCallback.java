package com.solucao.activity.table;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.asynctasks.TableSwapTask;
import com.solucao.domain.Table;
import com.solucao.manager.TableManager;

/**
 * Created by brunoramosdias on 30/05/14.
 */
public class TableSwapTaskCallback extends GenericActivity {

    @Inject
    TableManager tableManager;

    protected TableSwapTask tableSwapTask;

    public void setNewTable(Table newTable) {
        this.newTable = newTable;
    }

    protected Table newTable;

    public void tableResult(Table table){
         if(table == null){
             presentTableErrorDialog(newTable);
         }else{
             presentSuccesDialog();
         }
    }

    public void executeTableTask(Table table){
        newTable = table;
        tableSwapTask = new TableSwapTask(this,this,tableManager,table);
        tableSwapTask.execute();
    }

    public void cancelTableTask(){
        if(tableSwapTask != null){
            tableSwapTask.cancel(true);
        }

        tableSwapTask = null;
    }

    public void presentTableErrorDialog(final Table table){
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.erro));
        d.setMessage(getString(R.string.erro_trocar_mesas));
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        swapTable(table);
                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                cancelTableTask();
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }


    public void presentSuccesDialog(){
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setTitle(getString(R.string.sucesso));
        d.setMessage(getString(R.string.troca_mesa_sucesso));
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                setResult(RESULT_OK);
                finish();
            }
        });
        d.show();
    }

    protected void swapTable(Table table) {
        Table newTable = table;
        tableSwapTask = new TableSwapTask(this,this,tableManager,newTable);
        tableSwapTask.execute();

    }


}

package com.solucao.activity.table;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.fandas.webclient.serializer.SerializerUtil;
import com.solucao.R;
import com.solucao.domain.Setup;
import com.solucao.domain.Table;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.util.ConstUtil;

import roboguice.inject.InjectView;

/**
 * Created by brunoramosdias on 29/05/14.
 */
public class TableSwapActivity extends  TableSwapTaskCallback{

    @InjectView(R.id.selectTable)
    Button selectTable;
    @InjectView(R.id.readTable)
    Button readTable;
    @InjectView(R.id.goBack)
    Button goBack;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_swap);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_table_swap);
        goBack.setOnClickListener(goBackOnClickListener);
        selectTable.setOnClickListener(selectTableOnClickListener);
        readTable.setOnClickListener(readTableOnClickListener);
    }

    private void openQrCodeSacanner() {
        final Intent intent = new Intent(this, ZBarScannerActivity.class);
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE|ONE_D_MODE");
        startActivityForResult(intent, ConstUtil.QR_CODE_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ConstUtil.QR_CODE_SCAN:
                if(resultCode == RESULT_OK){
                    handleQRCodeResut(data);
                }
                break;

            case ConstUtil.TABLE_SELECT:
                if(resultCode == RESULT_OK){
                    finish();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleQRCodeResut(Intent data) {
        String result = data.getStringExtra(ZBarConstants.SCAN_RESULT);
        try {
            Setup setup = SerializerUtil.fromToObject(result, Setup.class);
            Table table = new Table();
            table.setId(setup.getTableId());
            executeTableTask(table);
        } catch (Throwable throwable) {
            Log.e(getString(R.string.app_name), "erro ao pegar setup do qrCode", throwable);
        }
    }

    View.OnClickListener goBackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    View.OnClickListener readTableOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openQrCodeSacanner();
        }
    };

    View.OnClickListener selectTableOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(TableSwapActivity.this, TableSelectActivity.class), ConstUtil.TABLE_SELECT);
        }
    };

}

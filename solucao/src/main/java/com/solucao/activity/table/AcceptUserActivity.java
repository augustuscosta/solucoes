package com.solucao.activity.table;

import android.accounts.NetworkErrorException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.fandas.webclient.serializer.SerializerUtil;
import com.solucao.R;
import com.solucao.activity.generic.GenericListActivity;
import com.solucao.activity.generic.TableConfirmationCallback;
import com.solucao.adapter.AcceptUserAdapter;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.manager.LoginManager;
import com.solucao.service.SyncService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import roboguice.RoboGuice;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 7/18/14.
 */
public class AcceptUserActivity extends GenericListActivity implements TableConfirmationCallback{

    @InjectView(R.id.goBack)
    private Button goBack;
    private List<AuthorizationRequestUser> authorizationRequestUsers;
    private AcceptUserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_accept_requests);
        String requests = getIntent().getStringExtra(SyncService.INTENT_AUTHORIZATION_REQUEST);
        try {
            authorizationRequestUsers = Arrays.asList(SerializerUtil.fromToObject(requests, AuthorizationRequestUser[].class));
        } catch (Throwable throwable) {
            Ln.e(throwable,"erro ao deserializar requests");
        }
        goBack.setOnClickListener(onClickGoBack);
        super.onCreate(savedInstanceState);
    }

    private void setAdapter(){
        adapter = new AcceptUserAdapter(authorizationRequestUsers,this,this);
        setListAdapter(adapter);
    }

    @Override
    protected void notifyNewRequests(Intent intent) {
        String requests = intent.getStringExtra(SyncService.INTENT_AUTHORIZATION_REQUEST);

        List<AuthorizationRequestUser> requestUsers = null;
        try {
            requestUsers = Arrays.asList(SerializerUtil.fromToObject(requests, AuthorizationRequestUser[].class));
        } catch (Throwable throwable) {
            Ln.e(throwable,"erro ao deserializar requests");
        }
        authorizationRequestUsers.addAll(requestUsers);
        setAdapter();
    }

    @Override
    public void answer(final AuthorizationRequestUser requestUser) {
        requestUser.setAccept(true);
        try {
            RoboGuice.getInjector(this).getInstance(LoginManager.class).answerAuthorizationRequest(requestUser);
        } catch (Exception e) {
            Ln.e(e, "erro ao responder usuario");
        }
        authorizationRequestUsers = adapter.getAuthorizationRequestUsers();
        if(authorizationRequestUsers == null || authorizationRequestUsers.isEmpty()){
            finish();
        }
    }

    private final View.OnClickListener onClickGoBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}

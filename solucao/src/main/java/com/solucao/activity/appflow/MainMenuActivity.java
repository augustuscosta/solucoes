package com.solucao.activity.appflow;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.activity.generic.GenericActivity;
import com.solucao.activity.order.CardapioActivity;
import com.solucao.activity.order.DecisionDialogCallback;
import com.solucao.activity.order.OrderBillActivity;
import com.solucao.activity.order.PedidosActivity;
import com.solucao.activity.order.SugestaoActivity;
import com.solucao.activity.table.GarcomCallActivity;
import com.solucao.activity.table.GetTableTask;
import com.solucao.activity.table.TableSwapActivity;
import com.solucao.asynctasks.VoidAsyncTask;
import com.solucao.domain.Setup;
import com.solucao.interfacehandlers.DialogHelper;
import com.solucao.manager.MenuManager;
import com.solucao.manager.impl.MenuManagerImpl;
import com.solucao.interfacehandlers.AcitivityInterfaceHandler;
import com.solucao.util.ConstUtil;

import roboguice.inject.InjectView;

/**
 * Created by brunoramosdias on 25/04/14.
 */
public class MainMenuActivity extends GenericActivity implements GetTableCallback,DecisionDialogCallback {

    @Inject
    MenuManager menuManager;
    @InjectView(R.id.cardapio)
    LinearLayout cardapio;
    @InjectView(R.id.meusPedidos)
    LinearLayout meusPedidos;
    @InjectView(R.id.sugestao)
    LinearLayout sugestao;
    @InjectView(R.id.pedirConta)
    LinearLayout pedirConta;
    @InjectView(R.id.chamarGarcon)
    LinearLayout chamarGarcom;
    @InjectView(R.id.trocarMesa)
    LinearLayout trocarMesa;
    private MenuTask menuTask;
    private GetTableTask getTableTask;
    private Setup setup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AcitivityInterfaceHandler.setIconAndTitle(this, R.layout.activity_main);
        setup = (Setup) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
        setListeners();
        loadMenu();
    }

    private void setListeners() {
        cardapio.setOnClickListener(onClickCardapio);
        meusPedidos.setOnClickListener(onClickMeusPedidos);
        sugestao.setOnClickListener(onClickSugestao);
        pedirConta.setOnClickListener(onClickPedirConta);
        chamarGarcom.setOnClickListener(onClickChamarCarcon);
        trocarMesa.setOnClickListener(onClickTrocarMesa);
    }

    private View.OnClickListener onClickCardapio = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, CardapioActivity.class));
        }
    };

    private View.OnClickListener onClickMeusPedidos = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, PedidosActivity.class));

        }
    };

    private View.OnClickListener onClickSugestao = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, SugestaoActivity.class));
        }
    };

    private View.OnClickListener onClickPedirConta = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(MainMenuActivity.this, OrderBillActivity.class),ConstUtil.ORDER_BILL);
        }
    };

    private View.OnClickListener onClickChamarCarcon = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, GarcomCallActivity.class));
        }
    };

    private View.OnClickListener onClickTrocarMesa = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, TableSwapActivity.class));
        }
    };

    @Override
    protected void onPause() {
        cancelTableTask();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ConstUtil.ORDER_BILL:
                switch (requestCode){
                    case RESULT_OK:
                    setResult(RESULT_OK);
                    finish();
                   break;

                    case RESULT_CANCELED:

                    break;

                    default:
                    break;
                }

                default:
                    break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void cancelTableTask() {
        if(getTableTask!= null){
            getTableTask.cancel(true);
            getTableTask = null;
        }
    }

    @Override
    public void accept() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void cancel() {

    }


    class MenuTask extends VoidAsyncTask {
        ProgressDialog dialog;
        Throwable throwable;

        public MenuTask(Context context) {
            super(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage(getString(R.string.processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                menuManager.retrieveAndSaveMenuFromNetwork();
            } catch (Throwable throwable) {
                this.throwable = throwable;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.cancel();
            if (throwable != null) {
                presentErrorDialog();
            }
            super.onPostExecute(aVoid);
        }
    }

    private void presentErrorDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.erro_menu));
        d.setMessage(getString(R.string.erro_menu_message));
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        loadMenu();
                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }
    private void loadMenu() {
       menuTask = new MenuTask(this);
       menuTask.execute();
    }


    @Override
    public void onBackPressed() {
        DialogHelper.presentDecisionDialog(this,getString(R.string.quit_title), getString(R.string.quit_message),this);

    }

    @Override
    public void openTableDialog(String message) {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setCancelable(false);
        d.setTitle(getString(R.string.erro));
        d.setMessage(message);
        d.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                }
        );
        d.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                loadMenu();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }

    @Override
    public void getTableSucess() {

    }

    private void executeTableTask(final Integer i) {
        if(i!= null){
            getTableTask =  new GetTableTask(i,this,onCancelTableListener,this);
            getTableTask.execute();
        }
    }

    private DialogInterface.OnCancelListener onCancelTableListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            openTableDialog(getString(R.string.error_mesa_ocupada));
        }
    };
}

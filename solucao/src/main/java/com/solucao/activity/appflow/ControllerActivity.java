package com.solucao.activity.appflow;

import android.content.Intent;
import android.os.Bundle;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.solucao.activity.user.SetupActivity;
import com.solucao.activity.user.UserActivity;
import com.solucao.domain.Setup;
import com.solucao.util.ConstUtil;

public class ControllerActivity extends RoboSherlockFragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, SetupActivity.class);
        startActivityForResult(intent, ConstUtil.SETUP_REQUEST);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstUtil.USER_REQUEST:
                switch (resultCode) {
                    case RESULT_OK:
                        Intent intent = new Intent(this, MainMenuActivity.class);
                        if(data.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
                            intent.putExtras(data.getExtras());
                        }
                        startActivityForResult(intent, ConstUtil.MAIN_MENU);
                        break;

                    case RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
            case ConstUtil.SETUP_REQUEST:
                switch (resultCode) {
                    case RESULT_OK:
                        Intent intent = new Intent(this, UserActivity.class);
                        if(data.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
                            intent.putExtras(data.getExtras());
                        }
                        startActivityForResult(intent, ConstUtil.USER_REQUEST);
                        break;

                    case RESULT_CANCELED:
                        finish();
                        break;
                }
                break;

            case ConstUtil.MAIN_MENU:
                Intent intent = new Intent(this, UserActivity.class);
                startActivityForResult(intent, ConstUtil.USER_REQUEST);
                break;

            default:
                break;
        }
    }
}

package com.solucao.activity.appflow;

/**
 * Created by brunoramosdias on 18/06/14.
 */
public interface GetTableCallback {

        void openTableDialog(String Message);
        void getTableSucess();

}

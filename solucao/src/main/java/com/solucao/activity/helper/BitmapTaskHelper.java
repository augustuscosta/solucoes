package com.solucao.activity.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 01/06/14.

 * @author pierrediderot - Modificando classe para funcionar asicronamente carregando uma imagem a partir de URL.
 */
public class BitmapTaskHelper extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<ImageView> imageViewReference;

    public BitmapTaskHelper(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Bitmap doInBackground(String... imageUrl) {
        if ( imageUrl == null || imageUrl.length == 0 ) {
            return null;
        }

        InputStream imageStream = null;
        try {
            final URL url = new URL(imageUrl[0]);
            imageStream = url.openConnection().getInputStream();

            return BitmapFactory.decodeStream(imageStream);
        } catch (IOException e) {
            Ln.e(e, "Erro ao requisitar imagens.");
        } finally {
            try {
                imageStream.close();
            } catch (IOException e) {
                Ln.e(e, "Erro ao fechar o stream para baixar imagens.");
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(final Bitmap bitmap) {
        if ( imageViewReference != null && bitmap != null ) {
            final ImageView imageView = imageViewReference.get();
            if ( imageView != null ) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

}

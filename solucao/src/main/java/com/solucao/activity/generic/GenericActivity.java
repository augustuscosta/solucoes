package com.solucao.activity.generic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.WindowManager;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.solucao.R;
import com.solucao.activity.order.PedidosActivity;
import com.solucao.activity.table.AcceptUserActivity;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.service.SyncService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pierrediderot on 20/03/14.
 */
public class GenericActivity extends RoboSherlockFragmentActivity{

    /*
        TODO Injete aqui os helpers e utils que forem compartilhados(usados) pelos filhos como o exemplo abaixo:

        @Inject
        private DialogUtil dialogUtil;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustSoftInputMode();
    }

    private void adjustSoftInputMode() {
        final boolean largeScreen = getResources().getBoolean(R.bool.isLargeScreen);
        if ( largeScreen ) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } else {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(SyncService.INTENT_AUTHORIZATION_REQUEST));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    public void goToPrevious(){

    }

    public void viewOrders(){
     startActivity(new Intent(this, PedidosActivity.class));
    }


    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            notifyNewRequests(intent);
        }
    };

    protected void notifyNewRequests(Intent intent) {
        String requestUser = intent.getStringExtra(SyncService.INTENT_AUTHORIZATION_REQUEST);
        Intent intentToGo = new Intent(this, AcceptUserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(SyncService.INTENT_AUTHORIZATION_REQUEST,  requestUser);
        intentToGo.putExtras(bundle);
        startActivity(intentToGo);
    }


}

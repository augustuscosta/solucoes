package com.solucao.activity.generic;

import com.solucao.domain.AuthorizationRequestUser;

/**
 * Created by brunoramosdias on 7/16/14.
 */
public interface TableConfirmationCallback {

    void answer(final AuthorizationRequestUser requestUser);
}

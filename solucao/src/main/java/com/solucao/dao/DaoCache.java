package com.solucao.dao;

import android.annotation.SuppressLint;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.inject.Inject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.solucao.database.DatabaseHelper;

import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

/**
 * Created by pierrediderot on 03/02/14.
 */
public class DaoCache {

    @Inject
    private DatabaseHelper databaseHelper;

    public static final int CONCURRENCY_LEVEL = 10;

    private LoadingCache<Class, Dao> daoCache;
    private LoadingCache<Class, QueryBuilder> queryCache;
    private LoadingCache<Class, DeleteBuilder> deleteCache;
    private LoadingCache<Class, UpdateBuilder> updateCache;

    @Inject
    public DaoCache(){
        daoCache = CacheBuilder.newBuilder().weakValues().concurrencyLevel(CONCURRENCY_LEVEL).build(daoLoader);
        queryCache = CacheBuilder.newBuilder().weakValues().concurrencyLevel(CONCURRENCY_LEVEL).build(queryLoader);
        deleteCache = CacheBuilder.newBuilder().weakValues().concurrencyLevel(CONCURRENCY_LEVEL).build(deleteLoader);
        updateCache = CacheBuilder.newBuilder().weakValues().concurrencyLevel(CONCURRENCY_LEVEL).build(updateLoader);
    }

    @SuppressLint("NewApi")
    public <T> Dao<T, Integer> getDao(Class<T> clazz) throws SQLException {
        try {
            return daoCache.get(clazz);
        } catch (ExecutionException e) {
            throw new SQLException(e);
        }
    }

    @SuppressLint("NewApi")
    public <T> QueryBuilder<T, Integer> getQueryBuilder(Class<T> clazz) throws SQLException {
        try {
            final QueryBuilder<T, Integer> queryBuilder = queryCache.get(clazz);
            queryBuilder.clear();
            return queryBuilder;
        } catch (ExecutionException e) {
            throw new SQLException(e);
        }
    }

    @SuppressLint("NewApi")
    public <T> DeleteBuilder<T, Integer> getDeleteBuilder(Class<T> clazz) throws SQLException {
        try {
            final DeleteBuilder<T, Integer> deleteBuilder =  deleteCache.get(clazz);
            deleteBuilder.clear();
            return deleteBuilder;
        } catch (ExecutionException e) {
            throw new SQLException(e);
        }
    }

    @SuppressLint("NewApi")
    public <T> UpdateBuilder<T, Integer> getUpdateBuilder(Class<T> clazz) throws SQLException {
        try {
            final UpdateBuilder<T, Integer> updateBuilder =  updateCache.get(clazz);
            updateBuilder.clear();
            return updateBuilder;
        } catch (ExecutionException e) {
            throw new SQLException(e);
        }
    }

    private final CacheLoader<Class, Dao> daoLoader = new CacheLoader<Class, Dao>() {
        @Override
        public Dao load(Class clazz) throws Exception {
            return databaseHelper.getDao(clazz);
        }
    };

    private final CacheLoader<Class, QueryBuilder> queryLoader = new CacheLoader<Class, QueryBuilder>() {
        @Override
        public QueryBuilder load(Class clazz) throws Exception {
            return getDao(clazz).queryBuilder();
        }
    };

    private final CacheLoader<Class, DeleteBuilder> deleteLoader = new CacheLoader<Class, DeleteBuilder>() {
        @Override
        public DeleteBuilder load(Class clazz) throws Exception {
            return getDao(clazz).deleteBuilder();
        }
    };

    private final CacheLoader<Class, UpdateBuilder> updateLoader = new CacheLoader<Class, UpdateBuilder>() {
        @Override
        public UpdateBuilder load(Class clazz) throws Exception {
            return getDao(clazz).updateBuilder();
        }
    };


}

package com.solucao.qrcode;

/**
 * Created by brunoramosdias on 12/05/14.
 */
public class QrCodeDecryptUtil {

    private static String PANTERA_KEY = "12371233144";

/*
    final Calendar c = Calendar.getInstance();
    mYear = c.get(Calendar.YEAR);
    mMonth = c.get(Calendar.MONTH);
    mDay = c.get(Calendar.DAY_OF_MONTH);
  */

    public static String ReverseStr(String s) {
        String res = "";
        for (int i = s.length(); i >= 1; i--) {
            res = res + s.substring(i,i+1); //StringsFunc.substring(s, i, 1);
        }


        return res;
    }

    private static String PlainText(String S, boolean KillControlChars) {
        int i, J;
        String AllowedChars;
        AllowedChars = "";

        String Result = S;
        for (i = 0; i < Result.length(); i++ ) {
            if (!(Result.charAt(i) >= ' ' && Result.charAt(i) <= '~')) {
                J = AllowedChars.length() - 1;

                if (J < 1) {
                    if (Result.charAt(i) >= '¿' && Result.charAt(i) <= '∆')
                        Result = Result.replace(Result.charAt(i), 'A');
                    else if (Result.charAt(i) == '«')
                        Result = Result.replace(Result.charAt(i), 'C');
                    else if (Result.charAt(i) >= '»' && Result.charAt(i) <= 'À')
                        Result = Result.replace(Result.charAt(i), 'E');
                    else if (Result.charAt(i) >= 'Ã' && Result.charAt(i) <= 'œ')
                        Result = Result.replace(Result.charAt(i), 'I');
                    else if (Result.charAt(i) == '—')
                        Result = Result.replace(Result.charAt(i), 'N');
                    else if (Result.charAt(i) >= '“' && Result.charAt(i) <= 'ÿ')
                        Result = Result.replace(Result.charAt(i), 'O');
                    else if (Result.charAt(i) >= 'Ÿ' && Result.charAt(i) <= '‹')
                        Result = Result.replace(Result.charAt(i), 'U');
                    else if (Result.charAt(i) == '›')
                        Result = Result.replace(Result.charAt(i), 'Y');
                    else if (Result.charAt(i) >= '‡' && Result.charAt(i) <= 'Ê')
                        Result = Result.replace(Result.charAt(i), 'a');
                    else if (Result.charAt(i) == 'Á')
                        Result = Result.replace(Result.charAt(i), 'c');
                    else if (Result.charAt(i) >= 'Ë' && Result.charAt(i) <= 'Î')
                        Result = Result.replace(Result.charAt(i), 'e');
                    else if (Result.charAt(i) >= 'Ï' && Result.charAt(i) <= 'Ô')
                        Result = Result.replace(Result.charAt(i), 'i');
                    else if (Result.charAt(i) == 'Ò')
                        Result = Result.replace(Result.charAt(i), 'n');
                    else if (Result.charAt(i) >= 'Ú' && Result.charAt(i) <= 'ˆ')
                        Result = Result.replace(Result.charAt(i), 'o');
                    else if (Result.charAt(i) >= '˘' && Result.charAt(i) <= '¸')
                        Result = Result.replace(Result.charAt(i), 'u');
                    else if (Result.charAt(i) == '˝')
                        Result = Result.replace(Result.charAt(i), 'y');
                    else if (Result.charAt(i) == '∞')
                        Result = Result.replace(Result.charAt(i), 'o');
                    else if (Result.charAt(i) == '™')
                        Result = Result.replace(Result.charAt(i), 'a');
                    else if (Result.charAt(i) == 'ª')
                        Result = Result.replace(Result.charAt(i), '>');
                    else if (Result.charAt(i) == '´')
                        Result = Result.replace(Result.charAt(i), '<');
                    else if (Result.charAt(i) > ' ' || KillControlChars)
                        Result = Result.replace(Result.charAt(i),' ');
                }
            }
        }
        return Result;
    }

    private static String encodeStr255(String S, String K) {
        byte JK, JS;
        int Lk;
        char C;
        String C2;

        if (K == "")
            K = PANTERA_KEY;
        String Result = "";
        try {
            Lk = K.length();
            for (int i = 0; i < S.length(); i++) {
                C2 = "" + (K.charAt((i % Lk) + 1));
                C2 = C2.toUpperCase();
                C2 = PlainText(C2, true);
                C = C2.charAt(0);
                if (C >= '0' && C <= '9')
                    JK = (byte)(((int)(C)) - ((int)('0')));
                else if (C >= 'A' && C <= 'Z')
                    JK = (byte)(((int)C) - ((int)'A') + 10);
                else
                    JK = 0;

                JS = (byte)((int)(S.charAt(i)));
                if ((JK ^ JS) == 0)
                    throw new Exception("Caractere zero encontrado na criptografia.");
                Result += (char)(JK ^ JS);
            }
        } catch (Exception e) {
            Result = "";
        }
        return Result;
    }

    /*

    Bruno, esta È o mÈtodo que vocÍ ir· utilizar.
    Invoque o mÈtodo passando a vari·vel key vazia:

    s = criptoStr255(s, '');

    O mÈtodo criptografa e descriptografa.  Se passar a string criptografada
    para o mÈtodo ele reverte a criptografia:

    s = 'bruno';
    s = criptoStr255(s, '');
    // s = '0f$7%y'
    s = criptoStr255(s, '');
    // s = 'bruno'

     */
    public static String criptoStr255(String S, String Key) {
        return encodeStr255(ReverseStr(encodeStr255(S, Key)), Key);
    }

}

package com.solucao.interfacehandlers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.solucao.R;
import com.solucao.activity.generic.TableConfirmationCallback;
import com.solucao.activity.order.DecisionDialogCallback;
import com.solucao.domain.AuthorizationRequestUser;

/**
 * Created by brunoramosdias on 04/06/14.
 */
public class DialogHelper {

    public static void presentError(Context context, String title, String description) {
        AlertDialog.Builder d = new AlertDialog.Builder(context);
        d.setTitle(title);
        d.setMessage(description);
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        d.show();
    }

    public static void presentDecisionDialog(Activity context, String title, String message, final DecisionDialogCallback callback) {
        final AlertDialog.Builder d = new AlertDialog.Builder(context);
        d.setCancelable(false);
        d.setTitle(title);
        d.setMessage(message);
        d.setPositiveButton(context.getString(R.string.sim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.accept();
                    }
                }
        );
        d.setNegativeButton(context.getString(R.string.nao), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
               callback.cancel();
            }
        });
        d.setIcon(android.R.drawable.ic_dialog_alert);
        d.show();
    }
}

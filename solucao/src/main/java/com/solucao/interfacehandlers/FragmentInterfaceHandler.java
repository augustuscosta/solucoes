package com.solucao.interfacehandlers;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.solucao.R;

/**
 * Created by brunoramosdias on 08/06/14.
 */
public class FragmentInterfaceHandler {

    public static void manageLowerBar(View view, int layoutId){
        final LinearLayout lowerBar = (LinearLayout) view.findViewById(R.id.lowerBar);
        final Button ordersButton = (Button) lowerBar.findViewById(R.id.orders);
        final Button saveOrderButton = (Button) lowerBar.findViewById(R.id.save_order);
        final Button addToOrdersButton = (Button) lowerBar.findViewById(R.id.addtToOrder);

        switch (layoutId){
            case(R.layout.fragment_category):
                saveOrderButton.setVisibility(View.GONE);
                addToOrdersButton.setVisibility(View.GONE);
                break;
            case(R.layout.fragment_products):
                saveOrderButton.setVisibility(View.GONE);
                addToOrdersButton.setVisibility(View.GONE);
                break;
            case(R.layout.fragment_pizza_meio_ameio):
                saveOrderButton.setVisibility(View.GONE);
                addToOrdersButton.setVisibility(View.VISIBLE);
                break;
            case(R.layout.fragment_options):
                saveOrderButton.setVisibility(View.GONE);
                addToOrdersButton.setVisibility(View.VISIBLE);
                break;
            case(R.layout.fragment_additional):
                saveOrderButton.setVisibility(View.GONE);
                addToOrdersButton.setVisibility(View.VISIBLE);
                break;
            case(R.layout.fragment_product_count):
                saveOrderButton.setVisibility(View.VISIBLE);
                addToOrdersButton.setVisibility(View.GONE);
                break;
            case(R.layout.fragment_product_detail):
                saveOrderButton.setVisibility(View.VISIBLE);
                addToOrdersButton.setVisibility(View.GONE);
                break;
        }
    }
}

package com.solucao.interfacehandlers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Category;
import com.solucao.domain.Product;

/**
 * Created by brunoramosdias on 26/04/14.
 */
public class AcitivityInterfaceHandler {

    /**
     * Metodo para mudar titulo e icone da barra superior nas activitys, manuseamento da barra inferior
     */


    public static void setIconAndTitle(Activity context, int layoutId) {
        final LinearLayout upperBar = (LinearLayout) context.findViewById(R.id.upperBar);
        final LinearLayout lowerBar = (LinearLayout) context.findViewById(R.id.lowerBar);
        Button orderButton= null;
        Button saveOrderButton = null;
        Button addToOrder  = null;
        if ( upperBar == null ){
            return;
        }
            final ImageView imageView = (ImageView) upperBar.findViewById(R.id.activityIcon);
            final TextView textView = (TextView) upperBar.findViewById(R.id.activityTitle);


        if(lowerBar!= null){
            orderButton = (Button) lowerBar.findViewById(R.id.orders);
            saveOrderButton = (Button) lowerBar.findViewById(R.id.save_order);
            addToOrder = (Button) lowerBar.findViewById(R.id.addtToOrder);
         }


        switch (layoutId) {
            case R.layout.activity_cardapio:
                textView.setText(context.getString(R.string.cardapio));
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.cardapio));
                break;
            case R.layout.activity_pedidos:
                textView.setText(context.getString(R.string.meus_pedidos));
                orderButton.setVisibility(View.VISIBLE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.GONE);
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.meus_pedidos));
                break;
            case R.layout.activity_user:
                textView.setText(context.getString(R.string.solucao_restaurante));
                orderButton.setVisibility(View.GONE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.GONE);
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.cardapio));
                break;
            case R.layout.activity_main:
                textView.setText(context.getString(R.string.solucao_restaurante));
                orderButton.setVisibility(View.GONE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.GONE);
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.cardapio));
                break;
            case R.layout.activity_garcom:
                textView.setText(context.getString(R.string.garcom));
                orderButton.setVisibility(View.GONE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.GONE);
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.chamar_garcon));
                break;
            case R.layout.activity_table_swap:
                textView.setText(context.getString(R.string.selecionar_mesa));
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.trocar_mesa));
                orderButton.setVisibility(View.GONE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.GONE);
                break;
            case R.layout.activity_sugestao:
                textView.setText(context.getString(R.string.sugestao));
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.sugestao));
                break;
            case R.layout.activity_orderbill:
                textView.setText(context.getString(R.string.ask_bill));
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.pedir_conta));
                orderButton.setVisibility(View.GONE);
                saveOrderButton.setVisibility(View.GONE);
                addToOrder.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
    }
}

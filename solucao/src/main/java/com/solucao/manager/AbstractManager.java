package com.solucao.manager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.fandas.webclient.AbstractCrudRequest;
import com.fandas.webclient.serializer.SerializerUtil;
import com.google.inject.Inject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.solucao.dao.DaoCache;
import com.solucao.domain.Session;
import com.solucao.exception.ConnectionNoAvailableException;
import com.solucao.httpclient.DefaultHttpClientBuilder;
import com.solucao.manager.network.NetworkServices;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.sql.SQLException;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpUriRequest;
import ch.boye.httpclientandroidlib.protocol.HTTP;
import roboguice.util.Ln;

public class AbstractManager<BaseType> extends AbstractCrudRequest<BaseType> {

    @Inject
    private DaoCache daoCache;

	@Inject
	private ConnectivityManager connectivityManager;

	@Inject
	protected Context context;

	@Inject
	private LoginManager loginManager;

	@Inject
	private DefaultHttpClientBuilder builderHttpClient;

	private Class<BaseType[]> mClassTypeArray;

	@SuppressWarnings("unchecked")
	public AbstractManager(Class<BaseType> type) {
		super(type);
		mClassTypeArray = (Class<BaseType[]>) Array.newInstance(type, 0).getClass();
	}

	@SuppressWarnings("unchecked")
	public AbstractManager(Class<BaseType> type, String serviceName) {
		super(type, serviceName);
		mClassTypeArray = (Class<BaseType[]>) Array.newInstance(type, 0).getClass();
	}

	@Override
	protected HttpClient getHttpClient() {
		return builderHttpClient.build();
	}

	@Override
	protected String getServer() {
		return Session.isActive() ? Session.getSession().getServerUrl() : null;
	}

	@Override
	public <ReturnType> ReturnType post(String request, Object example, Class<ReturnType> returnClazz) throws Throwable {
		Ln.d("Sending Post Request - " + request + " \nwith body\n" + example);
		return super.post(request, example, returnClazz);
	}

	@Override
	public HttpResponse post(String requestService, String requestBody) throws Throwable {
		Ln.d("Sending Post Request - " + requestService + " \nwith body\n" + requestBody);
		return super.post(requestService, requestBody);
	}

	@Override
	public HttpResponse post(String requestService, HttpEntity entity) throws Throwable {
		Ln.d("Sending Post Request - " + requestService);
		return super.post(requestService, entity);
	}

	@Override
	public HttpResponse put(String requestService, String requestBody) throws Throwable {
		Ln.d("Sending Put Request - " + requestService + " \nwith body\n" + requestBody);
		return super.put(requestService, requestBody);
	}

	@Override
	public HttpResponse get(String requestService) throws Throwable {
		Ln.d("Sending Get Request - " + requestService);
		return super.get(requestService);
	}

	public BaseType[] findByCriteria(final BaseType example, final int fistResult, final int maxResults) throws Throwable {
		return post(getServiceName()+"/filter?criteria=" + getCriteria(fistResult, maxResults), example, mClassTypeArray);
	}

	private String getCriteria(int fistResult, int maxResults) throws Throwable {
		// TODO Refatorar assim que possivel.
        try {
			return URLEncoder.encode("{\"filters\":[\"*\"],\"firstResult\":" + fistResult + ",\"maxResults\":"+ maxResults +"}", HTTP.UTF_8);
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		}
	}

	public <ReturnType> ReturnType get(final String requestService, final Class<ReturnType> clasz) throws Throwable {
		final HttpResponse response = get(requestService);

		try {
			return deserializeResponse(response, clasz);
		} catch (Exception e) {
			final String msg = "Error deserializing response to : " + clasz.getSimpleName();
			Ln.e(e, msg);
			throw new Throwable(msg, e);
		}
	}

    @Override
    protected HttpResponse execute(HttpUriRequest request) throws Throwable {
        checkIsConnectedAvailable();
        return super.execute(request);
    }

    public void checkIsConnectedAvailable() throws ConnectionNoAvailableException {
		final NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
		if ( activeNetwork == null || !activeNetwork.isConnected() ) {
			throw new ConnectionNoAvailableException("No internet connection available.");
		}
	}

	public String buildURL(final Object object, final String objectId, final NetworkServices service) {
		return object.getClass().getSimpleName() + "/" + objectId + "/" + service.getPath();
	}

	public String buildURL(final Object object, final NetworkServices service) {
		return buildURL(object, service.getPath());
	}

	public String buildURL(final Object object, final String service) {
		return object.getClass().getSimpleName() + "/" + service;
	}

	public String serializeToJson(final Object object) {
		try {
			return SerializerUtil.objectToString(object);
		} catch (Throwable e) {
            Ln.e(e, "Error trying serialize Object to Json");
        }

        return null;
	}

	@Override
	protected boolean authenticate() {
		try {
			loginManager.reAuthenticate();
			return true;
		} catch (Throwable e) {
			Ln.e(e, "Error to authenticate user");
			return false;
		}
	}

	@Override
	protected boolean isSuccess(HttpResponse response) {
		return super.isSuccess(response) && (response.getEntity() != null);
	}

    protected <T> Dao<T, Integer> getDao(Class<T> clazz) throws SQLException {
        return daoCache.getDao(clazz);
    }

    protected <T> QueryBuilder<T, Integer> getQueryBuilder(Class<T> clazz) throws SQLException {
        return daoCache.getQueryBuilder(clazz);
    }

    protected <T> DeleteBuilder<T, Integer> getDeleteBuilder(Class<T> clazz) throws SQLException {
        return daoCache.getDeleteBuilder(clazz);
    }

    protected <T> UpdateBuilder<T, Integer> getUpdateBuilder(Class<T> clazz) throws SQLException {
        return daoCache.getUpdateBuilder(clazz);
    }

    public Class<BaseType[]> getClassTypeArray() {
        return mClassTypeArray;
    }
}

package com.solucao.manager;

import com.solucao.domain.Order;

import java.util.List;

/**
 * Created by brunoramosdias on 17/05/14.
 */
public interface OrderManager {

    List<Order> getPendingOrders() throws Exception;

    List<Order> getWaitingApprovalOrders() throws Exception;

    List<Order> getAll() throws Exception;

    void save(Order order) throws Exception;

    float getTotal(List<Order> orders);

    void sendPendingOrders() throws Exception;

    void sendOrder(Order order) throws Exception;

    void synchronizeOrdersWaitingApproval() throws Exception;

    List<Order> orderBill(int tableId) throws Exception;
}

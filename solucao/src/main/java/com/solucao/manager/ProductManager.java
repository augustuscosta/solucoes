package com.solucao.manager;

import com.solucao.domain.Category;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;

import java.util.List;

/**
 * Created by brunoramosdias on 17/05/14.
 */
public interface ProductManager {

    Product getProduct(int id);

    List<Product> getProducts(Category category) throws Exception;

    List<Product> getProducts(int categoryId) throws Exception;

    Product getProductWithItens(int id) throws Exception;

    void save(Product product) throws Exception;

    void save(List<Product> products) throws Exception;

    void saveProductItemAdditionals(List<ProductItemAdditional> productItemAdditionals) throws Exception;

    void saveProductItemAdditional(ProductItemAdditional productItemAdditional) throws Exception;

    void saveProductItemMontages(List<ProductItemMontage> productItemMontages) throws Exception;

    void saveProductItemMontage(ProductItemMontage productItemMontage) throws Exception;

    void saveProductItemOptionals(List<ProductItemOptional> productItemOptionals) throws Exception;

    void saveProductItemOptional(ProductItemOptional productItemOptional) throws Exception;

    void deleteAll() throws Exception;
}

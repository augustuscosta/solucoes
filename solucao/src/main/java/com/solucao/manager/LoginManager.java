package com.solucao.manager;

import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.domain.Session;
import com.solucao.domain.Table;
import com.solucao.domain.User;

public interface LoginManager {

    boolean reAuthenticate() throws Throwable;

    User authenticate(final String userName, final String password) throws Exception;
    User signUp(User user) throws Exception;
    void createUniqueUser(User user) throws Exception;
    void createUniqueSession(Session session) throws Exception;
    void clearSession() throws Exception;
    void loadSession() throws Exception;

    void addUserInSession(User user) throws Exception;

    void addTableInSession(Table table) throws Exception;

    void answerAuthorizationRequest(AuthorizationRequestUser request) throws Exception;
}

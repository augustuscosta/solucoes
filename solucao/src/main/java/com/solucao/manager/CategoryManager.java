package com.solucao.manager;

import com.solucao.domain.Category;
import com.solucao.domain.CategoryType;

import java.util.List;

/**
 * Created by brunoramosdias on 17/05/14.
 */
public interface CategoryManager {

    List<Category> getCategories(CategoryType type) throws Exception;

    void save(Category category);

    void save(List<Category> categories);

    void deleteAll() throws Exception;
}

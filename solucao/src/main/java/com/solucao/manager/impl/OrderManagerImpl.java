package com.solucao.manager.impl;

import com.fandas.webclient.serializer.SerializerUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.solucao.domain.Order;
import com.solucao.domain.OrderProduct;
import com.solucao.domain.OrderProductItem;
import com.solucao.domain.OrderProductItemAdditional;
import com.solucao.domain.OrderProductItemMontage;
import com.solucao.domain.OrderProductItemOptional;
import com.solucao.domain.OrderStatusType;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.OrderManager;
import com.solucao.manager.network.NetworkServices;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 18/05/14.
 */
public class OrderManagerImpl extends AbstractManager<Order> implements OrderManager {

    public OrderManagerImpl() {
        super(Order.class);
    }

    @Override
    public List<Order> getPendingOrders() throws Exception {
        try {
            return getOrders(OrderStatusType.TO_SEND);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar os pedidos pendentes ou por aprovação.");
            throw new Exception("Erro ao recuperar os pedidos pendentes ou por aprovação.", e);
        }
    }

    @Override
    public List<Order> getWaitingApprovalOrders() throws Exception {
        try {
            return getOrders(OrderStatusType.WAITING_APPROVAL);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar os pedidos por aprovação.");
            throw new Exception("Erro ao recuperar os pedidos por aprovação.", e);
        }
    }

    @Override
    public List<Order> getAll() throws Exception {
        try {
            return getOrders(null);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar todos os pedidos.");
            throw new Exception("Erro ao recuperar todos os pedidos.", e);
        }
    }

    private List<Order> getOrders(final OrderStatusType statusType) throws Exception {
        final QueryBuilder<Order, Integer> orderQueryBuilder = getQueryBuilder(Order.class);

        if ( statusType != null ) {
            final Where<Order, Integer> where = orderQueryBuilder.where();
            where.eq(Order.COLUMN_ORDER_STATUS_TYPE, statusType);
        }

        final List<Order> orders = orderQueryBuilder.query();
        for (Order order : orders) {
            order.setProducts(getProducts(order.getId()));
        }

        return orders;
    }

    private List<OrderProduct> getProducts(final int orderId) throws Exception {
        try {
            final QueryBuilder<OrderProduct, Integer> queryBuilder = getQueryBuilder(OrderProduct.class);
            queryBuilder.where().eq(OrderProduct.COLUMN_ORDER_ID, orderId);

            final List<OrderProduct> products = queryBuilder.query();
            int productId;
            for (OrderProduct product : products) {
                productId = product.getProduct().getId();
                product.setProductItemAdditionals((List<OrderProductItemAdditional>) getProductItems(orderId, productId, OrderProductItemAdditional.class));
                product.setProductItemMontages((List<OrderProductItemMontage>) getProductItems(orderId, productId, OrderProductItemMontage.class));
                product.setProductItemOptionals((List<OrderProductItemOptional>) getProductItems(orderId, productId, OrderProductItemOptional.class));
            }

            return products;
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar os produtos de um pedido.");
            throw new Exception("Erro ao recuperar os produtos de um pedido.", e);
        }
    }

    private List<? extends OrderProductItem> getProductItems(final int orderId, final int productId, final Class<? extends OrderProductItem> clasz) throws Exception {
        try {
            final QueryBuilder<? extends OrderProductItem, Integer> queryBuilder = getQueryBuilder(clasz);
            queryBuilder.where().eq(OrderProduct.COLUMN_ORDER_ID, orderId)
                                .and()
                                .eq(OrderProduct.COLUMN_PRODUCT_ID, productId);
            return queryBuilder.query();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar itens do pedido.");
            throw new Exception("Erro ao recuperar itens do pedido.", e);
        }
    }

    @Override
    public void save(final Order order) throws Exception {
        try {
            getDao(Order.class).createOrUpdate(order);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao salvar um pedido.");
            throw new Exception("Erro ao salvar um pedido.", e);
        }

        saveOrderProducts(order.getProducts());
    }

    private void saveOrderProducts(final List<OrderProduct> products) throws Exception {
        if ( products == null || products.isEmpty() ) {
            return;
        }

        try {
            final Dao<OrderProduct, Integer> dao = getDao(OrderProduct.class);
            dao.callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    final int orderId = products.get(0).getOrder().getId(); // O id do pedido eh o mesmo para todos os produtos e items.
                    int productId;
                    for (OrderProduct product : products) {
                        dao.createOrUpdate(product);
                        productId = product.getProduct().getId();

                        saveOrderProductItems(OrderProductItemAdditional.class, product.getProductItemAdditionals(), orderId, productId);
                        saveOrderProductItems(OrderProductItemOptional.class, product.getProductItemOptionals(), orderId, productId);
                        saveOrderProductItems(OrderProductItemMontage.class, product.getProductItemMontages(), orderId, productId);
                    }

                    return null;
                }
            });
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir produtos de um pedido.");
            throw new Exception(e);
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir produtos de um pedido.");
            throw e;
        }
    }

    private void saveOrderProductItems(final Class<? extends OrderProductItem> clazz, final List<? extends OrderProductItem> orderProductItems, final Integer orderId, final Integer productId) throws Exception {
        try {
            final DeleteBuilder<? extends OrderProductItem, Integer> deleteBuilder = getDeleteBuilder(clazz);
            deleteBuilder.where().eq(OrderProduct.COLUMN_ORDER_ID, orderId)
                                 .and()
                                 .eq(OrderProduct.COLUMN_PRODUCT_ID, productId);

            deleteBuilder.delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar items de um produto em um pedido.");
        }

        if ( orderProductItems == null || orderProductItems.isEmpty() ) {
            return;
        }

        try {
            final Dao dao = getDao(clazz);
            for (OrderProductItem item : orderProductItems) {
                dao.create(item);
            }
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir items de um produto em um pedido.");
            throw new Exception(e);
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir items de um produto em um pedido.");
            throw e;
        }
    }

    @Override
    public float getTotal(final List<Order> orders) {
        if ( orders == null || orders.isEmpty() ) {
            return 0f;
        }

        float total = 0;
        for (Order order : orders) {
            total += order.getTotal();
        }

        return total;
    }

    @Override
    public void sendPendingOrders() throws Exception {
        try {
            final List<Order> orders = getPendingOrders();
            for (Order order : orders) {
                sendOrder(order);
            }
        } catch (Exception e) {
            Ln.e(e, "Erro ao enviar os pedidos pendentes para o server.");
            throw e;
        }
    }

    @Override
    public void sendOrder(final Order order) throws Exception {
        sendAndUpdateOrder(order);
    }

    private void sendAndUpdateOrder(Order order) throws Exception {
        try {
            order = post(NetworkServices.SEND_ORDER.getPath(), SerializerUtil.objectToString(order), Order.class);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao serializar o pedido no ato do envio para o server.");
            throw new Exception("Erro ao serializar o pedido no ato do envio para o server.", e);
        }

        updateOrder(order);
    }

    private void updateOrder(final Order order) throws Exception {
        try {
            getDao(Order.class).update(order);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao atualizar os pedidos retornados pelo server.");
            throw new Exception("Erro ao atualizar os pedidos retornados pelo server.", e);
        }

        updateProducts(order.getProducts());
    }

    private void updateProducts(final List<OrderProduct> products) throws Exception {
        try {
            final Dao<OrderProduct, Integer> dao = getDao(OrderProduct.class);
            for (OrderProduct product : products) {
                dao.update(product);
            }
        } catch (SQLException e) {
            Ln.e(e, "Erro ao atualizar os produtos retornados pelo server.");
            throw new Exception("Erro ao atualizar os produtos retornados pelo server.", e);
        }
    }

    @Override
    public void synchronizeOrdersWaitingApproval() throws Exception {
        try {
            List<Order> orders = getWaitingApprovalOrders();
            if ( orders == null || orders.isEmpty() ) {
                return;
            }

            final Order[] ordersResponse = post(NetworkServices.SYNC_ORDERS_WAITING_APPROVAL.getPath(), SerializerUtil.objectToString(orders), getClassTypeArray());
            if ( ordersResponse == null || ordersResponse.length == 0 ) {
                Ln.w("Atenção server não retornou os pedidos por aprovação.");
                return;
            }

            orders = Arrays.asList(ordersResponse);
            updateOrders(orders);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao atualizar os pedidos aguardando aprovação.");
            throw new Exception("Erro ao atualizar os pedidos aguardando aprovação.", e);
        }
    }

    private void updateOrders(final List<Order> orders) throws Exception {
        try {
            for (Order order : orders) {
                updateOrder(order);
            }
        } catch (Exception e) {
            Ln.e(e, "Erro ao atualizar o pedido por aprovação.");
            throw e;
        }
    }

    @Override
    public List<Order> orderBill(final int tableId) throws Exception {
        try {
            final Order[] ordersResponse = get(NetworkServices.ORDER_BILL.getPath().replace("{tableId}", String.valueOf(tableId)), getClassTypeArray());
            if ( ordersResponse == null || ordersResponse.length == 0 ) {
                return Collections.emptyList();
            }

            return Arrays.asList(ordersResponse);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao pedir a conta");
            throw new Exception("Erro ao pedir a conta", e);
        }
    }

}

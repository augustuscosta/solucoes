package com.solucao.manager.impl;

import com.j256.ormlite.stmt.QueryBuilder;
import com.solucao.domain.Category;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItem;
import com.solucao.domain.ProductItemAdditional;
import com.solucao.domain.ProductItemMontage;
import com.solucao.domain.ProductItemOptional;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.ProductManager;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 18/05/14.
 */
public class ProductManagerImpl extends AbstractManager<Product> implements ProductManager {

    public ProductManagerImpl() {
        super(Product.class);
    }

    @Override
    public Product getProduct(final int id) {
        try {
            return getDao(Product.class).queryForId(id);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar o produto por Id");
        }

        return null;
    }

    @Override
    public List<Product> getProducts(final Category category) throws Exception {
        try {
            final QueryBuilder<Product, Integer> queryBuilder = getQueryBuilder(Product.class);
            queryBuilder.where().eq(Product.COLUMN_CATEGORY_ID, category);
            return queryBuilder.query();
        } catch (SQLException e) {
            Ln.e(e, "Erro recuperar produtos de uma categoria.");
            throw new Exception("Erro recuperar produtos de uma categoria.", e);
        }
    }

    @Override
    public List<Product> getProducts(final int categoryId) throws Exception {
        return getProducts(new Category(categoryId));
    }

    @Override
    public Product getProductWithItens(final int id) throws Exception {
        try {
            final Product product = getDao(Product.class).queryForId(id);
            product.setItemsAdditional(getItems(id, ProductItemAdditional.class));
            product.setItemsMontage(getItems(id, ProductItemMontage.class));
            product.setItemsOptional(getItems(id, ProductItemOptional.class));
            return product;
        } catch (SQLException e) {
            Ln.e(e, "Erro recuperar produtos de uma categoria.");
            throw new Exception("Erro recuperar produtos de uma categoria.", e);
        }
    }

    private List<Product> getItems(final int productId, final Class<? extends ProductItem> classz) throws Exception {
        try {
            final QueryBuilder<? extends ProductItem, Integer> itemQueryBuilder = getQueryBuilder(classz);
            itemQueryBuilder.where().eq(ProductItem.COLUMN_PRODUCT_ID, productId);
            itemQueryBuilder.selectColumns(ProductItem.COLUMN_ITEM_ID);

            final QueryBuilder<Product, Integer> queryBuilder = getQueryBuilder(Product.class);
            queryBuilder.where().in(Product.COLUMN_ID, itemQueryBuilder);
            return queryBuilder.query();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar os itens de um produto.");
            throw new Exception("Erro ao recuperar os itens de um produto.", e);
        }
    }

    @Override
    public void save(final Product product) throws Exception {
        try {
            getDao(Product.class).create(product);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir o produto.");
            throw new Exception("Erro ao inserir o produto.", e);
        }
    }

    @Override
    public void save(final List<Product> products) throws Exception {
        try {
            getDao(Product.class).callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    for (Product product : products) {
                        save(product);
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir a lista de produtos.");
            throw new Exception("Erro ao inserir a lista de produtos.", e);
        }
    }

    @Override
    public void saveProductItemAdditionals(final List<ProductItemAdditional> productItemAdditionals) throws Exception {
        if ( productItemAdditionals == null || productItemAdditionals.isEmpty() ) {
            return;
        }

        try {
            getDao(ProductItemAdditional.class).callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    for (ProductItemAdditional productItemAdditional : productItemAdditionals) {
                        saveProductItemAdditional(productItemAdditional);
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir a lista de associassao de produtos -> items adicionais.");
            throw new Exception("Erro ao inserir a lista de associassao de produtos -> items adicionais.", e);
        }
    }

    @Override
    public void saveProductItemAdditional(final ProductItemAdditional productItemAdditional) throws Exception {
        if ( productItemAdditional == null ) return;

        try {
            getDao(ProductItemAdditional.class).create(productItemAdditional);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir associassao de produto -> item adicional");
            throw new Exception("Erro ao inserir associassao de produto -> item adicional", e);
        }
    }

    @Override
    public void saveProductItemMontages(final List<ProductItemMontage> productItemMontages) throws Exception {
        if ( productItemMontages == null || productItemMontages.isEmpty() ) {
            return;
        }

        try {
            getDao(ProductItemMontage.class).callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    for (ProductItemMontage productItemMontage : productItemMontages) {
                        saveProductItemMontage(productItemMontage);
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir a lista de associassao de produtos -> items montagens.");
            throw new Exception("Erro ao inserir a lista de associassao de produtos -> items montagens.", e);
        }
    }

    @Override
    public void saveProductItemMontage(final ProductItemMontage productItemMontage) throws Exception {
        if ( productItemMontage == null ) return;
        try {
            getDao(ProductItemMontage.class).create(productItemMontage);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir associassao de produto -> item montagem");
            throw new Exception("Erro ao inserir associassao de produto -> item montagem", e);
        }
    }

    @Override
    public void saveProductItemOptionals(final List<ProductItemOptional> productItemOptionals) throws Exception {
        if ( productItemOptionals == null || productItemOptionals.isEmpty() ) {
            return;
        }

        try {
            getDao(ProductItemOptional.class).callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    for (ProductItemOptional productItemOptional : productItemOptionals) {
                        saveProductItemOptional(productItemOptional);
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir a lista de associassao de produtos -> items opcionais.");
            throw new Exception("Erro ao inserir a lista de associassao de produtos -> items opcionais.", e);
        }
    }

    @Override
    public void saveProductItemOptional(final ProductItemOptional productItemOptional) throws Exception {
        if ( productItemOptional == null ) return;

        try {
            getDao(ProductItemOptional.class).create(productItemOptional);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao inserir associassao de produto -> item opcional");
            throw new Exception("Erro ao inserir associassao de produto -> item opcional", e);
        }
    }

    @Override
    public void deleteAll() throws Exception {
        try {
            getDeleteBuilder(ProductItemAdditional.class).delete();
            getDeleteBuilder(ProductItemMontage.class).delete();
            getDeleteBuilder(ProductItemOptional.class).delete();
            getDeleteBuilder(Product.class).delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar todos os produtos.");
            throw new Exception("Erro ao apagar todos os produtos.", e);
        }
    }

}
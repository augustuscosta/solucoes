package com.solucao.manager.impl;

import com.j256.ormlite.stmt.QueryBuilder;
import com.solucao.domain.Category;
import com.solucao.domain.CategoryType;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.CategoryManager;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 18/05/14.
 */
public class CategoryManagerImpl extends AbstractManager<Category> implements CategoryManager {

    public CategoryManagerImpl() {
        super(Category.class);
    }


    @Override
    public List<Category> getCategories(final CategoryType type) throws Exception {
        try {
            final QueryBuilder<Category, Integer> queryBuilder = getQueryBuilder(Category.class);

            if ( type != null ) {
                queryBuilder.where().eq(Category.COLUMN_TYPE, type.name());
            }

            queryBuilder.orderBy(Category.COLUMN_INDEX, true);

            return queryBuilder.query();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar categorias.");
            throw new Exception("Erro ao recuperar categorias.", e);
        }
    }

    @Override
    public void save(final Category category) {
        try {
            getDao(Category.class).create(category);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao salvar categoria.");
        }
    }

    @Override
    public void save(final List<Category> categories) {
        try {
            getDao(Category.class).callBatchTasks(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    for (Category category : categories) {
                        save(category);
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            Ln.e(e, "Erro ao inserir a lista de categorias.");
        }
    }

    @Override
    public void deleteAll() throws Exception {
        try {
            getDeleteBuilder(Category.class).delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar todos os registros de categoria.");
            throw  new Exception("Erro ao apagar todos os registros de categoria.", e);
        }
    }

}
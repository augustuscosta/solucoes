package com.solucao.manager.impl;

import com.google.inject.Inject;
import com.solucao.domain.LoadDataMenuTest;
import com.solucao.domain.Menu;
import com.solucao.domain.Session;
import com.solucao.domain.response.MenuResponse;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.CategoryManager;
import com.solucao.manager.MenuManager;
import com.solucao.manager.ProductManager;
import com.solucao.manager.network.NetworkServices;

import roboguice.util.Ln;

/**
 * Created by pierrediderot on 27/05/14.
 */
public class MenuManagerImpl extends AbstractManager<Menu> implements MenuManager {

    @Inject
    private CategoryManager categoryManager;

    @Inject
    private ProductManager productManager;

    public MenuManagerImpl() {
        super(Menu.class);
    }

    @Override
    public void retrieveAndSaveMenuFromNetwork() throws Throwable {
        try {
            // TODO Descomentar a linha a baixo.
            final MenuResponse menu = get(NetworkServices.LOAD_MENU.getPath().replace("{localId}", String.valueOf(Session.getSession().getLocalId())), MenuResponse.class);
//            final MenuResponse menu = LoadDataMenuTest.getMenuResponse(); // TODO Mockup
            saveMenuResponse(menu);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao recuperar o cardapio(menu)");
            throw e;
        }
    }

    private void saveMenuResponse(final MenuResponse menu) throws Exception {
        if ( menu == null ) {
            return;
        }

        menu.validate();
        clearMenu();

        categoryManager.save(menu.getCategories());

        productManager.save(menu.getProducts());
        productManager.saveProductItemAdditionals(menu.getProductItemAdditionals());
        productManager.saveProductItemMontages(menu.getProductItemMontages());
        productManager.saveProductItemOptionals(menu.getProductItemOptionals());
    }

    @Override
    public void clearMenu() throws Exception {
        try {
            productManager.deleteAll();
        } catch (Exception e) {
            Ln.e(e, "Erro ao apagar todos os produtos.");
            throw new Exception("Erro ao apagar todos os produtos.", e);
        }

        try {
            categoryManager.deleteAll();
        } catch (Exception e) {
            Ln.e(e, "Erro ao apagar todas as categorias.");
            throw new Exception("Erro ao apagar todas as categorias.", e);
        }
    }

}

package com.solucao.manager.impl;

import com.fandas.webclient.serializer.SerializerUtil;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.domain.Session;
import com.solucao.domain.Table;
import com.solucao.domain.User;
import com.solucao.exception.SessionNotActive;
import com.solucao.exception.UserBlocked;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.LoginManager;
import com.solucao.manager.network.NetworkServices;

import java.sql.SQLException;

import roboguice.util.Ln;

public class LoginManagerImpl extends AbstractManager<User> implements LoginManager {

    public LoginManagerImpl() {
        super(User.class);
    }

    private User authenticate(final User user) throws Exception {
        try {
             return post(NetworkServices.AUTHENTICATE_USER.getPath(), SerializerUtil.objectToString(user), User.class);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao auntenticar usuário no servidor.");
            throw new Exception("Erro ao auntenticar usuário no servidor.", e);
        }
    }

    @Override
    public boolean reAuthenticate() throws Throwable {
        if ( !Session.isActive() ) {
            throw new SessionNotActive("Não existe uma sessão ativa.");
        }

        User user = Session.getSession().getUser();
        user = authenticate(user);
        if ( user.getBlocked() ) {
            throw new UserBlocked("Usuário está bloqueado, favor verificar com administrativo.");
        }

        saveUser(user);
        return true;
    }

    @Override
    public User authenticate(final String userName, final String password) throws Exception {
        final User user = new User();
        user.setEmail(userName);
        user.setPassword(password);

        return authenticate(user);
    }

    @Override
    public User signUp(final User user) throws Exception {
        try {
            return post(NetworkServices.SIGNUP_USER.getPath(), SerializerUtil.objectToString(user), User.class);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao criar(singUp) usuário no servidor.");
            throw new Exception("Erro ao criar(singUp) usuário no servidor.", e);
        }
    }

    @Override
    public void createUniqueUser(final User user) throws Exception {
        try {
            deleteUsers();
            saveUser(user);
        } catch (Exception e) {
            Ln.e(e, "Erro ao criar um unico do usuário.");
            throw  e;
        }
    }

    private void deleteUsers() throws Exception {
        try {
            getDeleteBuilder(User.class).delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar os usuários.");
            throw new Exception("Erro ao apagar os usuários.", e);
        }
    }

    private void saveUser(final User user) throws Exception {
        try {
            getDao(User.class).createOrUpdate(user);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao criar usuário.");
            throw new Exception("Erro ao criar usuário.", e);
        }
    }

    @Override
    public void createUniqueSession(final Session session) throws Exception {
        try {
            clearSession();
            saveSession(session);
        } catch (Exception e) {
            throw e;
        }
    }

    private void saveSession(final Session session) throws Exception {
        try {
            getDao(Session.class).create(session);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao salvar uma sessão.");
            throw new Exception("Erro ao salvar uma sessão.", e);
        }
    }

    @Override
    public void clearSession() throws Exception {
        try {
            getDeleteBuilder(Session.class).delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar os registros de sessão.");
            throw new Exception("Erro ao apagar os registros de sessão.", e);
        }
    }

    @Override
    public void loadSession() throws Exception {
        try {
            final Session session = getQueryBuilder(Session.class).queryForFirst();
            Session.addSession(session);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar sessão");
            throw new Exception("Erro ao recuperar sessão", e);
        }
    }

    @Override
    public void addUserInSession(final User user) throws Exception {
        try {
            Session session = Session.getSession();
            session.setUser(user);
            createUniqueSession(session);
            loadSession();
        } catch (Exception e) {
            Ln.e(e, "Erro ao salvar o usuário na sessão.");
            throw new Exception("Erro ao salvar o usuário na sessão.", e);
        }
    }

    @Override
    public void addTableInSession(final Table table) throws Exception {
        try {
            Session session = Session.getSession();
            session.setTable(table);
            createUniqueSession(session);
            loadSession();
        } catch (Exception e) {
            Ln.e(e, "Erro ao salvar a mesa na sessão.");
            throw new Exception("Erro ao salvar a mesa na sessão.", e);
        }
    }

    @Override
    public void answerAuthorizationRequest(final AuthorizationRequestUser request) throws Exception {
        try {
            post(NetworkServices.AUTHORIZATION_REQUEST_ANSWER.getPath(), SerializerUtil.objectToString(request));
        } catch (Throwable e) {
            Ln.e(e, "Erro ao enviar a resposta de autorização para o server.");
            throw new Exception("Erro ao enviar a resposta de autorização para o server.", e);
        }
    }

}

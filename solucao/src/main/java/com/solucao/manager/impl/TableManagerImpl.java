package com.solucao.manager.impl;

import com.fandas.webclient.serializer.SerializerUtil;
import com.google.inject.Inject;
import com.solucao.domain.Table;
import com.solucao.domain.request.TableRequest;
import com.solucao.manager.AbstractManager;
import com.solucao.manager.LoginManager;
import com.solucao.manager.TableManager;
import com.solucao.manager.network.NetworkServices;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import roboguice.util.Ln;

/**
 * Created by brunoramosdias on 16/05/14.
 */
public class TableManagerImpl  extends AbstractManager<Table> implements TableManager {

    @Inject
    private LoginManager loginManager;

    public TableManagerImpl() {
        super(Table.class);
    }

    @Override
    public Table getTable() throws Exception {
        try {
            return getQueryBuilder(Table.class).queryForFirst();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao recuperar mesa.");
            throw new Exception("Erro ao recuperar mesa.", e);
        }
    }

    @Override
    public void callWaiter(final int tableId) throws Exception {
        try {
            get(NetworkServices.CALL_TABLE_WAITER.getPath().replace("{tableId}", String.valueOf(tableId)));
        } catch (Throwable e) {
            Ln.e(e, "Erro ao chamar o atendente.");
            throw new Exception("Erro ao chamar o atendente.", e);
        }
    }

    @Override
    public Table getTableFromNetwork(final int id) throws Exception {
        try {
            return get(NetworkServices.FIND_TABLE.getPath().replace("{tableId}", String.valueOf(id)), Table.class);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao recupera mesa do servidor.");
            throw new Exception("Erro ao recupera mesa do servidor.", e);
        }
    }

    @Override
    public List<Table> getTablesFromNetwork(final int localId) throws Exception {
        try {
            final Table[] tables = get(NetworkServices.TABLES.getPath().replace("{localId}", String.valueOf(localId)), getClassTypeArray());

            if ( tables == null || tables.length == 0 ) {
                return Collections.emptyList();
            }

            return Arrays.asList(tables);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao recuperar lista de mesas do servidor.");
            throw new Exception("Erro ao recuperar lista de mesas do servidor.", e);
        }
    }

    @Override
    public Table changeTable(final int localId, final Table origin, final Table target) throws Exception {
        try {
            final TableRequest tableRequest = new TableRequest(origin, target);
            return post(NetworkServices.TABLE_CHANGE.getPath().replace("{localId}", String.valueOf(localId)), SerializerUtil.objectToString(tableRequest), Table.class);
        } catch (Throwable e) {
            Ln.e(e, "Erro ao solicitar troca de mesa.");
            throw new Exception("Erro ao solicitar troca de mesa.", e);
        }
    }

    @Override
    public void createUniqueTable(final Table table) throws Exception {
        try {
            deleteAll();
            save(table);
            loginManager.addTableInSession(table);
        } catch (Exception e) {
            Ln.e(e, "Erro ao salvar a mesa");
            throw e;
        }
    }

    private void save(final Table table) throws Exception {
        try {
            getDao(Table.class).create(table);
        } catch (SQLException e) {
            Ln.e(e, "Erro ao salvar mesa.");
            throw new Exception("Erro ao salvar mesa.", e);
        }
    }

    private void deleteAll() throws Exception {
        try {
            getDeleteBuilder(Table.class).delete();
        } catch (SQLException e) {
            Ln.e(e, "Erro ao apagar os dados da mesa.");
            throw new Exception("Erro ao apagar os dados da mesa.", e);
        }
    }

}

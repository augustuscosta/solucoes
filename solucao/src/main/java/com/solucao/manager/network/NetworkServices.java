package com.solucao.manager.network;

/**
 * Created by pierrediderot on 07/03/14.
 */
public enum NetworkServices {

    // Menu
    LOAD_MENU("/menu/{localId}"),

    // User
    AUTHENTICATE_USER("/user/login"),
    SIGNUP_USER("/user/signup"),
    AUTHORIZATION_REQUEST_ANSWER("/user/authorization"),

    // Table
    CALL_TABLE_WAITER("/mesa/{tableId}/garcom"),
    FIND_TABLE("/mesa/{tableId}"),
    TABLE_CHANGE("/mesa/trocar/{localId}"),
    TABLES("/mesas/{localId}"),

    // Order
    SEND_ORDER("/pedido"),
    SYNC_ORDERS_WAITING_APPROVAL("/pedido/sync"),
    ORDER_BILL("/pedido/conta/{tableId}"),
    ;

    private final String value;

    private NetworkServices(String value) {
        this.value = value;
    }

    public String getPath() {
        return this.value;
    }
}

package com.solucao.manager;

/**
 * Created by pierrediderot on 27/05/14.
 */
public interface MenuManager {

    void retrieveAndSaveMenuFromNetwork() throws Throwable;

    void clearMenu() throws Exception;
}

package com.solucao.manager;

import com.solucao.domain.Table;

import java.util.List;

/**
 * Created by brunoramosdias on 16/05/14.
 */
public interface TableManager {

    Table getTable() throws Exception;

    void callWaiter(int tableId) throws Exception;

    Table getTableFromNetwork(int id) throws Exception;

    List<Table> getTablesFromNetwork(int localId) throws Exception;

    Table changeTable(int localId, Table origin, Table target) throws Exception;

    void createUniqueTable(Table table) throws Exception;
}

package com.solucao.holder;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import com.solucao.domain.Category;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public class CategoryHolder {

    public ImageView touchImageView;
    public TextView textView;

    public void drawImage(Category category, Activity context) {
        textView.setText(category.getName());
    }
}
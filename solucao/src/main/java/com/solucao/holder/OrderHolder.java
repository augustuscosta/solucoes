package com.solucao.holder;

import android.app.Activity;
import android.widget.TextView;

import com.google.inject.Inject;
import com.solucao.R;
import com.solucao.domain.Order;
import com.solucao.domain.OrderProduct;
import com.solucao.domain.OrderProductItemAdditional;
import com.solucao.domain.OrderProductItemMontage;
import com.solucao.domain.OrderProductItemOptional;
import com.solucao.manager.OrderManager;
import com.solucao.util.ConstUtil;

import java.text.DecimalFormat;

/**
 * Created by brunoramosdias on 11/06/14.
 */
public class OrderHolder {

    @Inject
    private OrderManager orderManager;
    public TextView orderNumber;
    public TextView orderDetail;
    public TextView orderPrice;
    public TextView orderStatus;

    public void drawRow(Order obj, Activity context) {
        setOrderNumber(obj);
        setOrderProducts(obj);
        setOrderStatus(obj, context);
        setOrderPrice(obj,context);
    }

    private void setOrderNumber(Order obj) {
//        if (obj.getOrderNumber() != null) {
//            orderNumber.setText(obj.getOrderNumber());
//        } else {
//            orderNumber.setText("");
//        }
    }

    private void setOrderProducts(Order obj) {
        String toShow = "";
        if (obj.getProducts() != null && !obj.getProducts().isEmpty()) {
            toShow = getOrderProductDetails(obj, toShow);
        }
        orderDetail.setText(toShow);
    }

    private String getOrderProductDetails(Order obj, String toShow) {
        for (OrderProduct orProduct : obj.getProducts()) {
            toShow += orProduct.getQuantity().toString() + ConstUtil.SPACE + orProduct.getProduct().getName() + ConstUtil.COMMA + ConstUtil.SPACE;
            toShow = setProductMontages(toShow, orProduct);
            toShow = setProductOptrionals(toShow, orProduct);
            toShow = setProductAditionals(toShow, orProduct);
        }
        return toShow;
    }

    private String setProductMontages(String toShow, OrderProduct orProduct) {
        if (orProduct.getProductItemMontages() != null && !orProduct.getProductItemMontages().isEmpty()) {
            for (OrderProductItemMontage montage : orProduct.getProductItemMontages()) {
                toShow += montage.getItem().getName() + ConstUtil.COMMA + ConstUtil.SPACE;
            }
        }
        return toShow;
    }

    private String setProductOptrionals(String toShow, OrderProduct orProduct) {
        if (orProduct.getProductItemOptionals() != null && !orProduct.getProductItemOptionals().isEmpty()) {
            for (OrderProductItemOptional optional : orProduct.getProductItemOptionals()) {
                toShow += optional.getItem().getName() + ConstUtil.COMMA + ConstUtil.SPACE;
            }
        }
        return toShow;
    }

    private String setProductAditionals(String toShow, OrderProduct orProduct) {
        if (orProduct.getProductItemAdditionals() != null && !orProduct.getProductItemAdditionals().isEmpty()) {
            for (OrderProductItemAdditional additional : orProduct.getProductItemAdditionals()) {
                toShow += additional.getQuantity().toString() + ConstUtil.SPACE + additional.getItem().getName() + ConstUtil.COMMA + ConstUtil.SPACE;
            }
        }
        return toShow;
    }

    private void setOrderStatus(Order obj, Activity context) {
        if (obj.getOrderStatusType() != null) {
            switch (obj.getOrderStatusType()) {
                case TO_SEND:
                    orderStatus.setText(context.getString(R.string.to_send));
                    break;
                case WAITING_APPROVAL:
                    orderStatus.setText(context.getString(R.string.wait_aprove));
                    break;
                case SUCCESS:
                    orderStatus.setText(context.getString(R.string.order_sucess));
                    break;
                case WARNING:
                    orderStatus.setText(obj.getMsg());
                    break;
            }
        } else {
            orderStatus.setText("");
        }
    }

    private void setOrderPrice(Order obj, Activity context) {
        if(obj.getTotal() >0) {
            orderPrice.setText(context.getString(R.string.currency)+ new DecimalFormat("#.00").format((obj.getTotal())));
        }

    }

}



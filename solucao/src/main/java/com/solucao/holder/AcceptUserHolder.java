package com.solucao.holder;

import android.widget.Button;
import android.widget.TextView;

import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.domain.Product;

/**
 * Created by brunoramosdias on 24/05/14.
 */
public class AcceptUserHolder {

    public TextView textView;
    public Button yesButton;
    public Button noButton;

    public void drawRow(AuthorizationRequestUser authorizationRequestUser){
        textView.setText(authorizationRequestUser.getUser().getName());
    }
}

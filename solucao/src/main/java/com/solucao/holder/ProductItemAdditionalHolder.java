package com.solucao.holder;

import android.widget.Button;
import android.widget.TextView;

import com.solucao.domain.Product;
import com.solucao.domain.ProductItemAdditional;

/**
 * Created by brunoramosdias on 24/05/14.
 */
public class ProductItemAdditionalHolder {

    public TextView textView;
    public TextView countTextView;
    public Button addButton;
    public Button removeButton;

    public void drawRow(Product productItemAdditional){
        textView.setText(productItemAdditional.getName());

    }
}

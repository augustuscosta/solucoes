package com.solucao.holder;

import android.widget.CheckBox;
import android.widget.TextView;

import com.solucao.domain.Product;

/**
 * Created by brunoramosdias on 01/06/14.
 */
public class ProductOptionHolder {


    public TextView optionName;
    public CheckBox optionCheckBox;


    public void drawRow(Product obj){
        if ( obj == null ) {
            return;
        }

        if(obj.getName()!= null){
            optionName.setText(obj.getName());
        }else{
            optionName.setText("");
        }


    }



    public boolean isChecked() {
        if ( !isExistCheckbox() ) {
            return false;
        }

        return optionCheckBox.isChecked();
    }

    private boolean isExistCheckbox() {
        return optionCheckBox != null;
    }

    public void setChecked(final boolean value) {
        optionCheckBox.setChecked(value);
    }

}

package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Order;
import com.solucao.holder.OrderHolder;

import java.util.List;

/**
 * Created by brunoramosdias on 11/06/14.
 */
public class OrderAdapter extends BaseAdapter {

    private List<Order> orders;
    private Activity context;

    public OrderAdapter(List<Order> orders, Activity context) {
        this.orders = orders;
        this.context = context;
    }

    @Override
    public int getCount() {
        return orders == null? 0: orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders == null? null :orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        OrderHolder orderHolder;
        Order obj = orders.get(position);
        if(view == null){
            orderHolder = new OrderHolder();
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.order_holder,null);
            orderHolder.orderNumber = (TextView) view.findViewById(R.id.order_number);
            orderHolder.orderDetail = (TextView) view.findViewById(R.id.order_details);
            orderHolder.orderStatus = (TextView) view.findViewById(R.id.order_status);
            orderHolder.orderPrice = (TextView) view.findViewById(R.id.order_price);
        }else{
            orderHolder = (OrderHolder) view.getTag();
        }
        view.setTag(orderHolder);
        orderHolder.drawRow(obj,context);
        return view;
    }
}

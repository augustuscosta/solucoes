package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Product;
import com.solucao.holder.ProductItemAdditionalHolder;

import java.util.List;

/**
 * Created by brunoramosdias on 24/05/14.
 */
public class ProductItemAditionalAdapter extends BaseAdapter {

    private List<Product> products;
    private String[] countArray;
    private Activity context;

    public ProductItemAditionalAdapter(List<Product> products, Activity context) {
        this.products = products;
        this.context = context;
        countArray = new String[products.size()];
    }

    @Override
    public int getCount() {
      return products == null ?0: products.size();
    }

    @Override
    public Product getItem(int position) {
        return products == null ? null: products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ProductItemAdditionalHolder holder;
        if(convertView == null){
            holder = new ProductItemAdditionalHolder();
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.aditional_holder,null);
            holder.textView = (TextView) view.findViewById(R.id.productItemAditionalName);
            holder.addButton = (Button) view.findViewById(R.id.buttonAdd);
            holder.removeButton = (Button) view.findViewById(R.id.buttonRemove);
            holder.countTextView = (TextView) view.findViewById(R.id.productCount);
        }else{
            view = convertView;
            holder = (ProductItemAdditionalHolder)view.getTag();
        }
        Product product = products.get(position);
        holder.addButton.setOnClickListener(onClickAddListener);
        holder.removeButton.setOnClickListener(onClickARemoveListenerListener);
        holder.drawRow(product);
        if(countArray[position]!= null){
            holder.countTextView.setText(countArray[position]);
        }
        view.setTag(holder);
        view.setTag(R.layout.aditional_holder,product);
        return view;
    }

    View.OnClickListener onClickAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            TextView textView = (TextView)parent.findViewById(R.id.productCount);
            Product obj = (Product) parent.getTag(R.layout.aditional_holder);
            int count = Integer.parseInt(textView.getText().toString());
            count++;
            textView.setText(Integer.toString(count));
            countArray[getProductPosition(obj)] = Integer.toString(count);
        }
    };

    View.OnClickListener onClickARemoveListenerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            TextView textView = (TextView)parent.findViewById(R.id.productCount);
            Product obj = (Product) parent.getTag(R.layout.aditional_holder);
            int count = Integer.parseInt(textView.getText().toString());
            if(count > 0){
                count--;
                textView.setText(Integer.toString(count));
            }
            countArray[getProductPosition(obj)] = Integer.toString(count);
        }
    };

    private Integer getProductPosition(Product product){
        for (int i = 0; i < products.size(); i++) {
            if(product.equals(products.get(i))){
                return i;
            }
        }
        return null;
    }

    public String[] getCountArray(){
        return countArray;
    }

    public void setCountArray(String[] countArray) {
        this.countArray = countArray;
        notifyDataSetChanged();
    }
}

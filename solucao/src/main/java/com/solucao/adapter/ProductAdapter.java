package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Category;
import com.solucao.domain.Product;
import com.solucao.holder.CategoryHolder;
import com.solucao.holder.ProductHolder;

import java.util.List;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public class ProductAdapter extends BaseAdapter {

    private List<Product> products;
    private Activity context;

    public ProductAdapter(List<Product> products, Activity context){
        this.products = products;
        this.context = context;
    }


    @Override
    public int getCount() {
        return products == null ?0: products.size();
    }

    @Override
    public Object getItem(int position) {
        return products == null?null : products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View i;
        ProductHolder holder;
        Product product = products.get(position);
        if(convertView == null){
            holder = new ProductHolder();
            LayoutInflater inflater = context.getLayoutInflater();
            i = inflater.inflate(R.layout.slide_holder, null);
            holder.touchImageView = (ImageView) i.findViewById(R.id.touchImage);
            holder.textView = (TextView) i.findViewById(R.id.categoryName);
        }else{
            i = convertView;
            holder = (ProductHolder) i.getTag();
        }
        holder.drawImage(product,context);
        i.setTag(holder);

        return i;
    }
}

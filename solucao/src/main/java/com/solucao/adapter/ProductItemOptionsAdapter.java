package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Product;
import com.solucao.domain.ProductItemOptional;
import com.solucao.holder.ProductOptionHolder;
import com.solucao.interfacehandlers.DialogHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brunoramosdias on 01/06/14.
 */
public class ProductItemOptionsAdapter extends BaseAdapter {


    private Activity context;
    private List<Product> list;
    private List<Product> checkedList;

    public ProductItemOptionsAdapter(final Activity context, final List<Product> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list == null ? 0 : list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ProductOptionHolder holder;
        Product optional = list.get(position);

        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.options_holder, parent,false);
            holder = new ProductOptionHolder();
            holder.optionName = (TextView) view.findViewById(R.id.productItemOptionName);
            holder.optionCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
            holder.optionCheckBox.setOnClickListener(listener);

        } else {
            holder = (ProductOptionHolder) view.getTag(R.layout.options_holder);
            holder.setChecked(false);
        }

        if (isChecked(optional)) {
            holder.setChecked(true);
        }

        view.setTag(R.layout.options_holder, holder);
        view.setTag(R.layout.fragment_options, optional);
        holder.drawRow(optional);
        return view;
    }

    /**
     * Create a single instance of items checked.
     */
    private void createSingleInstanceCheckedList() {
        if (checkedList == null) {
            checkedList = new ArrayList<Product>();
        }
    }

    private boolean isChecked(final Product medida) {
        createSingleInstanceCheckedList();
        return checkedList.contains(medida);
    }

    public void checkAll() {
        createSingleInstanceCheckedList();
        checkedList = new ArrayList<Product>(list);
        notifyDataSetChanged();
    }

    public void uncheckAll() {
        createSingleInstanceCheckedList();
        checkedList.clear();
        notifyDataSetChanged();
    }

    private void toogleCheckBox(View rootView) {
        ProductOptionHolder rowInfo = (ProductOptionHolder) rootView.getTag(R.layout.options_holder);
        final Product option = (Product) rootView.getTag(R.layout.fragment_options);
        createSingleInstanceCheckedList();

        if (checkedList.contains(option)) {
            checkedList.remove(option);
            rowInfo.setChecked(false);
        } else{
            if(isOnSameCategory(option)!= null){
                checkedList.remove(isOnSameCategory(option));
                checkedList.add(option);
                rowInfo.setChecked(true);
            }else{
                checkedList.add(option);
                rowInfo.setChecked(true);
            }
        }
        notifyDataSetChanged();
    }

    private Product isOnSameCategory(Product option) {
        for(Product obj:checkedList){
            if(obj.getCategory().equals(option.getCategory())){
                return obj;
            }
        }
        return null;
    }

    public boolean hasCheckedItems() {
        createSingleInstanceCheckedList();
        return !checkedList.isEmpty();
    }

    public List<Product> getCheckedItems() {
        return checkedList;
    }

    public void setCheckedItems(List<Product> produts) {
        checkedList = new ArrayList<Product>(produts);
        notifyDataSetChanged();
    }

    final private View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            View rootView = (View) v.getParent().getParent();
            toogleCheckBox(rootView);
        }
    };

}

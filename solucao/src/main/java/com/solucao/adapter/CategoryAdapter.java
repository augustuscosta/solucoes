package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.domain.Category;
import com.solucao.holder.CategoryHolder;

import java.util.List;

/**
 * Created by brunoramosdias on 28/04/14.
 */
public class CategoryAdapter extends BaseAdapter {

    private List<Category> categorys;
    private Activity context;

    public CategoryAdapter(List<Category> categorys, Activity context){
        this.categorys = categorys;
        this.context = context;
    }

    @Override
    public int getCount() {
        return categorys == null ?0:categorys.size();
    }

    @Override
    public Object getItem(int position) {
        return categorys== null?null : categorys.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View i;
        CategoryHolder holder;
        Category category = categorys.get(position);
        if(convertView == null){
            holder = new CategoryHolder();
            LayoutInflater inflater = context.getLayoutInflater();
            i = inflater.inflate(R.layout.slide_holder, null);
            holder.touchImageView = (ImageView) i.findViewById(R.id.touchImage);
            holder.textView = (TextView) i.findViewById(R.id.categoryName);
        }else{
            i = convertView;
            holder = (CategoryHolder) i.getTag();
        }
        holder.drawImage(category,context);
        i.setTag(holder);

        return i;
    }
}

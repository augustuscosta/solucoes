package com.solucao.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.solucao.R;
import com.solucao.activity.generic.TableConfirmationCallback;
import com.solucao.domain.AuthorizationRequestUser;
import com.solucao.holder.AcceptUserHolder;

import java.util.List;

/**
 * Created by brunoramosdias on 11/06/14.
 */
public class AcceptUserAdapter extends BaseAdapter {

    private final TableConfirmationCallback callback;
    private List<AuthorizationRequestUser> authorizationRequestUsers;
    private Activity context;

    public AcceptUserAdapter(List<AuthorizationRequestUser> authorizationRequestUsers, Activity context,TableConfirmationCallback callback) {
        this.authorizationRequestUsers = authorizationRequestUsers;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return authorizationRequestUsers == null? 0: authorizationRequestUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return authorizationRequestUsers == null? null :authorizationRequestUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        AcceptUserHolder acceptUserHolder;
        AuthorizationRequestUser obj = authorizationRequestUsers.get(position);
        if(view == null){
            acceptUserHolder = new AcceptUserHolder();
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.requet_holder,null);
            acceptUserHolder.textView = (TextView) view.findViewById(R.id.name);
            acceptUserHolder.yesButton = (Button) view.findViewById(R.id.button_yes);
            acceptUserHolder.noButton = (Button) view.findViewById(R.id.button_no);
        }else{
            acceptUserHolder = (AcceptUserHolder) view.getTag();
        }
        acceptUserHolder.yesButton.setOnClickListener(onClickAccept);
        acceptUserHolder.noButton.setOnClickListener(onClickReject);
        view.setTag(acceptUserHolder);
        view.setTag(R.layout.activity_user,obj);
        acceptUserHolder.drawRow(obj);
        return view;
    }


    private final View.OnClickListener onClickAccept = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AuthorizationRequestUser requestUser = getAuthorizationRequestUser(v);
            authorizationRequestUsers.remove(requestUser);
            notifyDataSetChanged();
            requestUser.setAccept(true);
            callback.answer(requestUser);
        }
    };

    private final View.OnClickListener onClickReject = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AuthorizationRequestUser requestUser = getAuthorizationRequestUser(v);
            authorizationRequestUsers.remove(requestUser);
            notifyDataSetChanged();
            requestUser.setAccept(false);
            callback.answer(requestUser);

        }
    };

    private AuthorizationRequestUser getAuthorizationRequestUser(View v) {
        View parent = (View) v.getParent().getParent();
        return (AuthorizationRequestUser) parent.getTag(R.layout.activity_user);
    }

    public List<AuthorizationRequestUser> getAuthorizationRequestUsers() {
        return authorizationRequestUsers;
    }

    public void setAuthorizationRequestUsers(List<AuthorizationRequestUser> authorizationRequestUsers) {
        this.authorizationRequestUsers = authorizationRequestUsers;
    }
}

package com.solucao.module;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.solucao.dao.DaoCache;
import com.solucao.httpclient.DefaultHttpClientBuilder;
import com.solucao.manager.CategoryManager;
import com.solucao.manager.LoginManager;
import com.solucao.manager.MenuManager;
import com.solucao.manager.OrderManager;
import com.solucao.manager.ProductManager;
import com.solucao.manager.TableManager;
import com.solucao.manager.impl.CategoryManagerImpl;
import com.solucao.manager.impl.LoginManagerImpl;
import com.solucao.manager.impl.MenuManagerImpl;
import com.solucao.manager.impl.OrderManagerImpl;
import com.solucao.manager.impl.ProductManagerImpl;
import com.solucao.manager.impl.TableManagerImpl;

public class ModuleConfiguration extends AbstractModule {

    @Override
    protected void configure() {
        bind(DaoCache.class).asEagerSingleton();
        bind(DefaultHttpClientBuilder.class).in(Scopes.SINGLETON);

        // Managers
        bind(LoginManager.class).to(LoginManagerImpl.class).in(Scopes.SINGLETON);
        bind(MenuManager.class).to(MenuManagerImpl.class).in(Scopes.SINGLETON);
        bind(CategoryManager.class).to(CategoryManagerImpl.class).in(Scopes.SINGLETON);
        bind(ProductManager.class).to(ProductManagerImpl.class).in(Scopes.SINGLETON);
        bind(OrderManager.class).to(OrderManagerImpl.class).in(Scopes.SINGLETON);
        bind(TableManager.class).to(TableManagerImpl.class).in(Scopes.SINGLETON);
        bind(TableManager.class).to(TableManagerImpl.class).in(Scopes.SINGLETON);
    }

}

package com.fandas.webclient.exception.handle;

import com.fandas.webclient.domain.ErrorMessage;
import com.fandas.webclient.exception.AccessDeniedException;
import org.apache.http.HttpStatus;

public class AccessDeniedExceptionHanlder extends AbstractExceptionHandler {

	@Override
	public int statusCode() {
		return HttpStatus.SC_FORBIDDEN;
	}
	
	@Override
	protected Throwable createException(ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return new AccessDeniedException("Access Denied");
		}
		
		return new AccessDeniedException(errorMessage.getMessage());
	}

}

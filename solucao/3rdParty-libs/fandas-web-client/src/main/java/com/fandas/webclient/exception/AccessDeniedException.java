package com.fandas.webclient.exception;

public class AccessDeniedException extends Throwable {

	private static final long serialVersionUID = -8764093375298519176L;

    public AccessDeniedException() {
    }

    public AccessDeniedException(String s) {
        super(s);
    }

    public AccessDeniedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AccessDeniedException(Throwable throwable) {
        super(throwable);
    }

}

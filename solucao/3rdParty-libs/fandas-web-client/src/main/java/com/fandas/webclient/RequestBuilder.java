package com.fandas.webclient;

import com.fandas.webclient.serializer.Serializer;
import org.apache.http.client.HttpClient;

public class RequestBuilder<Model> {

	private String server;
	private Class<Model> clazz; 
	private HttpClient httpClient;
	private String service;
	private Serializer serializer;
	
	private RequestBuilder(Class<Model> clazz) {
		this.clazz = clazz;
	}

	public static <Data> RequestBuilder<Data> create(Class<Data> clazz) {
		return new RequestBuilder<Data>(clazz);
	}
	
	public RequestBuilder<Model> toServer(final String server) {
		this.server = server;
		return this;
	}
	
	public RequestBuilder<Model> overrideHttpClient(final HttpClient client) {
		httpClient = client;
		return this;
	}
	
	public RequestBuilder<Model> overrideService(final String service) {
		this.service = service;
		return this;
	}
	
	public RequestBuilder<Model> overrideSerializer(final Serializer serializer) {
		this.serializer = serializer;
		return this;
	}

	public CrudRequest<Model> build() {
		return (CrudRequest<Model>) new CrudRequestImpl();
	}
	
	private class CrudRequestImpl extends AbstractCrudRequest<Model> {

		public CrudRequestImpl() {
			super(clazz);
		}

		@Override
		protected String getServer() {
			return server;
		} 
		
		@Override
		public String getServiceName() {
			return service == null ? super.getServiceName() : service;
		}
		
		@Override
		protected HttpClient getHttpClient() {
			return httpClient == null ? super.getHttpClient() : httpClient;
		}
		
		@Override
		protected Serializer getSerializer() {
			if (serializer != null) {
				return serializer;
			} else {
				return super.getSerializer();
			}
		}

		@Override
		protected boolean authenticate() {
			return false;
		}
	}

}

package com.fandas.webclient.exception;

public class BadRequestException extends Throwable {

	private static final long serialVersionUID = -4243763615894475599L;

    public BadRequestException() {
    }

    public BadRequestException(String s) {
        super(s);
    }

    public BadRequestException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public BadRequestException(Throwable throwable) {
        super(throwable);
    }

}

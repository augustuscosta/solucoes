package com.fandas.webclient;

import java.io.IOException;

public interface CrudRequest<Model> {

	/**
	 * Lista todas as entidades 
	 * @return
	 */
	public Model[] list() throws Throwable;
	
	/**
	 * Recupera uma entidade baseada no seu id. 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public Model findById(int id) throws Throwable;
	
	/**
	 * Executa uma query no servidor passando uma entidade 
	 * como exemplo. Cada propriedade nao nula sera utilizada 
	 * como clausula da consulta. 
	 * @param example
	 * @return
	 */
	public Model[] findByExample(Model example) throws Throwable;
	
	public Model create(Model model) throws Throwable;
	
	public Boolean update(String request, Object model) throws Throwable;
	
	public void delete(Integer id) throws Throwable;

	public Model[] findByExample(Model example,boolean fullRelatedObjects) throws Throwable;

	public Model[] executePost(Object model, String url, Class<Model> clazz) throws Throwable;
	
}

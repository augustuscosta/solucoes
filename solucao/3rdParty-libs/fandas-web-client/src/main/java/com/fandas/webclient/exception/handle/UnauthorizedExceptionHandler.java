package com.fandas.webclient.exception.handle;

import com.fandas.webclient.domain.ErrorMessage;
import com.fandas.webclient.exception.UnauthorizedException;
import org.apache.http.HttpStatus;

public class UnauthorizedExceptionHandler extends AbstractExceptionHandler {

	@Override
	public int statusCode() {
		return HttpStatus.SC_UNAUTHORIZED;
	}

	@Override
	public Throwable createException(final ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return new UnauthorizedException("Login or password is invalid.");
		}
		
		return new UnauthorizedException(errorMessage.getMessage());
	}

}

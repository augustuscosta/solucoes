package com.fandas.webclient.exception.handle;

import com.fandas.webclient.exception.ConflictException;

import com.fandas.webclient.domain.ErrorMessage;
import org.apache.http.HttpStatus;

public class ConflictExceptionHandler extends AbstractExceptionHandler {

	@Override
	public int statusCode() {
		return HttpStatus.SC_CONFLICT;
	}

	@Override
	public Throwable createException(final ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return new ConflictException("Request conflict. The request was previously processed by the server");
		}
		
		return new ConflictException(errorMessage.getMessage());
	}

}

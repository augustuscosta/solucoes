package com.fandas.webclient;

import com.fandas.webclient.exception.handle.*;
import com.fandas.webclient.serializer.Serializer;
import com.fandas.webclient.serializer.SerializerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe abstrata para realizacao de requisicoes HTTP como PUT, POST, GET, DELETE. 
 */
public abstract class AbstractHTTPRequest {

	private final List<ExceptionHandler> exceptionHandlers = new ArrayList<ExceptionHandler>();
	private final Object LOCK = new Object();
	
	/**
	 * Abstract Method: usado para obter o endereco do servidor que e passado 
	 * pela implementacao 
	 * @return
	 */
	protected abstract String getServer();
	
	protected abstract boolean authenticate();
	
	/**
	 * Abstract Method: usado para obter a implementacao do HTTP Client
	 * @return
	 */
	protected abstract HttpClient getHttpClient();
	
	public AbstractHTTPRequest() {
		addExceptionHandler(new BadRequestExceptionHandler());
		addExceptionHandler(new ConflictExceptionHandler());
		addExceptionHandler(new UnauthorizedExceptionHandler());
		addExceptionHandler(new AccessDeniedExceptionHanlder());
		addExceptionHandler(new InternalServerErrorExceptionHandler());
		addExceptionHandler(new DefaultExceptionHandler());
	}
	
	public void addExceptionHandler(ExceptionHandler handler) { 
		exceptionHandlers.add(handler);
	}

	public List<ExceptionHandler> getExceptionHandlers() {
		return exceptionHandlers;
	}

	public HttpResponse post(final String requestService, final String requestBody) throws Throwable {
		try {
			final StringBuilder builder = getServerURLBuilder();
			final HttpPost post = new HttpPost(encode(builder.append(requestService).toString()));
			
			addRequestBody(requestBody, post);
			return execute(post);
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
	}

	/**
	 * Realiza uma requisicao POST passando o servico e entidade, para chamadas com MultiPart
	 * @param requestService
	 * @param entity
	 * @return
	 */
	public HttpResponse post(String requestService, HttpEntity entity) throws Throwable {
		final StringBuilder builder = getServerURLBuilder();
		final HttpPost post = new HttpPost(encode(builder.append(requestService).toString()));
		addEntityToRequest(entity, post);
		return execute(post);
	}
	
	private void addEntityToRequest(final HttpEntity entity, final HttpPost post) {
		post.setEntity(entity);
	}

	public HttpResponse put(String requestService, String requestBody) throws Throwable {
		try {
			final StringBuilder builder = getServerURLBuilder();
			final HttpEntityEnclosingRequestBase put = new HttpPut(encode(builder.append(requestService).toString()));
			
			addRequestBody(requestBody, put);
			return execute(put);
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
	}
	
	public HttpResponse get(String requestService) throws Throwable {
		final StringBuilder builder = getServerURLBuilder();
		final HttpGet get = new HttpGet(encode(builder.append(requestService).toString()));
		return execute(get);
	}
	
	protected HttpResponse execute(final HttpUriRequest request) throws Throwable {
		synchronized ( LOCK  ) {
			HttpResponse response = null;
			try {
				response = getHttpClient().execute(request);
				
				if(isAccessDenied(response) && authenticate()){
					response = getHttpClient().execute(request);
				}
				
				for (ExceptionHandler handler : exceptionHandlers) {
					handler.handle(response);
				}
				
				return response;
			} catch (ClientProtocolException e) {
				consumeQuietly(response);
				throw e;
			} catch (IOException e) {
				consumeQuietly(response);
				throw e;
			}
		}
	}
	
	/**
	 * Consume stream from response body use EntityUtils.consumeQuietly from org.apache
	 */
	protected void consumeQuietly(final HttpResponse response) {
		if ( response == null ) return;
		EntityUtils.consumeQuietly(response.getEntity());
	}

	/**
	 * Cria um {@link StringBuilder} com o endereco inicial do servidor e adiciona uma 
	 * barra "/" ao final da string caso nao esteja presente. 
	 * Pode ser utilizado para concatenar o restante da URL
	 * @return
	 */
	protected StringBuilder getServerURLBuilder() {
		final StringBuilder builder = new StringBuilder(getServer());
		if ( builder.charAt(builder.length()-1) != '/' ) {
			builder.append("/");
		}
		
		return builder;
	}

	/**
	 * @return True if statusCode from response == 200 OK
	 */
	protected boolean isSuccess(final HttpResponse response) {
		return compareStatusCode(response, HttpStatus.SC_OK);
	}
	
	/**
	 * @return True if statusCode from response == 401 Unauthorized
	 */
	protected boolean isUnauthorized(final HttpResponse response) {
		return compareStatusCode(response, HttpStatus.SC_UNAUTHORIZED);
	}
	
	/**
	 * @return True if statusCode from response == 409 Conflict
	 */
	protected boolean isConflict(final HttpResponse response) {
		return compareStatusCode(response, HttpStatus.SC_CONFLICT);
	}
	
	/**
	 * @return True if statusCode from response == 403 Access Denied(Forbidden)
	 */
	protected boolean isAccessDenied(final HttpResponse response) {
		return compareStatusCode(response, HttpStatus.SC_FORBIDDEN);
	}
	
	protected boolean isExpectationFailed(final HttpResponse response) {
		return compareStatusCode(response, HttpStatus.SC_EXPECTATION_FAILED);
	}
	
	protected boolean compareStatusCode(final HttpResponse response, final int statusCode) {
		if ( response == null || response.getStatusLine() == null ) {
			return false;
		}
		
		if ( statusCode == response.getStatusLine().getStatusCode() ) {
			return true;
		}
		return false;
	}
	
	protected Serializer getSerializer() {
		return SerializerFactory.getDefault();
	}
	
	private void addRequestBody(String requestBody,
			final HttpEntityEnclosingRequestBase request)
			throws UnsupportedEncodingException {
		final HttpEntity entity = new StringEntity(requestBody, HTTP.UTF_8);
		request.setEntity(entity);
		addHeaders(request);
	}
	
	protected void addHeaders(final HttpEntityEnclosingRequestBase request) {
		request.addHeader("Content-Type", getSerializer().getContentType());
	}
	
	protected <T> T deserializeResponse(HttpResponse response, Class<T> type) throws Throwable {
		return (T) getSerializer().read(response.getEntity().getContent(), type);
	}
	
	private String encode(final String stringUrl) throws Throwable {
		try {
			final URL url = new URL(stringUrl);
			final URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			return uri.toURL().toString();
		} catch (MalformedURLException e) {
			throw e;
		} catch (URISyntaxException e) {
			throw e;
		}
	}
	
}

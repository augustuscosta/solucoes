package com.fandas.webclient;

import com.fandas.webclient.serializer.Serializer;
import com.fandas.webclient.serializer.SerializerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URLEncoder;

/**
 * Implementacao padrao das requisicoes realizadas via HTTP.
 */
public abstract class AbstractCrudRequest<SerializedType> extends AbstractHTTPRequest implements CrudRequest<SerializedType> {
	
	private String 		serviceName;
	private final Class<SerializedType>		mClassTypeSingle;
	private final Class<SerializedType[]> 	mClassTypeArray;
	private HttpClient  					mHttpClient;
	
	public AbstractCrudRequest(Class<SerializedType> type) {
		this(type,null, SerializerFactory.getDefault());
	}

	public AbstractCrudRequest(Class<SerializedType> type, String service) {
		this(type,service,null);
	}
	
	@SuppressWarnings("unchecked")
	public AbstractCrudRequest(Class<SerializedType> type, String service, Serializer serializer) {
		this.mClassTypeSingle = type;
		mClassTypeArray = (Class<SerializedType[]>) Array.newInstance(type, 0).getClass();
		setServiceName(service);
	}

	@Override
	public SerializedType create(SerializedType model) throws Throwable {
		return post(getServiceName(), model, mClassTypeSingle);
	}
	
	@Override
	public Boolean update(String request, Object model) throws Throwable {
		try {
			final ByteArrayOutputStream output = new ByteArrayOutputStream();
			getSerializer().write(output, model);
			HttpResponse response = put(request, new String(output.toByteArray(), "UTF-8"));
			return deserializeResponse(response, Boolean.class);
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} 
	}

	@Override
	protected String getServer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean authenticate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SerializedType[] executePost(Object model, String url, Class<SerializedType> clazz) throws Throwable {
		return post(url,model,mClassTypeArray);
	}
	
	@Override
	public void delete(Integer id) throws Throwable {
		try {
			final HttpDelete delete = new HttpDelete(getBaseUrlWithServiceNameAndId(id));
			final HttpResponse response = execute(delete);
			EntityUtils.consumeQuietly(response.getEntity());
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
	
	@Override
	public SerializedType[] list() throws Throwable {
		try {
			final HttpGet get = new HttpGet(getBaseUrlWithServiceName().toString());
			final HttpResponse response = execute(get);
			return deserializeResponse(response, mClassTypeArray);
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
	
	@Override
	public SerializedType findById(final int id) throws Throwable {
		try {
			final HttpGet get = new HttpGet(getBaseUrlWithServiceNameAndId(id));
			final HttpResponse response = execute(get);
			return deserializeResponse(response, mClassTypeSingle);
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
	
	public SerializedType[] findByExample(final SerializedType example) throws Throwable {
		return post(getServiceName()+"/filter/",example,mClassTypeArray);
	}
	
	@Override
	public SerializedType[] findByExample(final SerializedType example, boolean fullProjection) throws Throwable {
		if(fullProjection){
			try {
				return post(getServiceName()+"/filter/?criteria="+URLEncoder.encode("{\"filters\":[\"**.*\"]}","UTF-8"),example,mClassTypeArray);
			} catch (UnsupportedEncodingException e) {
				throw e;
			}
		}
		return post(getServiceName()+"/filter/",example,mClassTypeArray);
	}
	
	public SerializedType[] findByExample(final SerializedType example, String projection) throws Throwable {
		try {
			return post(getServiceName() + "/filter?criteria=" + URLEncoder.encode(projection,"UTF-8"),
					example, mClassTypeArray);
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		
	}
	
	/**
	 * Realiza um post ao servidor e retorna o objeto serializado pelo servidor
	 * @param request a url da request
	 * @param example o objeto post que sera enviado 
	 * @param returnClazz a classe do objeto de retorno
	 * @return
	 */
	public <ReturnType> ReturnType post(String request, Object example, Class<ReturnType> returnClazz) throws Throwable {
		try {
			final StringBuilder builder = getServerURLBuilder();
			final HttpPost post = new HttpPost(builder.append(request).toString());

			final ByteArrayOutputStream output = new ByteArrayOutputStream();
			getSerializer().write(output, example);

			final HttpEntity entity = new ByteArrayEntity(output.toByteArray());
			post.setEntity(entity);
			addHeaders(post);
			
			final HttpResponse response = execute(post);
			return deserializeResponse(response, returnClazz);
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}

	public <ReturnType> ReturnType post(String request, String requestBody, Class<ReturnType> returnClazz) throws Throwable {
		try {
			final StringBuilder builder = getServerURLBuilder();
			final HttpPost post = new HttpPost(builder.append(request).toString());

			final ByteArrayOutputStream output = new ByteArrayOutputStream();
			getSerializer().write(output, requestBody);

			final HttpEntity entity = new ByteArrayEntity(output.toByteArray());
			post.setEntity(entity);
			addHeaders(post);
			
			final HttpResponse response = execute(post);
			return deserializeResponse(response, returnClazz);
		} catch (IllegalStateException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}

	
	private String getBaseUrlWithServiceName() throws MalformedURLException {
		final StringBuilder builder = getServerURLBuilder();
		builder.append(getServiceName());
		return builder.toString();
	}

	private String getBaseUrlWithServiceNameAndId(final int id) throws MalformedURLException {
		final StringBuilder builder = getServerURLBuilder();
		builder.append(getServiceName());
		builder.append("/");
		builder.append(id);
		return builder.toString();
	}

	/**
	 * Altera a forma padrao do servico. Caso null seja passado, o valor padrao 
	 * de servico sera utilizado.
	 * Por exemplo: com.greenmile.domain.I18NResource utiliza por padrao
	 * o servico [server]/I18NResource
	 * @param service
	 */
	public void setServiceName(String service) {
		if( service == null ) {
			serviceName = mClassTypeSingle.getSimpleName().toLowerCase();
		} else {
			serviceName = service;
		}
	}
	
	public String getServiceName(){
		return this.serviceName;
	}
	
	/**
	 * Sobrescreve o httpclient padrao
	 * @param httpClient
	 */
	public void setHttpClient(HttpClient httpClient) {
		this.mHttpClient = httpClient;
	}
	
	protected HttpClient getHttpClient() {
		if ( mHttpClient == null ) {
			mHttpClient = HttpClientBuilder.create().build();
		}
		
		return mHttpClient;
	}
	
}

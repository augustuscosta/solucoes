package com.fandas.webclient.exception;

public class EntityNotFoundException extends Throwable {

    private static final long serialVersionUID = -5377488606622502542L;

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String s) {
        super(s);
    }

    public EntityNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public EntityNotFoundException(Throwable throwable) {
        super(throwable);
    }
}

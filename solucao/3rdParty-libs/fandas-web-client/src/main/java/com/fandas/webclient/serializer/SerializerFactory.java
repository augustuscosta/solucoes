package com.fandas.webclient.serializer;

import com.fandas.webclient.serializer.Serializer;
import com.fandas.webclient.serializer.jackson.JacksonSerializer;

public class SerializerFactory {

	private static Serializer defaultSerializer;
	
	static { 
		defaultSerializer = new JacksonSerializer();
	}
	
	public static Serializer getDefault() { 
		return defaultSerializer;
	}
	
	public static void setDefault(Serializer serializer) { 
		defaultSerializer = serializer;
	}
	 
}

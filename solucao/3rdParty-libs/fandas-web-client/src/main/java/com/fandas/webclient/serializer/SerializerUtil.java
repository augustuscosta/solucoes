package com.fandas.webclient.serializer;

import com.fandas.webclient.serializer.SerializerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class SerializerUtil {

	/**
	 * Usa o serializador padrao definido em {@link com.fandas.webclient.serializer.SerializerFactory}
	 * para transformar o objeto em String
	 * @param toBeSerialized
	 * @return
	 */
	public static String objectToString(Object toBeSerialized) throws Throwable {
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			SerializerFactory.getDefault().write(output, toBeSerialized);
			return output.toString();
		} finally { 
			try {
				output.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}
	

	/**
	 * Usa o serializador padrao definido em {@link SerializerFactory} 
	 * para deserializar um objeto armazenado como string
	 * @param serializedObject
	 * @return
	 */
	public static <Type> Type fromToObject(String serializedObject, Class<Type> clazz) throws Throwable {
		final ByteArrayInputStream in = new ByteArrayInputStream(serializedObject.getBytes());
		try {
			return (Type) SerializerFactory.getDefault().read(in,clazz);
		} finally { 
			try {
				in.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}
}

package com.fandas.webclient.exception.handle;

import com.fandas.webclient.domain.ErrorMessage;
import org.apache.http.HttpStatus;

public class InternalServerErrorExceptionHandler extends AbstractExceptionHandler {

	@Override
	public int statusCode() {
		return HttpStatus.SC_INTERNAL_SERVER_ERROR;
	}

	@Override
	public Throwable createException(final ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return new Throwable("Unexpected internal server error.");
		}
		
		return new Throwable(errorMessage.getMessage());
	}
	
}
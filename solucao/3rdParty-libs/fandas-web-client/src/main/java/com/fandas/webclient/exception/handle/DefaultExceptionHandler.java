package com.fandas.webclient.exception.handle;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

public class DefaultExceptionHandler extends AbstractExceptionHandler {

	@Override
	public void handle(HttpResponse response) throws Throwable {
		final int statusCode = response.getStatusLine().getStatusCode();
		if( statusCode() != statusCode ) {
			Throwable exception = super.createException(getErrorMessage(response));
			if ( exception == null ) {
				exception = getUnknownException(statusCode);
			}
			
			throw exception;
		}
	}

	private Throwable getUnknownException(final int statusCode) {
		String message = "Unknown status code(%s) returned.";
		message = String.format(message, statusCode);
		
		return new Throwable(message);
	}
	
	/**
	 * Status code especifico da excecao
	 * @return
	 */
	public int statusCode() {
		return HttpStatus.SC_OK;
	}
	
}

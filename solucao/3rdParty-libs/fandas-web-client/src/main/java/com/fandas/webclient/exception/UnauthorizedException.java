package com.fandas.webclient.exception;

public class UnauthorizedException extends Throwable {

	private static final long serialVersionUID = -5938945591103528201L;

    public UnauthorizedException() {
    }

    public UnauthorizedException(String s) {
        super(s);
    }

    public UnauthorizedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public UnauthorizedException(Throwable throwable) {
        super(throwable);
    }

}

package com.fandas.webclient.exception.handle;

import org.apache.http.HttpResponse;

public interface ExceptionHandler {

	void handle(HttpResponse response) throws Throwable;
	
}

package com.fandas.webclient.exception;

public class ConflictException extends Throwable {

    private static final long serialVersionUID = -7294737968271995646L;

    public ConflictException() {
    }

    public ConflictException(String s) {
        super(s);
    }

    public ConflictException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ConflictException(Throwable throwable) {
        super(throwable);
    }

}

package com.fandas.webclient.serializer.jackson;

import com.fandas.webclient.serializer.Serializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class JacksonSerializer implements Serializer<ObjectMapper> {

	private static final ObjectMapper mapper = new ObjectMapper();

	static {
        mapper.registerModule(new CustomModuleSerializerAndDeserializer());
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	@Override
	public <T> T read(InputStream in, Class<T> type) throws Throwable {
        try {
            return mapper.readValue(in, type);
		} catch (Exception e) {
			throw e;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) { }
			}
		}
	}

	@Override
	public <T> void write(OutputStream out, T object) throws Throwable {
		try {
            mapper.writeValue(out, object);
		} catch (Exception e) {
			throw e;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) { }
			}
		}
	}

	@Override
	public String getContentType() {
		return "application/json";
	}

    @Override
    public ObjectMapper getJsonMapper() { return mapper; }

}

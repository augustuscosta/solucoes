package com.fandas.webclient.serializer;

import java.io.InputStream;
import java.io.OutputStream;

public interface Serializer<JsonMapper> {

	/**
	 * Deserializa um objeto a partir de um stream
	 * @param in 
	 * @param type
	 * @return
	 */
	<T> T read(InputStream in, Class<T> type) throws Throwable;
	
	/**
	 * Serializa um objeto em um output stream indicado
	 * @param out
	 * @param object
	 */
	<T> void write(OutputStream out, T object) throws Throwable;
	
	/**
	 * Indica o content-type da informacao sendo serializada. 
	 * Ex.: "application/json"
	 * @return 
	 */
	String getContentType();

    JsonMapper getJsonMapper();

}

package com.fandas.webclient.domain;

public class ErrorMessage {

	private final Object model;

	private String name;

	private String label;

	private final String message;

	private String errorClass;
	
	public ErrorMessage(Object model, String message) {
		this.model = model;
		this.message = message;
	}

	public ErrorMessage(Object model, String message, String name, String label) {
		this(model, message);
		this.name = name;
		this.label = label;
	}
	
	public Object getModel() {
		return model;
	}

	public String getName() {
		return name;
	}

	public String getLabel() {
		return label;
	}

	public String getMessage() {
		return message;
	}
	
	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	public String toString() {
		String entityName = "No entity name";
		
		if (model != null) {
			entityName = model.getClass().getSimpleName();
		}
		
		return new StringBuffer()
			.append("Error for ")
			.append(entityName)
			.append(" with message: ")
			.append(message)
			.append(" for label: ")
			.append(label)
			.toString();
	}

}

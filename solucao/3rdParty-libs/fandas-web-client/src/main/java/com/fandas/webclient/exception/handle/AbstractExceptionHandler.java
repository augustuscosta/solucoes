package com.fandas.webclient.exception.handle;

import com.fandas.webclient.serializer.SerializerFactory;
import com.fandas.webclient.domain.ContentBody;
import com.fandas.webclient.domain.ErrorMessage;
import com.fandas.webclient.serializer.Serializer;
import org.apache.http.HttpResponse;

import java.util.logging.Logger;

public abstract class AbstractExceptionHandler implements ExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger("AbstractExceptionHandler");

	private Serializer serializer;

	@Override
	public void handle(HttpResponse response) throws Throwable {
		final int statusCode = response.getStatusLine().getStatusCode();
		if ( statusCode() == statusCode ) {
			final Throwable ex = createException(getErrorMessage(response));
			if ( ex != null ) {
				throw ex;
			}
		}
	}
	
	/**
	 * Status code especifico da excecao
	 * @return
	 */
	public abstract int statusCode();
	
	protected Throwable createException(final ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return null;
		}
		
		return new Throwable(errorMessage.getMessage());
	}
	
	protected ErrorMessage getErrorMessage(final HttpResponse response) throws Throwable {
		try {
			final ContentBody content = deserializeResponse(response);
			if ( content != null ) {
				return content.getErrorMessage();
			}
		} catch (Exception e) {
			// Nao foi possivel deserializar o Error vindo na response.
            LOGGER.throwing("AbstractExceptionHandler", "getErrorMessage", e);
		}
		
		return null;
	}

	private ContentBody deserializeResponse(HttpResponse response) throws Throwable {
		return (ContentBody) getSerializer().read(response.getEntity().getContent(), ContentBody.class);
	}

	private Serializer getSerializer() {
		return serializer == null ? SerializerFactory.getDefault() : serializer;
	}

	public void setSerializer(Serializer serializer) {
		this.serializer = serializer;
	}
	
}

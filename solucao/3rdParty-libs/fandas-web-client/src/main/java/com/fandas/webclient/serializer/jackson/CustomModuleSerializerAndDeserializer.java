package com.fandas.webclient.serializer.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

import java.util.Date;

public class CustomModuleSerializerAndDeserializer extends SimpleModule {

    public CustomModuleSerializerAndDeserializer() {
        addSerializer(Date.class, new CustomDateSerializer());
        addDeserializer(Date.class, new CustomDateDeserializer());
    }

}

package com.fandas.webclient.exception.handle;

import com.fandas.webclient.exception.BadRequestException;

import com.fandas.webclient.domain.ErrorMessage;
import org.apache.http.HttpStatus;

public class BadRequestExceptionHandler extends AbstractExceptionHandler {

	@Override
	public int statusCode() {
		return HttpStatus.SC_BAD_REQUEST;
	}

	@Override
	public Throwable createException(final ErrorMessage errorMessage) {
		if ( errorMessage == null ) {
			return new BadRequestException("Bad Request. The server not respond.");
		}
		
		return new BadRequestException(errorMessage.getMessage());
	}

}
